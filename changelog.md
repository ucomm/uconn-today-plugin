# Changelog
## 3.0.2
- Fix for forced cache clearing. That functionality had the potential to cause fatal errors on multisite due to issues with loading order.
## 3.0.1
- Fix for race condition that occurred when `delete_transient` and `get_transient` would overlap
- Ensure returning `ResponseData` from `CacheService` and `PostREST`
## 3.0.0
- This update is a significant re-write of the "interior" of the plugin. Namely: how requests to UConn Today are made and cached. These changes are meant to improve stability and assist with debugging.
- Added
  - testing suite via PestPHP and brainmonkey/mockery
  - `CurlService` to manage making HTTP requests
  - `ResponseData` to standardize responses and ensure enhanced debugging
  - `CacheService` to simplify caching functionality
  - `HttpRequest` interface to describe request functionality
  - `Errors` interfaces to describe error functionality
  - `ShortcodeService` and `ShortcodeDataService` to manage shortcode operations not directly related to content display
  - `AdminBar` introduces an admin bar menu item to assist with diagnostics and cache clearing
- Deprecated
  - `CategoryDataNormalizer`. This functionality has been moved to a new endpoint on uctoday at `/wp-rest/uconn/v1/taxonomies`. See the `uconn-today-utilities` plugin for implementation
- Updated
  - transient cache prefix
  - most of the other classes
  - additional shortcode information in the admin area including per request status and responses
## 2.4.3
- Resolved bug in SiteOrigin that was not respecting multiple category selections
## 2.4.2
- Updated data classes and shortcode class to use nullable type checking for arguments and return values
## 2.4.1
- Updated admin notice when no articles can be fetched
- Updated debugging information output by the `debug` shortcode parameter
## 2.4.0
- Added new display options
  - cards
  - side-by-side
- Added support for category display as needed
## 2.3.1
- Added error handling surrounding categoryrest endpoint
- Resolved bug with multiple category selections not displaying specifically in site origin widget
## 2.3.0
- Resolved trailing comma bug
## 2.2.8
- Jenkinsfile updates
## 2.2.7
- Updated jenkinsfile to allow for tagged releases & pushes to staging sites
## 2.2.6
- Corrected name of production branch in jenkinsfile
## 2.2.5
- Modified widget form to allow for multi-select on categories
## 2.2.4
- Fix for categories in widget not displaying completely.
## 2.2.3
- update to give the widget the ability to select multiple categories with either AND or OR fetch setting
## 2.2.2
- update to caching functionality to prevent caching strings by accident. this was breaking things with BB on occassion.
- fix to variables on search/post requests
- update to support new category slugs
- update to support safe fetching of main uct stories if none are returned on the initial request
## 2.2.1
- updated widget so that previous widget updates (2.2.0) work properly when used in Site Origin

## 2.2.0
- Extended widget functionality to display articles by article slug(s)
- fixed bug with displaying articles by search term 
- updated widget to display categories in dropdown, as opposed to relying on user input
## 2.1.1
- Fixed bug that caused cached stories to never expire

## 2.1.0
- Added support for a cache duration option to the plugin settings
- Fixed plugin constants to avoid conflicts with other plugins
- Composer updates

## 2.0.3
- Fixed shortcode integer validation to allow the same number of posts as the widget

## 2.0.2
- Fixed "read more" text on view with images

## 2.0.1
- Style updates 

## 2.0.0
- Full re-write of the base plugin including:
  - new shortcode implementation
  - new Beaver Builder module to replace the castor module
  - new widget for use in the core WP widget areas and SiteOrigin page builder
  - new JS "reader mode" implementation
  - updated local dev process
## 1.3.7
- Fix for all county.
## 1.3.6
- Official support for UCT Categorization updates.
- Fixed some logic issues with featured images.
## 1.3.5
- Hotfix, added zone context in the admin settings page.
## 1.3.4
- Added a setting which can disable custom UCT plugin styles.
- Re-vamped the admin page to provide better context and clearer options for those using the shortcode.
- Added a shortcode builder.
- Added a feature to show post dates.
- Extended timeout for ZONE requests using the new cURL implementation

## 1.3.3
- Fixed a bug related to styles loading in when using SiteOrigin.
- Added support for CURL > wp_remote_get for better SSL management, and to be more in line with other Aurora plugins.
## 1.3.2
- Added support for fetching S&C/regular posts by county taxonomy/tag
## 1.3.1
- Added support for fetching S&C/regular posts by slug
## 1.3.0
- Updated so that multiple instances will work with the siteorigin page builder
- Added support for excerpts below the post image/title
- Added support for "read more" links
## 1.2.0
- Restructure the markup with photos a bit, to include proper aria labels/titles/tags.
## 1.1.1
- Added rel noopener to links for images and text
## 1.1.0
- Better styling for images
- Improved error handling
- Improved caching
## 1.0.12
- Better support for screen readers/accessibility
## 1.0.11
- Added support for the new combined zoned posts/school-college posts endpoint.
## 1.0.10
- Fixed style bug when articles don't have pictures
## 1.0.9
- Layout changed to css grid with fallbacks
- Conditional loading for css
## 1.0.8
- Added support for ssl
## 1.0.7
- Dropped support for combined school/college w/ category route
- Simplified endpoints requests
- Added support for: authors, campuses, and relevanssi search
## 1.0.6
- Updated documentation
- Better route handling for API calls.
## 1.0.5
- Added basic support for tags
##1.0.4
- Switched to production endpoints
- Fixed bug re images not being displayed
- Updated boilerplate
## 1.0.3
- Works as a standalone plugin and within Beaver Builder/Castor.
## 1.0.2
- Project structure conforms to UComm wordpress boilerplate
- Restructured plugin to MVC architecture
- Accounts for endpoints at 2 locations with 3 different options
- All GET requests include images in the response by default. If images aren't desired, they're simply not used in the view.