# UCONN TODAY PLUGIN

Contains the source code for the UCONN Today plugin, which pulls select posts from the UCONN Today site and displays them via WordPress shortcode. There are three "modes" that this plugin can output:

- A collection of links with the featured image for each post
- A collection of links with just the titles and no images
- A javascript "reader mode" which keeps users on sites but still lets them view the content. **NB - The only way to activate this mode is through either the settings in the admin area or a SiteOrigin widget.**

It also allows for creating those views in three ways:
- Shortcode
- Beaver Builder module
- WordPress widget with SiteOrigin compatibility

All three implementations rely on the same `display` method that can be found in the `Shortcode` class.

## Usage
### To get a project running.

NB - You _must_ install [NVM](https://github.com/nvm-sh/nvm) to develop on this project. That way node modules/versions are managed correctly.

**Terminal 1**
```bash
$ composer install # first time only
$ docker-compose up
```

**Terminal 2**
```bash
$ nvm use
$ npm install # first time only
$ npm run dev
```

### Accessing containers
There are two ways to access containers. Either with docker directly or with docker compose.

To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name /bin/bash
```

```bash
$ docker-compose exec container_name bash
```

### Webpack
This project uses webpack. Please see the `webpack.config.js` file for details


### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
```bash
git tag # check the current tags/versions on the project
git flow release start {new_tag}
git flow release finish {new_tag}
# in the first screen add new info or just save the commit message as is
# in the second screen type in the same tag you just used and save.
git push --tags && git push origin master
git checkout develop
``` 
Finally re-run the pipeline build on the [satis repo](https://bitbucket.org/ucomm/composer-repository).

## Development
There are five main directories for this project
- `/admin-assets` holds CSS and JS files necessary for the admin settings page
- `/lib` holds the PHP classes
- `/partials` holds admin and public template files for display
- `/public-assets` holds CSS and JS assets _not_ used by the reader mode app
- `/src` holds Sass and JS(X) assets that _are_ used by the reader mode app

## Plugin Shortcode

WordPress shortcode is instantiated with `[UC-Today]`

**Shortcode Attributes:**

Shortcode attributes are not required, as each are set to default values.

- `school-college` = **DEPRECATED** selects school/college to pull UCONN Today posts from. This attribute is just left over in case someone somewhere has used it in a shortcode. It falls back safely to `category`
- `category` = selects the UCONN Today category to pull posts from
- `post-slugs` = selects individual posts by their slugs
- `number-of-posts` = The number of posts to be displayed by the plugin
- `columns` = how many columns the posts will be displayed in
- `pictures` = Whether or not to display pictures along with the posts
- `show-date` = Whether or not to show the date of the post. Defaults to false
- `show-excerpt` = Whether or not to show the post excerpt. Defaults to false
- `read-more-text` = Optional text to append after the excerpt

**Example Shortcode:**

- `[UC-Today category="arts" columns="2" pictures="false"]`
- `[UC-Today number-of-posts="6" columns="3"]`
- `[UC-Today]`

## Tandem Mode
Debugging REST requests to the UCToday theme can be a pain if you can only make them to either the dev or prod uctoday servers. That's because you then have to try debugging the requests from the servers instead of locally which is less than ideal 😉.

To make this task easier, you can now put this project in "tandem mode". To do so, follow these steps
- Start the uconn-today-2015-theme project with `docker-compose up`
- Start this project with `docker-compose -f docker-compose-tandem.yml up`

The first docker-compose file will create a default network for the theme project. The second will attach the containers for this project to _that_ network instead of creating a new one.