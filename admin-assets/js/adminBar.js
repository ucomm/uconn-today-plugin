document.addEventListener('DOMContentLoaded', () => {
  const shortcodeAttsContainer = document.getElementById('uct-shortcode-atts')
  if (!shortcodeAttsContainer) {
    return
  }
  const shortcodeAtts = document.querySelectorAll('.shortcode-atts')
  shortcodeAtts.forEach((atts, index) => {
    const attItems = atts.querySelectorAll('.shortcode-att')
    const shortcodeAttsList = document.createElement('ul')
    const shortcodeAttsTitle = document.createElement('p')
    shortcodeAttsTitle.innerHTML = `UCToday Plugin Attributes ${index + 1}`
    shortcodeAttsContainer.appendChild(shortcodeAttsTitle)
    shortcodeAttsContainer.appendChild(shortcodeAttsList)
    attItems.forEach(att => {
      const attItem = document.createElement('li')
      attItem.innerHTML = `- ${att.innerHTML}`
      shortcodeAttsList.appendChild(attItem)
    })
  });
})

