jQuery(document).ready( function() {

    window.checkOptions = (select) => {

      var catInputField, 
          catInputWrap, 
          searchInputField,
          searchInputWrap, 
          slugInputField, 
          slugInputWrap; 

      var wrapperContainer = select.closest('.uct-widget-container'); 
      var wrapperID = jQuery(wrapperContainer).attr("id").replace(/uct-widget-container-/, '');

      var inputContainer = select.closest('.filter-type-select'); 
      var inputID = jQuery(inputContainer).attr("id").replace(/-filter-type/, '');

      if (inputID.length) { 
        catInputField = document.getElementById(inputID + '-category');
        searchInputField = document.getElementById(inputID + '-search'); 
        slugInputField = document.getElementById(inputID + '-post-slugs'); 
      } else { 
        catInputField = document.getElementById('widget-' + wrapperID + '-category');
        searchInputField = document.getElementById('widget-' + wrapperID + '-search'); 
        slugInputField = document.getElementById('widget-' + wrapperID + '-post-slugs'); 
      }

      catInputWrap = catInputField.closest('.category-input-wrapper'); 
      searchInputWrap = searchInputField.closest('.search-input-wrapper');
      slugInputWrap = slugInputField.closest('.post-slugs-input-wrapper');

      // hide the non-selected filter types, but also clear their values (necessary to keep clean shortcode and expected behavior)
      if (select.options[select.selectedIndex].value == "categoryFilter") {
        catInputWrap.style.display = 'block';
        searchInputWrap.style.display = 'none';
        searchInputField.value = '';  
        slugInputWrap.style.display = 'none';
        slugInputField.value = '';
      } else if (select.options[select.selectedIndex].value == "searchFilter") {
        catInputWrap.style.display = 'none';
        catInputField.value = '';
        searchInputWrap.style.display = 'block';
        slugInputWrap.style.display = 'none'; 
        slugInputField.value = ''; 
      } else if (select.options[select.selectedIndex].value == "slugFilter") { 
        catInputWrap.style.display = 'none';
        catInputField.value = '';
        searchInputWrap.style.display = 'none';
        searchInputField.value = '';
        slugInputWrap.style.display = 'block';
      } else { 
        catInputWrap.style.display = 'none';
        catInputField.value = '';
        searchInputWrap.style.display = 'none';
        searchInputField.value = '';
        slugInputWrap.style.display = 'none';
        slugInputField.value = '';
      }
    }

}
); 