function regenerateShortcodeBuilder() {
  let code_area = document.getElementById('uct-shortcode-builder');

  changeCategorizationDisplay();

  let attributes = '';
  attributes += `${getCategorizationString()}`;
  attributes += ` ${getCheckedValue('pictures')}`;
  attributes += ` ${getCheckedValue('show-excerpt')}`;
  attributes += ` ${getCheckedValue('show-date')}`;
  attributes += ` ${getValue('number-of-posts')}`
  attributes += ` ${getValue('columns')}`
  attributes += ` ${getValue('read-more-text')}`;

  code_area.innerHTML = `[UC-Today ${attributes}]`;
}

function getCategorizationString() {
  let categorization = document.getElementById('categorization');
  let cat = categorization.options[categorization.selectedIndex].value;
  let val_element = document.getElementById(cat);
  let val = ``;
  let output = ``;
  if (val_element) {
    val = val_element.value;
  }

  output = `${cat}="${val}"`

  return output;
}

function getCheckedValue(id) {
  const el = document.getElementById(id)
  const val = (el.checked) ? 'true' : 'false'
  return `${id}="${val}"`
}

function getValue(id) {
  const el = document.getElementById(id)
  const val = el.value
  return `${id}="${val}"`
}

function changeCategorizationDisplay() {
  let categorization = document.getElementById('categorization');
  let cat = categorization.options[categorization.selectedIndex].value;

  let togglefields = document.querySelectorAll('.categorization-toggle');
  togglefields.forEach(element => {
    element.style.display = 'none';
    if (element.dataset.cat === cat) {
      element.style.display = 'block';
    }
  });
}

function doCopyShortcode({ target }) {
  const builder = document.getElementById('uct-shortcode-builder')
  navigator.clipboard.writeText(builder.innerText)
    .then(() => {
      target.innerText = 'Shortcode Copied!'
    })
    .catch(err => console.log(err))
}

// Main process
window.addEventListener('load', function () {
  let categorization = document.getElementById('categorization');
  let include_pictures = document.getElementById('pictures');
  let show_excerpt = document.getElementById('show-excerpt');
  let show_date = document.getElementById('show-date');
  let number_posts = document.getElementById('number-of-posts');
  let read_more_text = document.getElementById('read-more-text');
  let category = document.getElementById('category');
  let author = document.getElementById('author');
  let columns = document.getElementById('columns');
  let copyShortcode = document.getElementById('copy-shortcode')

  regenerateShortcodeBuilder();

  categorization.addEventListener("change", regenerateShortcodeBuilder);
  include_pictures.addEventListener("change", regenerateShortcodeBuilder);
  show_excerpt.addEventListener("change", regenerateShortcodeBuilder);
  show_date.addEventListener("change", regenerateShortcodeBuilder);
  number_posts.addEventListener("change", regenerateShortcodeBuilder);
  number_posts.addEventListener("keyup", regenerateShortcodeBuilder);
  read_more_text.addEventListener("keyup", regenerateShortcodeBuilder);
  category.addEventListener("keyup", regenerateShortcodeBuilder);
  columns.addEventListener("change", regenerateShortcodeBuilder);
  columns.addEventListener("keyup", regenerateShortcodeBuilder);
  author.addEventListener("keyup", regenerateShortcodeBuilder);
  copyShortcode.addEventListener('click', doCopyShortcode);
});