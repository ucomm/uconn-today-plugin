pipeline {
  agent any
  environment {
    PROJECT_SLUG = "uconn-today-plugin"
    DEV_BRANCH = "develop"
    FEATURE_BRANCH = "feature/*"
    PROD_BRANCH = "master"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag'
    )
    booleanParam(
      name: 'DEPLOY_TO_PROD',
      defaultValue: false,
      description: 'In case the master branch needs to go to prod, this will toggle the prod push'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
    }
    stage("Prepare Build Assets") {
      // feature branches can be added and/or this "when" block can be removed to build on all commitss
      when {
        anyOf {
          branch "${DEV_BRANCH}";
          branch "${FEATURE_BRANCH}";
          branch "${PROD_BRANCH}"
          buildingTag()
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/npm.sh"
        }
      }
      post {
        always {
          echo "======== $BRANCH_NAME end asset builds ========"
        }
        success {
          echo "======== success - $BRANCH_NAME asset builds ========"
        }
        failure {
          echo "======== failed - $BRANCH_NAME asset builds ========"
        }
      }
    }
    stage('Dev Pushes') {
      when {
        anyOf {
          branch "${FEATURE_BRANCH}";
          branch "${DEV_BRANCH}"
        }
      }
      // config files need to be accessed by their IDs. these can be found in Manage Jenkins > Config Files area
      // the ID listed below is the one for the staging-plugin-paths file
      steps {
        configFileProvider([configFile(fileId: '723952ef-c9a9-4894-b153-460f3d5afcda', targetLocation: './plugin-paths.sh')]) {
          catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
            sh "${WORKSPACE}/ci-scripts/dev-push.sh"
          }
        }
      }
      post {
        failure {
          sendNotifications "DEV PUSH FAILED"
        }
      }
    }
    // I know these two stages could probably be run in parallel in the general dev push stage.
    // however, sometimes permissions get messed up on the aurora sandboxes and I don't want the whole job failing because of that
    stage('Aurora Sandbox Push') {
      when {
        anyOf {
          branch "${FEATURE_BRANCH}";
          branch "${DEV_BRANCH}"
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/aurora-sandbox-push.sh"
        }
      }
      post {
        failure {
          sendNotifications "AURORA SANDBOX PUSH FAILED"
        }
      }
    }
    stage('Health Sandbox Push') {
      when {
        anyOf {
          branch "${FEATURE_BRANCH}";
          branch "${DEV_BRANCH}"
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/health-sandbox-push.sh"
        }
      }
      post {
        failure {
          sendNotifications "HEALTH SANDBOX PUSH FAILED"
        }
      }
    }
    stage('Dev - bitbucket archive') {
      when {
        branch "${DEV_BRANCH}"
      }
      environment {
        FILENAME = "$GIT_BRANCH"
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
        }
      }
    }
    stage('Staging Push') {
      when {
        branch "${PROD_BRANCH}"
      }
      // config files need to be accessed by their IDs. these can be found in Manage Jenkins > Config Files area
      // the ID listed below is the one for the staging-plugin-paths file
      steps {
        configFileProvider([configFile(fileId: '723952ef-c9a9-4894-b153-460f3d5afcda', targetLocation: './plugin-paths.sh')]) {
          catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
            sh "${WORKSPACE}/ci-scripts/staging-push.sh"
          }
        }
      }
      post {
        failure {
          sendNotifications "FAILURE - Push to staging"
        }
      }
    }
    stage('Prod Push') {
      environment {
        FILENAME = "$TAG_NAME"
      }
      when {
        anyOf {
          buildingTag();
          expression {
            return params.DEPLOY_TO_PROD && env.BRANCH_NAME == "master"
          }
        }
      }
      parallel {
        stage('Push tag to prod') {
          steps {
            // this ID is the one for the prod plugin paths
             configFileProvider([configFile(fileId: 'bc2af336-9f19-4b43-852c-7dc672293373', targetLocation: './plugin-paths.sh')]) {
            // use a prod push script similar to the example. 
              catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                sh "${WORKSPACE}/ci-scripts/prod-push.sh"
              }
            }
          }
        }
        stage('Archive tag to bitbucket') {
          steps {
            script {
              if (env.RSYNC_DRY_RUN == "false") {
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                  sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
                }
              }
            }
          }
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          // NB - don't use script blocks very much.
          // I'm just adding this so th comm0-updates channel doesn't get flooded on dry runs
          script {
            if (env.RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *$PROJECT_SLUG* updated"
                  ]              
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "${PROJECT_SLUG} Changelog")
            }
          }
        }
      }
    }
  }
  post {
    // send slack notifications when the project finishes
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
    always {
      echo "======== Cleanup ========"
      sh "rm -rf node_modules"
    }
  }
}