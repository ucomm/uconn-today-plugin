/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(self["webpackChunkuconn_today_plugin"] = self["webpackChunkuconn_today_plugin"] || []).push([["uct-video-embed"],{

/***/ "./src/js/Components/VideoFormatEmbed.jsx":
/*!************************************************!*\
  !*** ./src/js/Components/VideoFormatEmbed.jsx ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-player */ \"./node_modules/react-player/lib/index.js\");\n\n\n\nvar VideoFormatEmbed = function VideoFormatEmbed(_ref) {\n  var url = _ref.url;\n  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default().createElement(react_player__WEBPACK_IMPORTED_MODULE_1__.default, {\n    url: url,\n    style: {\n      margin: '50px auto'\n    }\n  });\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (VideoFormatEmbed);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly91Y29ubi10b2RheS1wbHVnaW4vLi9zcmMvanMvQ29tcG9uZW50cy9WaWRlb0Zvcm1hdEVtYmVkLmpzeD84MzI4Il0sIm5hbWVzIjpbIlZpZGVvRm9ybWF0RW1iZWQiLCJ1cmwiLCJtYXJnaW4iXSwibWFwcGluZ3MiOiI7Ozs7QUFBQTtBQUNBOztBQUVBLElBQU1BLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsT0FBYTtBQUFBLE1BQVZDLEdBQVUsUUFBVkEsR0FBVTtBQUNwQyxzQkFBTywyREFBQyxpREFBRDtBQUFhLE9BQUcsRUFBRUEsR0FBbEI7QUFBdUIsU0FBSyxFQUFFO0FBQUVDLFlBQU0sRUFBRTtBQUFWO0FBQTlCLElBQVA7QUFDRCxDQUZEOztBQUlBLCtEQUFlRixnQkFBZiIsImZpbGUiOiIuL3NyYy9qcy9Db21wb25lbnRzL1ZpZGVvRm9ybWF0RW1iZWQuanN4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xuaW1wb3J0IFJlYWN0UGxheWVyIGZyb20gJ3JlYWN0LXBsYXllcidcblxuY29uc3QgVmlkZW9Gb3JtYXRFbWJlZCA9ICh7IHVybCB9KSA9PiB7XG4gIHJldHVybiA8UmVhY3RQbGF5ZXIgdXJsPXt1cmx9IHN0eWxlPXt7IG1hcmdpbjogJzUwcHggYXV0bycgfX0gLz5cbn1cblxuZXhwb3J0IGRlZmF1bHQgVmlkZW9Gb3JtYXRFbWJlZCJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/js/Components/VideoFormatEmbed.jsx\n");

/***/ })

}]);