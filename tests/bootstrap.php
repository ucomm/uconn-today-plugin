<?php
require_once dirname(__FILE__) . '/env.php';

/**
 * Prevent the error
 * Patchwork\Exceptions\DefinedTooEarly: The file that defines wp_remote_get() was included earlier than Patchwork. Please reverse this order to be able to redefine the function in question.
 */
require_once dirname(__FILE__, 2) . '/vendor/antecedent/patchwork/Patchwork.php';
require_once dirname(__FILE__, 2) . '/vendor/autoload.php';
require_once dirname(__FILE__, 2) . '/www/wordpress/wp-load.php';
require_once dirname(__FILE__, 2) . '/uconn-today-plugin.php';

require_once dirname(__FILE__) . '/TestCases/TestCase.php';
require_once dirname(__FILE__) . '/TestCases/AssetsTestCase.php';
require_once dirname(__FILE__) . '/TestCases/SettingsTestCase.php';
require_once dirname(__FILE__) . '/TestCases/QueriesTestCase.php';
require_once dirname(__FILE__) . '/TestCases/ServicesTestCase.php';
require_once dirname(__FILE__) . '/TestCases/NormalizersTestCase.php';
require_once dirname(__FILE__) . '/TestCases/FrontendTestCase.php';
require_once dirname(__FILE__) . '/TestCases/AdminTestCase.php';