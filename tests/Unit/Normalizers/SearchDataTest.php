<?php

uses(NormalizersTestCase::class);

beforeEach(function() {
  $this->searchDataNormalizer->setData($this->storyData);
});

afterEach(function() {
  $this->searchDataNormalizer->setData([]);
});

describe('Search Data Normalizer: Unit', function() {
  it('normalizes a search result', function() {
    $normalizedResult = $this->searchDataNormalizer->normalizeResult($this->storyData[0]);
    expect($normalizedResult)
      ->toBe(1);
  });

  it('normalizes all search results', function() {
    $searches = [
      (object) [
        'id' => 1,
        'title' => 'Title 1',
        'date' => '2021-01-01',
      ],
      (object) [
        'id' => 3,
        'title' => 'Title 3',
        'date' => '2021-01-03',
      ],
      (object) [
        'id' => 2,
        'title' => 'Title 2',
        'date' => '2021-01-02',
      ]
    ];
    $this->searchDataNormalizer->setData($searches);
    $normalizedResults = $this->searchDataNormalizer->createNormalizedData();
    expect($normalizedResults)->toBe([ 1,3,2 ]);
  });
});