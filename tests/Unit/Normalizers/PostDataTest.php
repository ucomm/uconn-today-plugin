<?php

uses(NormalizersTestCase::class);

describe('Post Data Normalizer: Unit', function() {
  it('sorts stories by date', function() {
    $storyData = [
      (object) ['ID' => 1, 'date' => '2021-01-01'],
      (object) ['ID' => 3, 'date' => '2021-01-03'],
      (object) ['ID' => 2, 'date' => '2021-01-02']
    ];
    $sortedStories = $this->postDataNormalizer->testSortStoriesByDate($storyData);
    expect($sortedStories[0]->ID)
      ->toBe(3)
      ->and($sortedStories[1]->ID)
      ->toBe(2)
      ->and($sortedStories[2]->ID)
      ->toBe(1);
  });

  it('returns `null` for an unset featured image', function() {
    $story = (object) [];
    $featuredImage = $this->postDataNormalizer->testSetFeaturedImage($story);
    expect($featuredImage)->toBeNull();
  });

  it('returns a featured image url', function() {
    $featuredImage = $this->postDataNormalizer->testSetFeaturedImage($this->featuredImageObject);
    expect($featuredImage)->toBe('medium_large.jpg');
  });

  it('returns featured image alt text', function() {
    $altText = $this->postDataNormalizer->testSetFeaturedImageAlt($this->featuredImageObject);
    expect($altText)->toBe('alt text');
  });

  it('sets the story date to `m/d/y` format', function() {
    $date = '2021-01-01';
    $formattedDate = $this->postDataNormalizer->testSetStoryDate($date);
    expect($formattedDate)->toBe('01/01/21');
  });

  it('returns an empty array for unset post terms', function() {
    $postTerms = null;
    $categories = $this->postDataNormalizer->testSetPublicCategories($postTerms);
    expect($categories)->toBe([]);
  }); 

  it('returns an array of public categories from all possible terms', function() {
    $publicCategories = $this->postDataNormalizer->testSetPublicCategories($this->postTerms);

    expect($publicCategories)
      ->toBeArray()
      ->and($publicCategories[0])
      ->toHaveProperties([
        'id',
        'name',
        'slug',
        'link'
      ]);
  });

  it('creates normalized data for a single story', function() {
    $this->postDataNormalizer->shouldReceive('setStoryDate')
      ->once()
      ->with($this->storyData[0]->date)
      ->andReturn('01/01/21');

    $this->postDataNormalizer->shouldReceive('setFeaturedImage')
      ->once()
      ->with($this->storyData[0])
      ->andReturn('https://test.com/medium_large.jpg');

    $this->postDataNormalizer->shouldReceive('setFeaturedImageAlt')
      ->once()
      ->with($this->storyData[0])
      ->andReturn('alt text');

    $this->postDataNormalizer->shouldReceive('setPublicCategories')
      ->once()
      ->with($this->storyData[0]->_embedded->{'wp:term'})
      ->andReturn([
        (object) [
          'id' => 1,
          'name' => 'Category',
          'slug' => 'category',
          'link' => 'https://test.com/category'
        ]
      ]);
    $normalizedStory = $this->postDataNormalizer->testCreateNormalizedStory($this->storyData[0]);

    expect($normalizedStory)
      ->toHaveKeys([
        'title',
        'link',
        'date',
        'excerpt',
        'featured_image',
        'featured_image_alt',
        'public_categories'
      ]);
  });

  it('normalizes an array of stories', function() {
    $this->postDataNormalizer->setData($this->storyData);

    $this->postDataNormalizer->shouldReceive('sortStoriesByDate')
      ->once()
      ->with($this->storyData)
      ->andReturn($this->storyData);

    $this->postDataNormalizer->shouldReceive('createNormalizedStory')
      ->atLeast()
      ->once()
      ->with($this->storyData[0])
      ->andReturn([
        'title' => 'Story 1',
        'link' => 'https://test.com/story-1',
        'date' => '01/01/21',
        'excerpt' => 'Story 1 excerpt',
        'featured_image' => 'https://test.com/medium_large.jpg',
        'featured_image_alt' => 'alt text',
        'public_categories' => [
          (object) [
            'id' => 1,
            'name' => 'Category',
            'slug' => 'category',
            'link' => 'https://test.com/category'
          ]
        ]
      ]);


    $normalizedData = $this->postDataNormalizer->createNormalizedData();
    expect($normalizedData)
      ->toBeArray()
      ->and($normalizedData[0])
      ->toHaveKeys([
        'title',
        'link',
        'date',
        'excerpt',
        'featured_image',
        'featured_image_alt',
        'public_categories'
      ]);
    $this->postDataNormalizer->setData([]);
  });
});