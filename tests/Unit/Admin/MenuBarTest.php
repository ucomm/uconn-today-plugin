<?php

uses(AdminTestCase::class);

use Brain\Monkey\Functions;

describe('Menu Bar: Unit', function() {
  it('adds a menu group item', function() {
    $this->wpAdminBar->shouldReceive('add_menu')
      ->once()
      ->with([
        'id' => 'uctoday-plugin-admin-bar-group',
        'title' => 'UConn Today Plugin',
        'href' => '#'
      ]);

    $this->adminBar->addMenuGroup($this->wpAdminBar);
  }); 

  // be careful with the HTML in the meta key.
  // changing it will break the test and leave you confused. make sure that they are the same.
  it('adds a clear cache menu item', function() {
    $this->wpAdminBar->shouldReceive('add_menu')
      ->once()
      ->with([
      'id' => 'uctoday-plugin-admin-bar-clear-cache',
      'parent' => 'uctoday-plugin-admin-bar-group',
      'title' => 'Clear UConn Today Cache',
      'meta' => [
        'html' => '<form method="POST" action="/">
          <input type="hidden" name="clear-uct-cache" value="true">
          <input type="submit" value="Clear UConn Today Cache">
        </form>'
      ]
    ]);

    $this->adminBar->addClearCacheMenu($this->wpAdminBar);
  });

  it('adds a current shortcode attributes container', function() {
    $this->wpAdminBar->shouldReceive('add_menu')
      ->once()
      ->with([
        'id' => 'uctoday-plugin-admin-bar-shortcode-attributes',
        'parent' => 'uctoday-plugin-admin-bar-group',
        'title' => 'Current Shortcode Attributes',
        'meta' => [
          'html' => '<div id="uct-shortcode-atts"></div>'
        ]
      ]);

    $this->adminBar->currentShortcodeAttributes($this->wpAdminBar);
  });

  it('adds the menu bar', function() {
    $this->adminBar->shouldReceive('addMenuGroup')
      ->once()
      ->with($this->wpAdminBar);

    $this->adminBar->shouldReceive('addClearCacheMenu')
      ->once()
      ->with($this->wpAdminBar);

    $this->adminBar->shouldReceive('currentShortcodeAttributes')
      ->once()
      ->with($this->wpAdminBar);

    $this->adminBar->addMenuBar($this->wpAdminBar);
  });

});