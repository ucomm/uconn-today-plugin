<?php

uses(SettingsTestCase::class);

use Brain\Monkey\Functions;

describe('Options: Unit', function() {
  it('can get the settings name', function() {
    $name = $this->pluginOptions->getSettingsName();
    expect($name)->toBe('uctoday_plugin_settings');
  });

  it('can get the settings group', function() {
    $group = $this->pluginOptions->getSettingsGroup();
    expect($group)->toBe('uctoday_plugin');
  });

  it('can get the settings page', function() {
    $page = $this->pluginOptions->getSettingsPage();
    expect($page)->toBe('uctoday_plugin');
  });

  it('registers settings', function() {
    Functions\expect('register_setting')
      ->once()
      ->with('uctoday_plugin', 'uctoday_plugin_settings');
    $this->pluginOptions->registerSettings();
  });

  it('adds a settings section', function() {
    Functions\expect('add_settings_section')
      ->once()
      ->with(
        'uctoday_plugin_general',
        'General Settings',
        [ $this->pluginOptions, 'generalSettingsSectionCallback' ],
        'uctoday_plugin'
      );
    $this->pluginOptions->addSettingsSection();
  });

  it('adds settings fields to the page', function() {
    $page = 'uctoday_plugin';

    Functions\when('wp_get_environment_type')
      ->justReturn('production');
    
    Functions\expect('add_settings_field')
      ->once()
      ->with(
        'uctoday_plugin_setting_disable_styles',
        'Disable UConn Today Plugin Custom Styles',
        [ $this->pluginOptions, 'settingDisableStylesCallback' ],
        $page,
        'uctoday_plugin_general',
        ''
      );

    Functions\expect('add_settings_field')
      ->once()
      ->with(
        'uctoday_plugin_setting_cache_duration',
        'UCToday Plugin Cache Duration',
        [$this->pluginOptions, 'settingCacheDurationCallback'],
        $page,
        'uctoday_plugin_general'
      );

    Functions\expect('add_settings_field')
      ->never()
      ->with(
        'uctoday_plugin_setting_tandem_mode',
        'Tandem Mode',
        [$this->pluginOptions, 'settingTandemModeCallback'],
        $page,
        'uctoday_plugin_general'
      );

    $this->pluginOptions->addSettingsFields();
  });

  it('enables a tandem mode setting for local development', function() {
    Functions\when('wp_get_environment_type')
      ->justReturn('local');

    Functions\expect('add_settings_field')
      ->once()
      ->with(
        'uctoday_plugin_setting_tandem_mode',
        'Enable Tandem Mode',
        [$this->pluginOptions, 'settingTandemModeCallback'],
        'uctoday_plugin',
        'uctoday_plugin_general'
      );

    $this->pluginOptions->addSettingsFields();
  });

  it('echos an empty string for the general settings section', function() {
    ob_start();
    $this->pluginOptions->generalSettingsSectionCallback();
    $output = ob_get_clean();
    expect($output)->toBe('');
  });

  it('prevents any role lower than author from clearing the cache', function() {
    global $wpdb;
    $mockWpdb = Mockery::mock('wpdb');
    $wpdb = $mockWpdb;

    Functions\when('current_user_can')
      ->justReturn(false);

    $mockWpdb->shouldNotReceive('prepare');
    $mockWpdb->shouldNotReceive('esc_like');
    $mockWpdb->shouldNotReceive('query');
  });


});

describe('Settings Page: Unit', function() {
  it('gets the menu slug', function() {
    $slug = $this->settingsPage->getMenuSlug();
    expect($slug)->toBe('uctoday_settings');
  });

  it('adds an option page on init', function() {
    Functions\expect('add_options_page')
      ->once()
      ->with(
        'UConn Today Plugin',
        'UConn Today Plugin',
        'manage_options',
        'uctoday_settings',
        [$this->settingsPage, 'getOptionsPage']
      );

    $this->settingsPage->init();
  });
});