<?php

uses(FrontendTestCase::class);

use Brain\Monkey\Functions;
use UCTP\Frontend\Shortcode;

describe('Shortcode: Unit', function() {
  it('gets the public display path', function($style, $withPictures, $expected) {
    $path = $this->shortcode->getPublicDisplayPath($style, $withPictures);
    expect($path)->toBe($expected);
  })->with([
    [
      'style' => 'with-pictures',
      'withPictures' => true,
      'expected' => UCT_PLUGIN_DIR . 'partials/public/display-with-pictures.php'
    ],
    [
      'style' => 'cards',
      'withPictures' => false,
      'expected' => UCT_PLUGIN_DIR . 'partials/public/display-cards.php'
    ],
    [
      'style' => 'side-by-side',
      'withPictures' => false,
      'expected' => UCT_PLUGIN_DIR . 'partials/public/display-side-by-side.php'
    ],
    [
      'style' => 'default',
      'withPictures' => false,
      'expected' => UCT_PLUGIN_DIR . 'partials/public/display-no-pictures.php'
    ]
  ]);

  it('gets the pictures class', function($style, $withPictures, $expected) {
    $class = $this->shortcode->getPicturesClass($style, $withPictures);
    expect($class)->toBe($expected);
  })->with([
    [
      'style' => 'uconn-edu-dropshadow',
      'withPictures' => true,
      'expected' => 'uconn-edu-dropshadow'
    ],
    [
      'style' => 'default',
      'withPictures' => true,
      'expected' => 'with-pictures'
    ],
    [
      'style' => 'default',
      'withPictures' => false,
      'expected' => ''
    ]
  ]);

  it('adds the shortcode', function() {
    Functions\expect('add_shortcode')
      ->once()
      ->with('UC-Today', [$this->shortcode, 'display']);

    ob_start();
    $this->shortcode->addShortcode();
    ob_get_clean();
  });
})->covers(Shortcode::class);