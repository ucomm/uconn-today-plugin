<?php

uses(QueriesTestCase::class);

describe('Requester: Unit', function () {
  it('gets the environment', function () {
    $env = $this->requester->getEnv();
    expect($env)->toBe('dev.today.uconn.edu');
  });

  it('gets the endpoint', function () {
    $endpoint = $this->requester->getEndpoint();
    expect($endpoint)->toBe('/posts');
  });

  it('sets the endpoint', function () {
    $endpoint = $this->requester->setEndpoint('/pages');
    expect($endpoint)->toBe('/pages');
    $this->requester->setEndpoint('/posts');
  });

  it('gets the URL', function () {
    $url = $this->requester->getUrl();
    expect($url)->toBe('https://dev.today.uconn.edu/wp-rest/wp/v2/posts?');
  });

  it('gets the query atts', function () {
    $atts = $this->requester->getQueryAtts();
    expect($atts)->toBe([]);
  });

  it('sets the query atts', function () {
    $atts = $this->requester->setQueryAtts(['per_page' => 5]);
    expect($atts)->toBe(['per_page' => 5]);
    $this->requester->setQueryAtts([]);
  });

  it('filters queries to remove null attributes', function () {
    $query = [
      'test' => 'data',
      'null' => null
    ];
    $filtered = $this->requester->testFilterQuery($query);
    expect($filtered)->toBe(['test' => 'data']);
  });

  it('does a curl request and returns `ResponseData`', function () {
    $this->requester->curlService->shouldReceive('request')
      ->once()
      ->with('https://tests.com?test')
      ->andReturn($this->goodResponseData);

    $data = $this->requester->testDoCurlRequest('https://tests.com?test');
    expect($data)->toBe($this->goodResponseData);
  });

  it('builds a query string from the filtered query', function () {
    $query = [
      'author' => 'author-test-slug',
      'category' => 'category-test-slug',
      'number-of-posts' => 5,
      'post-slugs' => 'post-slug',
      'include' => 'include-slug',
      'tag' => 'tag-slug',
      'offset' => 1,
      'null' => null
    ];
    $this->postRest->setQueryAtts($query);
    $queryString = $this->postRest->buildQuery();
    // the order of the query string responds to the order of elements in the query array
    expect($queryString)->toBe('author-name=author-test-slug&category-name=category-test-slug&offset=1&posts_per_page=5&slug=post-slug&include=include-slug&tag=tag-slug');
    $this->postRest->setQueryAtts([]);
  });

  it('it uses the `CacheService` to set a transient ID', function () {
    $this->cacheService->shouldReceive('transientID')
      ->once()
      ->with('https://test.com?test')
      ->andReturn('utp-test');

    $this->requester->testtransientID('https://test.com?test');
  });

  it('uses the `CacheService` to cache a response', function () {
    $this->cacheService->shouldReceive('cacheResponse')
      ->once()
      ->with($this->goodResponseData, 'utp-test')
      ->andReturn($this->goodResponseData);

    $this->requester->testCacheResponse($this->goodResponseData, 'utp-test');
  });

  it('sets deprecated school/college slugs to regular post slugs', function () {
    $atts = [
      'sc-post-slugs' => 'test-slug'
    ];

    $testSlug = $this->requester->testSetSlugs($atts);
    expect($testSlug)->toBe('test-slug');
  });
});
