<?php

uses(QueriesTestCase::class);

use UCTP\Queries\CategoryREST;
use UCTP\Services\ResponseData;

describe('Category REST: Unit', function() {
  it('builds the query from parameters', function() {
    $queryAtts = $this->categoryRest->getQueryAtts();
    $this->categoryRest->shouldReceive('filterQuery')
      ->once()
      ->with($queryAtts)
      ->andReturn([
        'per_page' => 100,
        'page' => 1
      ]);
    $queryParams = $this->categoryRest->testBuildQuery();
    expect($queryParams)->toBe('per_page=100&page=1');
  });

  it('sets an arbitrary route', function() {
    $route = 'https://test.com?test';
    $route = $this->categoryRest->setRoute($route);
    expect($route)->toBe('https://test.com?test');
  });

  it('sets a route with a query string', function() {
    $this->categoryRest->shouldReceive('buildQuery')
      ->once()
      ->andReturn('per_page=100&page=1');
    $route = $this->categoryRest->setRoute();
    expect($route)->toBe('https://dev.today.uconn.edu/wp-rest/wp/v2/categories?per_page=100&page=1');
  });

  it('makes a request and returns `ResponseData`', function() {
    $this->categoryRest->shouldReceive('setRoute')
      ->once()
      ->andReturn('https://dev.today.uconn.edu/wp-rest/wp/v2/categories?per_page=100&page=1');

    $this->categoryRest->shouldReceive('doCurlRequest')
      ->once()
      ->with('https://dev.today.uconn.edu/wp-rest/wp/v2/categories?per_page=100&page=1')
      ->andReturn($this->goodResponseData);
    $response = $this->categoryRest->handleRequest();
    $isResponseData = $response instanceof ResponseData;
    expect($isResponseData)->toBe(true);
  });

  it('returns injected headers if they are set', function() {
    $this->categoryRest->setHeaders([
      'X-WP-TotalPages' => 10
    ]);
    $result = $this->categoryRest->testGetHeaders($this->categoryRest->route);
    expect($result)->toBe([
      'X-WP-TotalPages' => 10
    ]);
    $this->categoryRest->setHeaders([]);
  });

  it('returns an empty array for an invalid route', function() {
    $result = $this->categoryRest->testGetHeaders('notaurl');
    expect($result)->toBe([]);
  });

  it('returns `0` if the header is not set', function() {
    $this->categoryRest->route = $this->postRest->route;

    $this->categoryRest->shouldReceive('getHeaders')
      ->once()
      ->with($this->postRest->route)
      ->andReturn([]);
    
    $result = $this->categoryRest->getNumOfCatPages();
    expect($result)->toBe(0);
  });

  it('requires an associative array format for headers', function(){
    $this->categoryRest->route = $this->postRest->route;

    $this->categoryRest->shouldReceive('getHeaders')
      ->once()
      ->with($this->postRest->route)
      ->andReturn([
        '0' => 'X-WP-TotalPages: 10'
      ]);
    
    $result = $this->categoryRest->getNumOfCatPages();
    expect($result)->toBe(0);
  });

  it('gets the number of category pages', function() {
    $this->categoryRest->route = $this->postRest->route;

    $this->categoryRest->shouldReceive('getHeaders')
      ->once()
      ->with($this->postRest->route)
      ->andReturn([
        'X-WP-TotalPages' => 10
      ]);
    
    $result = $this->categoryRest->getNumOfCatPages();
    expect($result)->toBe(10);
  });
})->covers(CategoryREST::class);