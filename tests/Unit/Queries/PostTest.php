<?php

uses(QueriesTestCase::class);

use Brain\Monkey\Functions;
use UCTP\Services\ResponseData;

describe('PostREST: Unit', function () {
  it('gets the query atts', function () {
    $atts = $this->postRest->getQueryAtts();
    expect($atts)->toBe($this->queryAtts);
  });

  it('sets the query atts', function () {
    $atts = $this->postRest->setQueryAtts(['per_page' => 5]);
    expect($atts)->toBe(['per_page' => 5]);
    $this->postRest->setQueryAtts([]);
  });

  it('sets the query by normalizing the shortcode atts with the WP REST API params', function () {
    $queryAtts = [
      'author' => 'test-author',
      'category' => 'test-category',
      'offset' => 0,
      'number-of-posts' => 5,
      'post-slugs' => 'test-slug',
      'include' => 'test-include',
      'tag' => 'test-tag',
      'search' => 'test-search'
    ];

    $this->postRest->setQueryAtts($queryAtts);
    $setQuery = $this->postRest->testSetQuery();
    expect($setQuery)
      ->toMatchArray([
        'author-name' => 'test-author',
        'category-name' => 'test-category',
        'offset' => 0,
        'posts_per_page' => 5,
        'slug' => 'test-slug',
        'include' => 'test-include',
        'tag' => 'test-tag',
        'search' => 'test-search'
      ]);
    $this->postRest->setQueryAtts([]);
  });

  it('sets the route to an arbitrary value if passed an argument', function () {
    $route = $this->postRest->setRoute('https://test.com?test');
    expect($route)->toBe('https://test.com?test');
  });

  it('sets the route to the URL with the query string', function () {

    $this->postRest->shouldReceive('buildQuery')
    ->once()
      ->andReturn('number-of-posts=4&offset=0');

    $route = $this->postRest->setRoute();
    expect($route)->toBe('https://dev.today.uconn.edu/wp-rest/wp/v2/posts?number-of-posts=4&offset=0&_embed');
  });

  it('handles bad transient data by attempting a new request', function () {
    $getTransientCount = 0;

    $this->postRest->shouldReceive('setRoute')
    ->once()
      ->andReturn('https://test.com?test');

    $this->postRest->shouldReceive('doTransientID')
    ->once()
      ->with('https://test.com?test')
      ->andReturn('uctoday-test');

    Functions\stubs([
      'get_transient' => function () use (&$getTransientCount) {
        $getTransientCount++;
        if ($getTransientCount === 1) {
          return false;
        } else {
          return $this->goodResponseData;
        }
      },
      'delete_transient' => true
    ]);

    $this->postRest->shouldReceive('doCurlRequest')
    ->once()
      ->with('https://test.com?test')
      ->andReturn($this->goodResponseData);

    $this->postRest->shouldReceive('cacheResponse')
    ->once()
      ->with($this->goodResponseData, 'uctoday-test')
      ->andReturn($this->goodResponseData);

    $response = $this->postRest->handleRequest();
    expect($response)->toBe($this->goodResponseData);
  });

  it('handles an valid transient with no post data by making a new request without caching', function () {

    $this->postRest->shouldReceive('setRoute')
    ->once()
      ->andReturn('https://dev.today.uconn.edu/wp-rest/wp/v2/posts?category-name=today-homepage&posts_per_page=4&offset=0&_embed');

    $this->postRest->shouldReceive('doTransientID')
    ->once()
      ->with('https://dev.today.uconn.edu/wp-rest/wp/v2/posts?category-name=today-homepage&posts_per_page=4&offset=0&_embed')
      ->andReturn('uctoday-test');

    Functions\when('get_transient')
      ->justReturn(new ResponseData(
        false,
        [],
        200
      ));

    $this->postRest->shouldReceive('doCurlRequest')
    ->once()
      ->with('https://dev.today.uconn.edu/wp-rest/wp/v2/posts?category-name=today-homepage&posts_per_page=4&offset=0&_embed')
      ->andReturn($this->goodResponseData);

    $this->postRest->shouldReceive('cacheResponse')
    ->never();

    $this->postRest->handleRequest();
  });
});