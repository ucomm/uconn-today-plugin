<?php

uses(QueriesTestCase::class);

use Brain\Monkey\Functions;
use UCTP\Services\ResponseData;

describe('Search REST: Unit', function () {
  it('sets search terms for a search query and replaces spaces with +', function () {
    $search = $this->searchRest->testSetSearch('Test search');
    expect($search)->toBe('test+search');
  });

  it('builds a query string for the search endpoint', function() {
    $this->searchRest->shouldReceive('setSearch')
      ->once()
      ->with('Test search')
      ->andReturn('test+search');

    $this->searchRest->shouldReceive('filterQuery')
      ->once()
      ->with([
        'number-of-posts' => 4,
        'offset' => 0,
        'search' => 'test+search'
      ])
      ->andReturn([
        'number-of-posts' => 4,
        'offset' => 0,
        'search' => 'test+search'
      ]);

    $queryString = $this->searchRest->buildQuery();
    expect($queryString)->toBe('number-of-posts=4&offset=0&search=test%2Bsearch');
  });

  it('sets an arbitrary route', function() {
    $route = $this->searchRest->setRoute('https://test.com?test');
    expect($route)->toBe('https://test.com?test');
  });

  it('sets the route with a query', function() {
    $this->searchRest->shouldReceive('buildQuery')
      ->once()
      ->andReturn('number-of-posts=4&offset=0&search=test%2Bsearch');

    $route = $this->searchRest->setRoute();
    expect($route)->toBe('https://dev.today.uconn.edu/wp-rest/wp/v2/search?number-of-posts=4&offset=0&search=test%2Bsearch');
  });

  it('handles the request', function() {
    $this->searchRest->shouldReceive('setRoute')
      ->once()
      ->andReturn('https://test.com?search=test');

    $this->searchRest->shouldReceive('doCurlRequest')
      ->once()
      ->with('https://test.com?search=test')
      ->andReturn($this->goodResponseData);

    $response = $this->searchRest->handleRequest();
    $isResponseData = $response instanceof ResponseData;
    expect($isResponseData)->toBeTrue();
  });
});