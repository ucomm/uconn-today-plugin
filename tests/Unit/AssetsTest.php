<?php

uses(AssetsTestCase::class);

use UCTP\Assets\Loader;
use Brain\Monkey\Functions;

describe('Loader: Unit', function() {
  it('gets the asset handle', function() {
    $handle = $this->loader->getHandle();
    expect($handle)->toBe('uctoday-plugin');
  });

  it('sets the asset handle', function() {
    $this->loader->setHandle('new-handle');
    $handle = $this->loader->getHandle();
    expect($handle)->toBe('new-handle');
    $this->loader->setHandle('uctoday-plugin');
  });

  it('gets the admin asset handle', function() {
    $handle = $this->loader->getAdminHandle();
    expect($handle)->toBe('uctoday-plugin-admin');
  });

  it('sets the admin asset handle', function() {
    $this->loader->setAdminHandle('new-handle');
    $handle = $this->loader->getAdminHandle();
    expect($handle)->toBe('new-handle');
    $this->loader->setAdminHandle('uctoday-plugin-admin');
  });

  it('gets the build directory for localhost', function() {
    $dir = $this->loader->getBuildDir();
    expect($dir)->toBe('dev-build');
  });

  it('gets the build directory for production', function() {
    Functions\when('wp_get_environment_type')->justReturn('production');
    $loader = new Loader($this->shortcodeSlug);
    $dir = $loader->getBuildDir();
    expect($dir)->toBe('build');
  });

  it('returns false if the content does not have a shortcode', function() {
    $post = new WP_Post($this->postData);

    Functions\expect('has_shortcode')
      ->once()
      ->with($post->post_content, $this->shortcodeSlug)
      ->andReturn(false);

    $hasShortcode = $this->loader->contentHasShortcode($post);

    expect($hasShortcode)->toBeFalse(); 
  });

  it('returns true if the content has a shortcode', function() {
    $this->postData->post_content = '[UC-Today-Test]';
    $post = new WP_Post($this->postData);

    Functions\expect('has_shortcode')
      ->once()
      ->with($post->post_content, $this->shortcodeSlug)
      ->andReturn(true);

    $hasShortcode = $this->loader->contentHasShortcode($post);
    expect($hasShortcode)->toBeTrue();
  });

  it('returns false if SiteOrigin is not active', function() {
    $post = new WP_Post($this->postData);
    Functions\when('is_plugin_active')
      ->justReturn(false);
    $soStatus = $this->loader->checkSiteOriginStatus($post);
    expect($soStatus)->toBeFalse();
  });

  it('returns false if there is no SiteOrigin panel data', function() {
    $post = new WP_Post($this->postData);
    Functions\stubs([
      'is_plugin_active' => false,
      'get_post_meta' => false
    ]);
    $soStatus = $this->loader->checkSiteOriginStatus($post);
    expect($soStatus)->toBeFalse();
  });

  it('returns false if there are no UCToday widgets in the SO data', function() {
    $post = new WP_Post($this->postData);
    Functions\stubs([
      'is_plugin_active' => true,
      'get_post_meta' => [
        'widgets' => [
          [
            'panels_info' => 'nothing here'
          ]
        ]
      ]
    ]);
    $soStatus = $this->loader->checkSiteOriginStatus($post);
    expect($soStatus)->toBeFalse();
  });

  it('returns true if there is an ative UCT widget in the SO data', function() {
    $post = new WP_Post($this->postData);
    Functions\stubs([
      'is_plugin_active' => true,
      'get_post_meta' => [
        'widgets' => [
          [
            'panels_info' => [
              'UCTP\Frontend\UCTWidget'
            ]
          ]
        ]
      ]
    ]);
    $soStatus = $this->loader->checkSiteOriginStatus($post);
    expect($soStatus)->toBeTrue();
  })->skip('unsure how to mock `array_search`');
});