# Unit Tests

Test individual "units" of functionality. For instance, test that each method of a class behaves as expected.