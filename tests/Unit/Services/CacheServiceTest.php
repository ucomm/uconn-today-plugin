<?php

uses(ServicesTestCase::class);

use Brain\Monkey\Functions;

describe('Cache Service: Unit', function () {
  it('gets the cache prefix', function () {
    $prefix = $this->cacheService->getTransientPrefix();
    expect($prefix)->toBe('uctoday-');
  });

  it('geets the cache duration', function () {
    $duration = $this->cacheService->getCacheDuration();
    expect($duration)->toBe(6 * HOUR_IN_SECONDS);
  });

  it('sets the transient ID with `empty-query` if there is no query', function () {
    $route = 'https://test.com';
    $transientID = $this->cacheService->transientID($route);
    expect($transientID)->toBe('uctoday-empty-query');
  });

  it('sets the transient ID substituting `=&+` with `_`', function () {
    $route = 'https://test.com?test=1&another=2+3';
    $transientID = $this->cacheService->transientID($route);
    expect($transientID)->toBe('uctoday-test_1_another_2_3');
  });

  it('returns `false` if a transient is not set', function () {
    Functions\stubs([
      'get_option' => [],
      'set_transient' => false
    ]);

    $transient = $this->cacheService->cacheResponse($this->goodResponseData, 'uctoday-test');
    expect($transient)->toBeFalse();
  });

  it('returns a cached transient if it is set', function () {
    Functions\stubs([
      'get_option' => [],
      'set_transient' => true,
      'get_transient' => $this->goodResponseData
    ]);

    $transient = $this->cacheService->cacheResponse($this->goodResponseData, 'uctoday-test');
    expect($transient)->toBe($this->goodResponseData);
  });

  it('returns `false` if the cache doesn\'t clear by force', function () {
    Functions\stubs([
      'is_multisite' => false,
      'get_blog_details' => (object) ['blog_id' => 1],
      'current_user_can' => false
    ]);

    $didClear = $this->cacheService->forceClearCache();
    expect($didClear)->toBeFalse();
  });

  it('clears the UCT cache for single sites', function () {
    global $wpdb;
    $mockWpdb = Mockery::mock('wpdb');
    $wpdb = $mockWpdb;

    Functions\stubs([
      'is_multisite' => false,
      'get_blog_details' => (object) ['blog_id' => 1],
      'current_user_can' => true
    ]);

    $mockWpdb->shouldReceive('prepare')
    ->once()
      ->with('DELETE FROM wp_options WHERE option_name LIKE %s;', '%_uctoday-%')
      ->andReturn('DELETE FROM wp_options WHERE option_name LIKE %_uctoday-%');

    $mockWpdb->shouldReceive('esc_like')
    ->once()
      ->with('_uctoday-')
      ->andReturn('_uctoday-');

    $mockWpdb->shouldReceive('query')
    ->once()
      ->with('DELETE FROM wp_options WHERE option_name LIKE %_uctoday-%')
      ->andReturn(10);

    $didClear = $this->cacheService->forceClearCache();
    expect($didClear)->not()->toBeFalse();
  });

  it('clears the UCT cache for multisite installations', function () {
    global $wpdb;
    $mockWpdb = Mockery::mock('wpdb');
    $wpdb = $mockWpdb;

    Functions\stubs([
      'is_multisite' => true,
      'get_blog_details' => (object) ['blog_id' => 2],
      'current_user_can' => true
    ]);

    $mockWpdb->shouldReceive('prepare')
    ->once()
      ->with('DELETE FROM wp_2_options WHERE option_name LIKE %s;', '%_uctoday-%')
      ->andReturn('DELETE FROM wp_2_options WHERE option_name LIKE %_uctoday-%');

    $mockWpdb->shouldReceive('esc_like')
    ->once()
      ->with('_uctoday-')
      ->andReturn('_uctoday-');

    $mockWpdb->shouldReceive('query')
    ->once()
      ->with('DELETE FROM wp_2_options WHERE option_name LIKE %_uctoday-%')
      ->andReturn(10);

    $didClear = $this->cacheService->forceClearCache();
    expect($didClear)->not()->toBeFalse();
  });
});