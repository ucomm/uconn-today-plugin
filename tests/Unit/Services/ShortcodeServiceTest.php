<?php

uses(ServicesTestCase::class);

use Brain\Monkey\Functions;

describe('Shortcode Service: Unit', function() {
  it('gets the shortcode slug `UC-Today`', function() {
    $slug = $this->shortcodeService->getShortcodeSlug();
    expect($slug)->toBe('UC-Today');
  });
  it('gets the default shortcode attributes `array`', function() {
    $atts = $this->shortcodeService->getDefaultAtts();
    expect($atts)
      ->toBe([
        'author' => null,
        'category' => null,
        'columns' => '4',
        'offset' => '',
        'pictures' => false,
        'number-of-posts' => '4',
        'post-slugs' => null,
        'read-more-text' => '',
        'search' => null,
        'include' => null,
        'show-excerpt' => false,
        'show-date' => false,
        'tag' => null,
        'debug' => false,
        'safe-fetch' => true,
        'style' => ''
      ]);
  });

  it('gets public attributes', function() {
    $atts = $this->shortcodeService->getPublicAtts();
    expect($atts)->toBe([]);
  });

  it('sanitizes atts with `sanitize_text_field`', function() {
    $cleanAtts = $this->shortcodeService->testSanitizeAtts($this->badAtts);
    expect($cleanAtts)
      ->toBe([
        'author' => '',
        'category' => '>test',
        'search' => 'test extra whitespace'
      ]);
  });

  it('sets the attributes for the shortcode', function() {
    $slug = $this->shortcodeService->getShortcodeSlug();
    $defaultAtts = $this->shortcodeService->getDefaultAtts();
    // pretend that someone passed in these attributes
    $testAtts = [
      'category' => 'arts',
      'columns' => '3',
      'number-of-posts' => '6'
    ];
    // merge the default attributes with the test attributes as the return value
    $returnedAtts = $this->goodAtts;
    $returnedAtts['category'] = 'arts';
    $returnedAtts['columns'] = '3';
    $returnedAtts['number-of-posts'] = '6';

    // expect that `shortcode_atts` is called with the default attributes, the test attributes, and the slug
    Functions\expect('shortcode_atts')
      ->once()
      ->with(
        $defaultAtts,
        $testAtts,
        $slug
      )
      ->andReturn($returnedAtts);

    $this->shortcodeService->setAtts($testAtts);
  });
});