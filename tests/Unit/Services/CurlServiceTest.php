<?php

uses(ServicesTestCase::class);

use UCTP\Services\ResponseData;

describe('Curl Service: Unit', function() {
  it('returns an empty array if `$rawData` is not a string', function() {
    $data = $this->curlService->testDecodeRawData(false);
    expect($data)->toBe([]);
  });

  it('returns an empty array if `$rawData` is not JSON', function() {
    $data = $this->curlService->testDecodeRawData('test');
    expect($data)->toBe([]);
  });

  it('decodes a json string into an array', function() {
    $data = $this->curlService->testDecodeRawData('{"test": "data"}');
    expect($data)->toBe(['test' => 'data']);
  });

  it('decodes a complex json string into an array', function() {
    // make sure that the decoder can handle multiple types of response data
    $complexData = [
      'test' => 'data',
      'another' => [1, 2, 3],
      'encoded_obj' => (object) ['test' => 'object']
    ];
    $complexDataString = json_encode($complexData);
    $data = $this->curlService->testDecodeRawData($complexDataString);
    expect($data)->toBeArray()
      ->and($data)
      ->toHaveKeys([
        'test',
        'another',
        'encoded_obj'
      ])
      ->and($data['test'])
      ->toBe('data')
      ->and($data['another'])
      ->toBe([1, 2, 3])
      ->and($data['encoded_obj'])
      ->toBeObject();
  });

  it('returns `ResponseData` with an initialization error if curl fails', function() {
    $this->curlService->ch = false;

    $didInitCurl = $this->curlService->testInitCurl('https://test.com');
    $isResponseData = $didInitCurl instanceof ResponseData;
    expect($isResponseData)->toBeTrue()
      ->and($didInitCurl->error)
      ->toBeTrue()
      ->and($didInitCurl->errorCode)
      ->toBe(ResponseData::CURL_INIT_ERROR);
  });

  it('returns a `curl` handle if curl initializes', function() {
    $this->curlService->ch = 'test';

    $this->curlService->shouldReceive('setOption')
      ->times(3)
      ->andReturn(true);

    $didInitCurl = $this->curlService->testInitCurl('https://test.com');
    expect($didInitCurl)->toBe('test');
  });

  it('returns `ResponseData` with a `CURL_TIMEOUT_ERROR` if curl timesout', function() {
    $this->curlService->shouldReceive('errno')
      ->once()
      ->andReturn(CURLE_OPERATION_TIMEOUTED);

    $data = $this->curlService->testHandleCurlResponse('test', 'test', 200);
    $isResponseData = $data instanceof ResponseData;

    expect($isResponseData)
      ->toBeTrue()
      ->and($data->error)
      ->toBeTrue()
      ->and($data->response)
      ->toBeNull()
      ->and($data->httpCode)
      ->toBe(0)
      ->and($data->errorCode)
      ->toBe(ResponseData::CURL_TIMEOUT_ERROR);
  });

  it('returns `ResponseData` with a `NETWORK_ERROR` if the `HTTP` code is greater than 400', function() {
    $notFoundData = [
      'error' => 'not found'
    ];
    $this->curlService->shouldReceive('errno')
      ->once()
      ->andReturn(0);

    $data = $this->curlService->testHandleCurlResponse(
      'test', 
      json_encode($notFoundData),
      404
    );

    $isResponseData = $data instanceof ResponseData;
    expect($isResponseData)
      ->toBeTrue()
      ->and($data->error)
      ->toBeTrue()
      ->and($data->response)
      ->toBe($notFoundData)
      ->and($data->httpCode)
      ->toBe(404);
  });

  it('returns `ResponseData` with `INVALID_RESPONSE` if the `$rawData` is `false`', function() {
    $this->curlService->shouldReceive('errno')
      ->once()
      ->andReturn(0);

    $data = $this->curlService->testHandleCurlResponse('test', false, 200);
    $isResponseData = $data instanceof ResponseData;
    expect($isResponseData)
      ->toBeTrue()
      ->and($data->error)
      ->toBeTrue()
      ->and($data->response)
      ->toBeFalse()
      ->and($data->httpCode)
      ->toBe(200)
      ->and($data->errorCode)
      ->toBe(ResponseData::INVALID_RESPONSE);
  });

  it('returns `ResponseData` with the decoded response data if everything is good', function() {
    $goodData = [
      'test' => 'data'
    ];

    $this->curlService->shouldReceive('errno')
      ->once()
      ->andReturn(0);

    $data = $this->curlService->testHandleCurlResponse(
      'test', 
      json_encode($goodData),
      200
    );

    $isResponseData = $data instanceof ResponseData;
    expect($isResponseData)
      ->toBeTrue()
      ->and($data->error)
      ->toBeFalse()
      ->and($data->response)
      ->toBe($goodData)
      ->and($data->httpCode)
      ->toBe(200);
  });
});