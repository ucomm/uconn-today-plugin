<?php

uses(ServicesTestCase::class);

use UCTP\Services\ResponseData;

describe('ResponseData: Unit', function () {
  it('returns `-1` if the error code is invalid', function () {
    // Create mock with constructor args
    $mock = Mockery::mock('UCTP\Services\ResponseData')
      ->makePartial()
      ->shouldAllowMockingProtectedMethods();

    // Set expectations before constructor call
    $mock->shouldReceive('validateErrorCode')
      ->once()
      ->with(10)
      ->andReturn(ResponseData::INVALID_ERROR_CODE);

    // Call constructor manually
    $mock->__construct(true, 'test', 200, 10);

    expect($mock->errorCode)->toBe(ResponseData::INVALID_ERROR_CODE);
  });
});