<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

use UCTP\Assets\Loader;
use UCTP\Assets\ScriptLoader;
use UCTP\Assets\StyleLoader;
use UCTP\Frontend\Shortcode;

class AssetsTestCase extends TestCase
{

    // Adds Mockery expectations to the PHPUnit assertions count.
    use MockeryPHPUnitIntegration;

    public Loader $loader;
    public ScriptLoader $scriptLoader;
    public StyleLoader $styleLoader;
    public Shortcode $shortcode;
    public string $shortcodeSlug;
    public object $postData;

    protected function setUp(): void
    {
        parent::setUp();
        Monkey\setUp();
        $this->shortcodeSlug = 'UC-Today-Test';
        $this->shortcode = Mockery::mock(Shortcode::class);
        $this->loader = new Loader($this->shortcodeSlug);
        $this->scriptLoader = new ScriptLoader($this->shortcodeSlug);
        $this->styleLoader = new StyleLoader($this->shortcodeSlug);
        $this->postData = (object) [
            'ID' => 1,
            'post_title' => 'Test Post',
            'post_name' => 'test-post',
            'post_content' => 'Test Content',
            'post_status' => 'publish'
        ];
    }

    protected function tearDown(): void
    {
        Monkey\tearDown();
        parent::tearDown();
        wp_delete_post($this->postData->ID, true);
    }
}