<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;
use UCTP\Admin\Settings\Options as PluginOptions;
use UCTP\Admin\Settings\SettingsPage;
use UCTP\Services\CacheService;
use UCTP\Services\NotificationService;

class SettingsTestCase extends TestCase
{

    // Adds Mockery expectations to the PHPUnit assertions count.
    use MockeryPHPUnitIntegration;
    public PluginOptions $pluginOptions;
    public SettingsPage $settingsPage;

    protected function setUp(): void
    {
        parent::setUp();
        Monkey\setUp();

        $this->pluginOptions = new PluginOptions('uctoday_plugin');
        $this->settingsPage = Mockery::mock(SettingsPage::class, [new CacheService(), new NotificationService('', '')])->makePartial();
    }

    protected function tearDown(): void
    {
        Monkey\tearDown();
        parent::tearDown();
    }
}