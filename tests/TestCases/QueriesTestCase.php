<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;
use UCTP\Queries\Requester;
use UCTP\Queries\PostREST;
use UCTP\Queries\SearchREST;
use UCTP\Queries\CategoryREST;
use UCTP\Services\CurlService;
use UCTP\Services\CacheService;
use UCTP\Services\ResponseData;

class TestRequester extends Requester {
    public function __construct(
        array $queryAtts = [],
        string $endpoint = '/posts',
        ?CurlService $curlService = null,
        ?CacheService $cacheService = null
    ) {
        parent::__construct($queryAtts, $endpoint, $curlService, $cacheService);
    }

    public function testtransientID(string $route): string {
        return $this->transientID($route);
    }

    public function testSetSlugs(array $atts): ?string {
        return $this->setSlugs($atts);
    }

    public function testCacheResponse(ResponseData $data, string $transientID) {
        return $this->cacheResponse($data, $transientID);
    }

    public function testDoCurlRequest(string $route): ResponseData {
        return $this->doCurlRequest($route);
    }

    public function testFilterQuery(array $query): array {
        return $this->filterQuery($query);
    }

    public function testBuildQuery(): string {
        return $this->buildQuery();
    }
}

class TestPostREST extends PostREST {
    public $safeFetch;
    public $queryString;
    public $route;
    public $usePictures;
    public function __construct(
        bool $safeFetch,
        array $queryAtts = [],
        bool $usePictures = true,
        string $endpoint = '/posts'
    ) {
        $this->safeFetch = $safeFetch;
        $this->queryAtts = $queryAtts;
        $this->usePictures = $usePictures;
        $this->endpoint = $endpoint;
        parent::__construct($this->safeFetch, $this->queryAtts, $this->usePictures, $this->endpoint);
    }

    public function testSetQuery(): array {
        return $this->setQuery();
    }
}

class TestSearchREST extends SearchREST {
    public function __construct(
        array $queryAtts = [],
        string $endpoint = '/search'
    ) {
        parent::__construct($queryAtts, $endpoint);
    }

    public function testSetSearch(string $search): string {
        return $this->setSearch($search);
    }
}

class TestCategoryREST extends CategoryREST {
    public function __construct(
        array $queryAtts = [],
        string $endpoint = '/categories'
    ) {
        parent::__construct($queryAtts, $endpoint);
    }

    public function testBuildQuery(): string {
        return $this->buildQuery();
    }

    public function testGetHeaders(string $route): array {
        return $this->getHeaders($route);
    }
}

class QueriesTestCase extends TestCase
{

    // Adds Mockery expectations to the PHPUnit assertions count.
    use MockeryPHPUnitIntegration;
    public CurlService $curlService;
    public CacheService $cacheService;
    public ResponseData $goodResponseData;
    public ResponseData $emptyResponseData;
    public ResponseData $badResponseData;
    public TestRequester $requester;
    public TestPostREST $postRest;
    public TestSearchREST $searchRest;
    public TestCategoryREST $categoryRest;
    public array $queryAtts;

    protected function setUp(): void
    {
        parent::setUp();
        Monkey\setUp();
        $this->goodResponseData = Mockery::mock(new ResponseData(
                false,
                ['test' => 'data'],
                200
            )
        );

        $this->emptyResponseData = Mockery::mock(new ResponseData(
                false,
                [],
                200
            )
        );

        $this->badResponseData = Mockery::mock(new ResponseData(
                true,
                [],
                404,
                1
            )
        );

        $this->queryAtts = [
            'number-of-posts' => 4,
            'offset' => 0
        ];

        // mock the CurlService and CacheService class since they're not being tested here
        $this->curlService = Mockery::mock(CurlService::class);
        $this->cacheService = Mockery::mock(CacheService::class);

        // create a new instance of the Requester class
        $this->requester = Mockery::mock(
            TestRequester::class, 
            [
                [], 
                '/posts', 
                $this->curlService, 
                $this->cacheService
            ]
        )->makePartial()
        ->shouldAllowMockingProtectedMethods();

        // create a new instance of the PostREST class
        // ensure that it's a partial mock so to mock protected methods
        $this->postRest = Mockery::mock(TestPostREST::class, [ 
                true,
                $this->queryAtts,
            ])
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $this->searchRest = Mockery::mock(TestSearchREST::class, [ 
                [
                    'number-of-posts' => 4,
                    'offset' => 0,
                    'search' => 'Test search'
                ],
                '/search'
            ])
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();
        $this->categoryRest = Mockery::mock(TestCategoryREST::class, [ 
                [
                    'per_page' => 100,
                    'page' => 1
                ],
                '/categories'
            ])
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();
        // set the route for the PostREST class so that we don't accidentally hit the live API
        $this->postRest->route = 'https://test.com?test';
    }

    protected function tearDown(): void
    {
        Monkey\tearDown();
        parent::tearDown();
    }
}