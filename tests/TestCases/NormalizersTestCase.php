<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

use UCTP\Services\DataNormalizer;
use UCTP\Services\PostDataNormalizer;
use UCTP\Services\SearchDataNormalizer;

class TestPostDataNormalizer extends PostDataNormalizer {
  public function testSortStoriesByDate(array $storyData): array {
    return $this->sortStoriesByDate($storyData);
  }

  public function testSetFeaturedImage($story): ?string {
    return $this->setFeaturedImage($story);
  } 

  public function testSetFeaturedImageAlt($story): string {
    return $this->setFeaturedImageAlt($story);
  }

  public function testSetStoryDate($date): string {
    return $this->setStoryDate($date);
  }

  public function testSetPublicCategories(?array $postTerms): array {
    return $this->setPublicCategories($postTerms);
  }

  public function testCreateNormalizedStory($story): array {
    return $this->createNormalizedStory($story);
  }
}


class NormalizersTestCase extends TestCase {

  // Adds Mockery expectations to the PHPUnit assertions count.
  use MockeryPHPUnitIntegration;
  public DataNormalizer $dataNormalizer;
  public TestPostDataNormalizer $postDataNormalizer;
  public SearchDataNormalizer $searchDataNormalizer;
  public array $storyData;
  // this is a separate property meant to mock the data that would be returned from the WP REST API
  public array $categoryData;
  public array $normalizedCats;
  public stdClass $featuredImageObject;
  public array $postTerms;

  protected function setUp(): void
  {
    parent::setUp();
    Monkey\setUp();
    $this->dataNormalizer = new DataNormalizer([]);
    $this->postDataNormalizer = Mockery::mock(
        TestPostDataNormalizer::class, [[]]
      )->makePartial()
      ->shouldAllowMockingProtectedMethods();

    $this->searchDataNormalizer = new SearchDataNormalizer([]);

    $this->postTerms = [
      (object) [
        'id' => 1,
        'name' => 'Category',
        'slug' => 'category',
        'link' => 'https://test.com/category'
      ]
    ];
    
    $this->featuredImageObject =
    (object) [
      '_embedded' => [
        'wp:featuredmedia' => [
          0 => [
            'media_details' => [
              'sizes' => [
                'medium_large' => ['source_url' => 'medium_large.jpg'],
                'medium' => ['source_url' => 'medium.jpg']
              ]
            ],
            'alt_text' => 'alt text'
          ]
        ]
      ]
    ];

    $this->storyData = [
      (object) [
        'id' => 1,
        'date' => '2024-11-11T13:34:57',
        'date_gmt' => '2024-11-11T13:34:57',
        'guid' => (object) [
          'rendered' => 'https://uconn.edu/story-1'
        ],
        'slug' => 'story-1',
        'status' => 'publish',
        'type' => 'post',
        'link' => 'https://uconn.edu/story-1',
        'title' => (object) [
          'rendered' => 'Story 1'
        ],
        'content' => (object) [
          'rendered' => 'Story 1 content'
        ],
        'excerpt' => (object) [
          'rendered' => 'Story 1 excerpt'
        ],
        '_embedded' => (object) [
          'wp:term' => $this->postTerms,
          'wp:featuredmedia' => [
            (object) [
              'id' => 1,
              'date' => '2024-11-11T13:34:57',
              'slug' => 'story-1-featured-image',
              'type' => 'attachment',
              'link' => 'https://uconn.edu/story-1-featured-image',
              'title' => (object) [
                'rendered' => 'Story 1 featured image'
              ],
              'alt_text' => 'Story 1 featured image alt text',
              'media_details' => (object) [
                'sizes' => (object) [
                  'medium' => (object) [
                    'source_url' => 'https://uconn.edu/story-1-featured-image-medium.jpg'
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ];
  }

  protected function tearDown(): void
  {
    Monkey\tearDown();
    parent::tearDown();
  }
}
