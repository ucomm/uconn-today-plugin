<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

use UCTP\Frontend\Shortcode;
use UCTP\Services\ShortcodeService;

class FrontendTestCase extends TestCase
{

    // Adds Mockery expectations to the PHPUnit assertions count.
    use MockeryPHPUnitIntegration;
    public Shortcode $shortcode;
    public ShortcodeService $shortcodeService;

    protected function setUp(): void
    {
        parent::setUp();
        Monkey\setUp();
        $this->shortcode = new Shortcode();
    }

    protected function tearDown(): void
    {
        Monkey\tearDown();
        parent::tearDown();
    }
}