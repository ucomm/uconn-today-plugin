<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

use UCTP\Services\ShortcodeService;
use UCTP\Services\CacheService;
use UCTP\Services\CurlService;
use UCTP\Services\ResponseData;

class TestResponseData extends ResponseData {
    public function testValidateErrorCode(int $code): int {
        return $this->validateErrorCode($code);
    }
}

class TestCurlService extends CurlService {
    public function testDecodeRawData($rawData): array {
        return $this->decodeRawData($rawData);
    }

    public function testInitCurl(string $url) {
        return $this->initCurl($url);
    }

    public function testHandleCurlResponse($ch, $rawData, int $httpCode): ResponseData {
        return $this->handleCurlResponse($ch, $rawData, $httpCode);
    }
}

class TestShortcodeService extends ShortcodeService {
    public function testSanitizeAtts(array $atts): array {
        return $this->sanitizeAtts($atts);
    }
}

class ServicesTestCase extends TestCase
{

    // Adds Mockery expectations to the PHPUnit assertions count.
    use MockeryPHPUnitIntegration;

    public CacheService $cacheService;
    public CurlService $curlService;
    public TestShortcodeService $shortcodeService;
    public ResponseData $goodResponseData;
    public ResponseData $badResponseData;
    public array $goodAtts;
    public array $badAtts;

    protected function setUp(): void
    {
        parent::setUp();
        Monkey\setUp();
        $this->cacheService = new CacheService();
        $this->curlService = Mockery::mock(TestCurlService::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods(); 
        $this->shortcodeService = Mockery::mock(TestShortcodeService::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();
        $this->goodResponseData = Mockery::mock(new ResponseData(
                false, 
                [ 'test' => 'data' ], 
                200
            )
        );
        $this->badResponseData = Mockery::mock(new ResponseData(
                true,
                [],
                404 
            )
        );

        $this->goodAtts = [
            'author' => 'test-author',
            'category' => 'test',
            'number-of-posts' => '8',
            'post-slugs' => 'test-post-1,test-post-2',
            'read-more-text' => 'Read more...',
            'search' => 'test-search',
            'include' => '1,2,3',
            'columns' => '4',
            'offset' => '',
            'pictures' => 'true',
            'show-excerpt' => 'true',
            'show-date' => 'true',
            'safe-fetch' => 'true',
            'category' => 'test',
            'style' => 'test-style'
        ];

        // attributes to test sanitize_text_field
        $this->badAtts = [
            'author' => '<script>alert("test")</script>',
            'category' => '>test%2B',
            'search' => '
                test extra whitespace  
            '
        ];
    }

    protected function tearDown(): void
    {
        Mockery::close();
        Monkey\tearDown();
        parent::tearDown();
    }
}