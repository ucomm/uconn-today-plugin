<?php

use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

use UCTP\Admin\Menus\AdminBar;

/**
 * Mock the WP_Admin_Bar class directly
 */
class WP_Admin_Bar {
  public function add_menu($args) {
    return $args;
  }
}

class AdminTestCase extends TestCase
{

  // Adds Mockery expectations to the PHPUnit assertions count.
  use MockeryPHPUnitIntegration;

  public WP_Admin_Bar $wpAdminBar;
  public AdminBar $adminBar;

  protected function setUp(): void
  {
    parent::setUp();
    Monkey\setUp();
    $this->wpAdminBar = Mockery::mock(WP_Admin_Bar::class)->makePartial();
    $this->adminBar = Mockery::mock(AdminBar::class)->makePartial();
  }

  protected function tearDown(): void
  {
    Monkey\tearDown();
    parent::tearDown();
  }
}
