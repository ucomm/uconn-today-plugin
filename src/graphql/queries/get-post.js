import { gql } from '@apollo/client'

/**
 * 
 * GraphQL query to get a single post based on its ID. NB - the ID is not the same as the database ID
 * 
 */
export const getPost = gql`
  query POST_QUERY ($id: String! $cursor: String) {
    post (id: $id idType: ID) {
      id
      databaseId
      title
      uri
      link
      content
      excerpt
      slug
      date
      author {
        node {
          uri
          name
        }
      }
      featuredImage {
        node {
          altText
          sourceUrl
          srcSet
          sizes
          caption
        }
      }
      postFormatVideoEmbedLink {
        videoUrl
      }
    }
  }
`