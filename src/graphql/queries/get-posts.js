import { gql } from '@apollo/client'

/**
 * 
 * Get a selection of posts by category slug. This query uses relay/cursor based pagination. See - https://relay.dev/graphql/connections.htm
 * 
 */
export const getPostsQuery = gql`
  query GET_POSTS (
    $first: Int
    $after: String
    $categorySlug: String
  ) {
    posts (
      where: {
        status: PUBLISH
        categoryName: $categorySlug
      }
      first: $first
      after: $after
    ) {
      pageInfo {
        endCursor
      }
      edges {
        cursor
        node {
          databaseId
          id
          title
          slug
          uri
          link
          date
          content
          excerpt
          author {
            node {
              uri
              name
            }
          }
          featuredImage {
            node {
              altText
              sourceUrl
              srcSet
              sizes
              caption
            }
          }
          postFormatVideoEmbedLink {
            videoUrl
          }
        }
      }
    }
  }
`