import { HttpLink, from } from '@apollo/client'
import fetch from 'unfetch'

const link = from([
  new HttpLink({ 
    uri: 'https://today.uconn.edu/wordpress/graphql', 
    fetch 
  })
])

export default link