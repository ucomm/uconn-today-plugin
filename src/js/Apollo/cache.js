import { InMemoryCache, defaultDataIdFromObject } from '@apollo/client/core';

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        /**
         * 
         * We don't want to make a separate request to the server for post data after the initial GET_POSTS request. Rather, we want to return the Post object referenced by the ID passed to the client. This is `post` referenced in the POST_QUERY graphql request. It only exists in the client.
         * 
         * See the following link for info on the `toReference` function - https://www.apollographql.com/docs/react/caching/advanced-topics/#cache-redirects-using-field-policy-read-functions
         * 
         * @param {*} _ 
         * @param object args from the query and the toReference function from apollo
         * 
         * @returns Object the post from the apollo cache
         * 
         */
        post(_, { args, toReference }) {
          return toReference({
            __typename: 'Post',
            id: args.id
          })
        },
        posts: {
          /**
           * See - https://www.apollographql.com/docs/react/caching/cache-field-behavior/#specifying-key-arguments
           * 
           * Needed to assist with pagination and being able to use multiple instances of the shortcode.
           * 
           * @param args not used
           * @param object context - includes the variables passed to the query
           * 
           * @returns string the category slug
           */
          keyArgs: ( __, context ) => {
            return context.variables.categorySlug
          },
        },
      },
    },
  }
})