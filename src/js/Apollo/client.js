import { ApolloClient } from '@apollo/client/core'

import { cache } from './cache'
import link from './link'

const apolloClient = new ApolloClient({
  link,
  cache
})

export default apolloClient