// polyfill stuff...
import 'core-js/es/object/assign'
import 'core-js/es/array/from'
import 'core-js/es/array/includes'
import 'core-js/es/promise'
import 'regenerator-runtime/runtime'

import React, { Component, Suspense } from 'react'
import { render } from 'react-dom'
import { ApolloProvider } from '@apollo/client'
import { AnalyticsProvider } from 'use-analytics'

import apolloClient from './Apollo/client'
import { analytics } from './utils/analytics'

// creates the JS app styles
import '../sass/app-styles.scss'
import ListLoader from './Components/ListLoader'

// lazy load the actual list component. this way if it's further down the page, it won't be needed until called
const ArticleList = React.lazy(() => import(
  /* webpackChunkName: "uct-article-list" */
  './Components/ArticleList'
))

const app_containers = document.querySelectorAll('.uctoday-plugin-app-container');

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      client: null,
      loaded: false,
      props: props
    }
  }

  componentDidMount() {
    this.setState({
      client: apolloClient,
      loaded: true
    })
  }

  render() {

    const { client, loaded, props } = this.state

    if (!loaded) {
      return <ListLoader />
    }

    return (
      <ApolloProvider client={client}>
        <AnalyticsProvider instance={analytics}>
          <Suspense fallback={<ListLoader />}>
            <ArticleList category={props.category} numPosts={props.numPosts} columns={props.columns} />
          </Suspense>
        </AnalyticsProvider>
      </ApolloProvider>
    )
  }
}

// support multiple instances of the shortcode being used
app_containers.forEach(ac => {
  if ( ac.dataset.category ) {
    const id_string = 'uctoday-plugin-app-' + ac.dataset.category;
    if ( !ac.dataset.numPosts ) {
      ac.dataset.numPosts = "4";
    }
    render(<App category={ac.dataset.category} numPosts={ac.dataset.numPosts} columns={ac.dataset.columns} />, document.getElementById(id_string));
  }
});