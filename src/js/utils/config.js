
/**
 * 
 * A defaults for social share config links/button
 * 
 */
export const socialShareConfig = {
  copy: {
    link: '',
    title: 'Copy article link'
  },
  email: {
    link: '',
    title: 'Email article'
  },
  twitter: {
    link: '',
    title: 'Share on Twitter'
  },
  linkedIn: {
    link: '',
    title: 'Share on LinkedIn'
  },
  facebook: {
    link: '',
    title: 'Share on Facebook'
  },
  reddit: {
    link: '',
    title: 'Share on Reddit'
  }
}

/**
 * 
 * @param object currentConfig the config object before the post data is known
 * @param object post the post data 
 * @returns object a merged object with the current config and link based on the post data
 * 
 */
export const createSocialConfig = (currentConfig, post) => {

  const encodedTitle = encodeURIComponent(post.title)

  return Object.assign({}, currentConfig, {
    copy: {
      ...currentConfig.copy,
      link: post.link
    },
    email: {
      ...currentConfig.email,
      link: `mailto:?subject=${post.title}&body=Check out this UConn Today article - ${post.link}`
    },
    twitter: {
      ...currentConfig.twitter,
      link: `https://twitter.com/intent/tweet?text=${encodedTitle}+${post.link}`
    },
    linkedIn: {
      ...currentConfig.twitter,
      link: `https://www.linkedin.com/sharing/share-offsite/?url=${post.link}`
    },
    facebook: {
      ...currentConfig.facebook,
      link: `https://www.facebook.com/dialog/share?app_id=1689478697951363&display=popup&href=${post.link}&redirect_uri=${post.link}`
    },
    reddit: {
      ...currentConfig.reddit,
      link: `https://www.reddit.com/submit?url=${post.link}&title=${encodedTitle}`
    }
  })
}