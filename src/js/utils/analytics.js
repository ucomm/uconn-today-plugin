// analytics provided by - https://getanalytics.io/

import Analytics from 'analytics'
import googleAnalytics from '@analytics/google-analytics'

const isProd = process.env.NODE_ENV === 'production' ? true : false

const debug = !isProd ? true : false

const nonProdHosts = [
  'localhost',
  'dev',
  'test'
]

const isProdHost = nonProdHosts.some(substring => window.location.hostname.includes(substring))

// creates a config for the analytics with the hardcoded value of the UCToday GA container
const analyticsConfig = {
  trackingId: 'UA-1427009-15',
  anonymizeIp: true,
}

// allow multiple analytics configuration for production
// https://getanalytics.io/plugins/google-analytics/#using-multiple-instances
if (window.location.host !== 'localhost') {
  analyticsConfig.instanceName = 'uctodayReaderInstance'
}


const plugins = [
  // send analytics to google
  googleAnalytics(analyticsConfig)
]

export const analytics = new Analytics({
  app: 'uct-gql-test',
  debug,
  plugins
})

/**
 * 
 * @param function tracker analytics hook to be used as a closure
 * @param object postData
 */
export const trackPage = (tracker, postData = {}) => {
  const title = !isProdHost ?
    `Test: ${window.uctodayPostData.siteTitle} - ${postData.title}` :
    `UCTPlugin: ${window.uctodayPostData.siteTitle} - ${postData.title}`
  tracker({
    title,
    url: window.location.href,
    path: postData.uri
  })
}

/**
 *
 * @param function tracker analytics hook to be used as a closure
 * @param object event data 
 */
export const trackEvent = (tracker, data) => {
  const eventName = !isProdHost ?
    `test-event-${data.eventName}` :
    `event-${data.eventName}`
    
  const category = !isProdHost ?
    `Test: ${data.category}` :
    `UCTPlugin: ${data.category}`

  const label = !isProdHost ?
    `Test: ${data.label}` :
    `UCTPlugin: ${data.label}`

  const trackerOpts = {
    category,
    label,
  }

  if (data.hasOwnProperty('value')) {
    trackerOpts.value = data.value
  }

  tracker(eventName, trackerOpts)
}