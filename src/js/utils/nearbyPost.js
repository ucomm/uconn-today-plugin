/**
 * 
 * Return data about the next or previous post
 * 
 * @param array posts - the posts
 * @param array args - client args
 * @param string direction - get the next or previous post
 * 
 * @returns object the nearby post
 */
export const nearbyPost = (posts, args, direction) => {

  const found = posts.findIndex(({ cursor }) => cursor === args.cursor)

  const nearby = direction === 'next' ? posts[found + 1] : posts[found - 1]

  const nearbyIdFull = nearby !== undefined && nearby.node !== undefined ?
    nearby.node.__ref :
    null

  const nearbyId = nearbyIdFull !== null ?
    nearbyIdFull.split(':') :
    null

  return {
    id: nearbyId !== null ? nearbyId[1] : '',
    cursor: nearby !== undefined && nearby.cursor !== null ? nearby.cursor : ''
  }
}