import { cache } from '../Apollo/cache';
import { nearbyPost } from './nearbyPost';


/**
 * This function does two things:
 * 1. Makes the non-extendable Apollo response object, extendable.
 * 2. Adds the critical previous/next post declarations to the post, allowing for navigation between multiple.
 * 
 * @param object the current post
 * @param object post data to provide context: the post ID, cursor, and category
 * 
 * @returns object a post with data modified to reflect its nearby posts
 * 
 */
export const addContextToPost = (post, postData) => {

    const args = {
        cursor: postData.cursor,
        id: postData.postID
    };

    const posts = cache.data.data.ROOT_QUERY['posts:' + postData.category].edges;
    const nearby_prev = nearbyPost(posts, args, 'previous');
    const nearby_next = nearbyPost(posts, args, 'next');

    const npost = Object.create(post);
    npost.previousPost = nearby_prev;
    npost.nextPost = nearby_next;

    return npost;
}