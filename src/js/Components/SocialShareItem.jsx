import React from 'react'
/**
 *
 *
 * @param object props
 * - `children`
 * - `config` object
 * - `popup` boolean
 * - `socialClick` function
 * 
 */
const SocialShareItem = ({ children, config, popup = true, socialClick }) => {
  
  return (
    <a 
      href={config.link} 
      title={config.title}
      target={popup ? 'popup': ''}
      onClick={() => socialClick(config.title)}
    >{children}</a>
  )
}

export default SocialShareItem