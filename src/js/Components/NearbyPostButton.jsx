import React, { useEffect } from 'react'
import { useApolloClient } from '@apollo/client'
import { useAnalytics } from 'use-analytics'

import { getPost } from '../../graphql/queries/get-post'
import { trackEvent } from '../utils/analytics'
import { addContextToPost } from '../utils/addContextToPost';

const NearbyPostButton = ({ type, postData, clickHandler, children }) => {

  const client = useApolloClient()
  const { track } = useAnalytics()

  const getNearbyPost = () => {
    const { post } = client.readQuery({
      query: getPost,
      variables: {
        cursor: postData.cursor,
        id: postData.postID
      }
    })

    const npost = addContextToPost(post, postData)

    return npost
  }

  /**
 * 
 * If an left/right arrow key is pressed and the type is wrong, immediately return. Otherwise get the appropriate post from the cache.
 * 
 * @param string key - the key that was pressed
 */
  const keyDownHandler = ({ key }) => {
    if (type === 'next' && key === 'ArrowLeft' || type === 'previous' && key === 'ArrowRight') {
      return
    }

    if (key === 'ArrowRight' || key === 'ArrowLeft') {
      const npost = getNearbyPost()
      clickHandler(npost, type)
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', keyDownHandler)
    return () => {
      document.removeEventListener('keydown', keyDownHandler)
    }
  }, [keyDownHandler ])

  return (
    <button
      disabled={postData.cursor !== '' && postData.postID !== '' ? null : 'disabled'}
      onClick={() => {

        /**
        *
        * When the post post preview is clicked:
        * - find the post in the apollo cache with the variables passed in as props
        * - run the click handler passed as a prop with the post returned from the query.
        *
        */
        const npost = getNearbyPost()
        clickHandler(npost, type)
        trackEvent(track, {
          eventName: `${type}-button-click`,
          category: `Nearby Post Button Click`,
          label: `Move toward ${npost.title}`
        })
      }}
    >
      { children}
    </button>
  )
}

export default NearbyPostButton