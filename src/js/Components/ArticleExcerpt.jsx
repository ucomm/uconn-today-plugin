import React from 'react'

const ArticleExcerpt = ({ excerpt }) => (
  <>
    <div id="uct-excerpt-container" dangerouslySetInnerHTML={{ __html: excerpt }} />
    <b className="uct-blue-separator"></b>
  </>
)

export default ArticleExcerpt