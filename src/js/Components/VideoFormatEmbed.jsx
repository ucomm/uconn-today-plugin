import React from 'react'
import ReactPlayer from 'react-player'

const VideoFormatEmbed = ({ url }) => {
  return <ReactPlayer url={url} style={{ margin: '50px auto' }} />
}

export default VideoFormatEmbed