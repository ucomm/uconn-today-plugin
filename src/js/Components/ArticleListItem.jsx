import React from 'react'
import { useApolloClient } from '@apollo/client'
import { useAnalytics } from 'use-analytics'
import { getPost } from '../../graphql/queries/get-post'
import { addContextToPost } from '../utils/addContextToPost';

import ArticleTitle from './ArticleTitle'
import FeaturedImage from './FeaturedImage'

import { trackEvent } from '../utils/analytics'

/**
 * A single post item. Data is managed via callback _after_ a user clicks on one of the articles on the list.
 *
 * @param object props 
 * - `postData` is an object with the selected post's cursor and ID so that apollo can find it in the cache.
 * - `clickHandler` is a callback function passed to the item so that state can be managed by the article list
 * 
 */
const ArticleListItem = ({ postData, clickHandler }) => {
  
  const client = useApolloClient()
  const { track } = useAnalytics()

  /**
  *
  * When the post post preview is clicked (mouse or keyboard):
  * - find the post in the apollo cache with the variables passed in as props
  * - run the click handler passed as a prop with the post returned from the query.
  *
  */
  const doClickEvent = () => {
    const args = {
      cursor: postData.cursor,
      id: postData.postID
    };

    const { post } = client.readQuery({
      query: getPost,
      variables: args
    });

    const npost = addContextToPost(post, postData);

    clickHandler(npost)

    trackEvent(track, {
      eventName: 'article-opened',
      category: 'Article Opened',
      label: post.title
    })
  }

  return (
    <div
      className="uctoday-list-item uctoday-pictures uctoday-celled"
      role="button"
      tabIndex="0"
      onClick={doClickEvent}
      onKeyDown={evt => {
        if (evt.code === 'Enter' || evt.code === 'Space') {
          doClickEvent()
          evt.preventDefault()
        }
      }}
    >
      <FeaturedImage image={postData.featuredImage} />
      <ArticleTitle>
        <p className="uctoday-cell-title">{postData.title}</p>
      </ArticleTitle>
    </div>
  )
}

export default ArticleListItem