import React from 'react'

const ArticleTitle = ({ children }) => <div className="uctoday-title-container">
  {children}
</div>

export default ArticleTitle