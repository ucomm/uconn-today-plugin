import React, { useState, useEffect, Suspense } from 'react'

import FeaturedImage from './FeaturedImage'

const VideoFormatEmbed = React.lazy(() => import(
  /* webpackChunkName: "uct-video-embed" */
  './VideoFormatEmbed'
))

const ArticleContent = ({ content, link, featuredImage, videoEmbedUrl, socialShare }) => {

  const [ hasWonderPluginShortcode, setHasWonderPluginShortcode ] = useState(false)

  useEffect(() => {
    if (content !== null && content.includes('wonderplugingallery')) {
      setHasWonderPluginShortcode(true)
    }
    // when the component unmounts, reset so that the link doesn't go to the next article
    return () => setHasWonderPluginShortcode(false)
  }, [hasWonderPluginShortcode, content])

  return (
    <div>
      { socialShare }
      <figure>
        <FeaturedImage image={featuredImage} useCaption={true} />
      </figure>

      {
        videoEmbedUrl !== null && <Suspense fallback={<div>Loading...</div>}>
          <VideoFormatEmbed url={videoEmbedUrl} />
        </Suspense>
      }

      <div id="uct-article-content" dangerouslySetInnerHTML={
        !hasWonderPluginShortcode ?
        { __html: content } :
        { __html: `${content}<div><p><a href="${link}">See our photo gallery</a></p></div>`}
      } />
      <div className="full-story-container">
        <p>
          <a href={link}>Get the full story at UConn Today.</a>
        </p>
      </div>
    </div>
  )
}

export default ArticleContent 