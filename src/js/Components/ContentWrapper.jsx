import React from 'react'

import ArticleTitle from './ArticleTitle'
import ArticleExcerpt from './ArticleExcerpt'
import AuthorData from './AuthorData'
import ArticleContent from './ArticleContent'
import NearbyPostButton from './NearbyPostButton'
import ContentToggle from './ContentToggle'
/**
 *
 * This component wraps the actual reader mode view. While it doesn't manage state directly, it passes callback functions to components which help with that.
 *
 * @param object props
 * - `open` boolean: whether the reader view is open or closed
 * - `currentPost` object: the post the user clicked on
 * - `nearbyPostClickHandler` function: a callback to pass to the nearby post buttons
 * - `closeArticleClickHandler` function: a callback to pass to the reader toggle button
 * - `socialShareContainer` react component: a component to pass to the article content
 * - `category` string: the category needs to be passed to the nearby post buttons. That way if there are multiple instances of this app that the right articles are selected when users click among them.
 * 
 */
const ContentWrapper = ({ 
  open, 
  currentPost, 
  nearbyPostClickHandler, 
  closeArticleClickHandler,
  socialShareContainer,
  category
}) => {
  
  return (
    <article
      id="uct-content-wrapper"
      aria-modal="true"
      aria-labelledby="uct-current-post-title"
      role="dialog"
      className={`uct-wrapper-${open ? 'open' : 'closed'}`}
    >
      <div className="uct-content-container">
        <div id="uct-article-actions-container" className="uct-actions-container">
          <div id="uct-nearby-posts-container">
            <div className="uct-nearby-posts-wrapper uct-prev-post-wrapper">
              <NearbyPostButton
                clickHandler={nearbyPostClickHandler}
                postData={{
                  cursor: currentPost.previousPost.cursor,
                  postID: currentPost.previousPost.id,
                  category: category
                }}
                type={'previous'}
              >
                Previous
              </NearbyPostButton>
            </div>
            <div className="uct-nearby-posts-wrapper uct-next-post-wrapper">
              <NearbyPostButton
                clickHandler={nearbyPostClickHandler}
                postData={{
                  cursor: currentPost.nextPost.cursor,
                  postID: currentPost.nextPost.id,
                  category: category
                }}
                type={'next'}
              >
                Next
              </NearbyPostButton>
            </div>
          </div>
          <ContentToggle
            open={open}
            closeArticle={closeArticleClickHandler}
          />
        </div>

        <AuthorData date={currentPost.date} author={currentPost.author.node} />
        <ArticleTitle>
          <h1 id="uct-current-post-title">{currentPost.title}</h1>
        </ArticleTitle>
        <ArticleExcerpt excerpt={currentPost.excerpt} />

        <ArticleContent
          content={currentPost.content}
          link={currentPost.link}
          featuredImage={currentPost.featuredImage}
          socialShare={socialShareContainer}
          videoEmbedUrl={currentPost.postFormatVideoEmbedLink.videoUrl}
        />
      </div>
    </article>
  )
}

export default ContentWrapper