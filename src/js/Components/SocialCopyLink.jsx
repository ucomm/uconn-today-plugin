import React, { useState } from 'react'
/**
 *
 * A button that allows people to copy the current article's link to their clipboard.
 * 
 * @param object props
 * - `children`
 * - `config` object: contains the link and title to the current post.
 * - `open` boolean: whether the reader mode is open or not. Helps reset the text of the button.
 * - `socialClick` function: a callback to help track analytics events
 * 
 */
const SocialCopyLink = ({ children, config, open, socialClick }) => {

  const [ isCopied, setIsCopied ] = useState(false)

  if (isCopied && !open) {
    setIsCopied(false)
  }

  return (
    <button
      onClick={() => {
        navigator.clipboard.writeText(config.link)
          .then(() => {
            setIsCopied(true)
            socialClick(config.title)
          })
          .catch(err => console.log({ err }))
      }}
    >
      {children}<span>{ isCopied && open ? ' Copied!' : ' Copy Link' }</span>
    </button>
  )
}

export default SocialCopyLink