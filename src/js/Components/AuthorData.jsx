import React from 'react'

const AuthorData = ({ date, author }) => {

  const unformattedDate = new Date(date)

  const dateOptions = {
    month: 'long',
    day: 'numeric',
    year: 'numeric'
  }

  const formattedDate = new Intl.DateTimeFormat('en-US', dateOptions).format(unformattedDate)

  return (
    <div id="uct-author-container">
      <p>
        <time>{formattedDate} </time> <span aria-hidden="true">|</span>
        <a href={`https://today.uconn.edu${author.uri}`}> {author.name}</a>
      </p>
    </div>
  )
}

export default AuthorData