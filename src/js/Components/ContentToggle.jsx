import React, { useEffect } from 'react'

const ContentToggle = ({ open, closeArticle }) => {
  const style = {
    visibility: open ? 'visible' : 'hidden'
  }

  const escapeKeyHandler = ({ key }) => {
    if (open && key === 'Escape') {
      closeArticle()
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', escapeKeyHandler)
    return () => {
      document.removeEventListener('keydown', escapeKeyHandler)
    }
  }, [escapeKeyHandler])

  return (
    <div id="uct-toggle-container">
      <button
        style={style}
        onClick={closeArticle}
      >Close</button>
    </div>
  )
}

export default ContentToggle