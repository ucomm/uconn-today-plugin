import React, { Suspense, useState, useEffect } from 'react'
import { useQuery } from '@apollo/client'
import { useAnalytics } from 'use-analytics'

import { getPostsQuery } from '../../graphql/queries/get-posts'
import { socialShareConfig, createSocialConfig } from '../utils/config'
import { trackPage } from '../utils/analytics'
import ArticleListItem from './ArticleListItem'
import ListLoader from './ListLoader'
import SocialShareContainer from './SocialShareContainer';

/**
 * 
 * This component represents the list of articles retrieved from UCToday.
 * The list can be in one of three possible states: error, loading, or with data. These states are managed by the `useQuery` apollo hook. 
 * The view of the list will depend on which state the list is in. 
 * 
 * Analytics are collected whenever someone clicks on an article.
 * Actually displaying the article content is deferred until someone clicks on an article.
 * 
 */



// we don't know if someone will open an article. So we'll lazy load it as its own chunk.
const ContentWrapper = React.lazy(() => import(
  /* webpackChunkName: "uct-content-wrapper" */
  './ContentWrapper'
))  

const ArticleList = (props) => {
  const numPosts = props.numPosts;
  const category = props.category;
  const columns = props.columns;

  const { loading, error, data } = useQuery(getPostsQuery, {
    variables: {
      first: parseInt(numPosts),
      after: null,
      categorySlug: category
    }
  })

  const [currentPost, setCurrentPost] = useState({})
  const [openArticle, setOpenArticle] = useState(false)
  const [socialShare, setSocialShare] = useState(socialShareConfig)

  const { page } = useAnalytics()

  const listItemClickHandler = (post, category) => {
    setCurrentPost(post, category)
    setOpenArticle(!openArticle)

    const newSocialConfig = createSocialConfig(socialShare, post)
    
    setSocialShare(newSocialConfig)

    trackPage(page, { title: post.title, uri: post.uri })
  }

  const nearbyPostClickHandler = (post) => {
    setCurrentPost(post)

    const newSocialConfig = createSocialConfig(socialShare, post)

    setSocialShare(newSocialConfig)
    trackPage(page, { title: post.title, uri: post.uri })
  }

  const closeArticleClickHandler = () => {
    setOpenArticle(false)
  }

  /**
   * Bootstrap does some really annoying things with font size. Adding a class to the html tag helps manage styles for the reader view.
   */
  useEffect(() => {
    const html = document.querySelector('html')
    html.classList.toggle('uct-reader-open', openArticle)
    return () => true
  }, [ openArticle ])
  
  if (loading) return (
    <>
      <div id="uct-article-list">
        <ListLoader number={numPosts} />
      </div>
    </>
  )
  if (error) return <p>Error {error}</p>

  return (
    <>
      <div id="uct-article-list" className={`uctoday-plugin uctoday-plugin-cols-${columns}`}>
        <div aria-live="polite" role="region" style={{ 
          position: 'absolute', 
          left: '-9999px'
        }}>
          <p>
            { `${numPosts} UConn Today articles loaded` }
          </p>
        </div>
        {
          data.posts.edges.map(({ cursor, node }) => <ArticleListItem 
            key={node.id}
            postData={{
              cursor,
              postID: node.id,
              title: node.title,
              featuredImage: node.featuredImage,
              category: props.category
            }}
            clickHandler={listItemClickHandler}
          />)
        }
      </div>
      {
        Object.keys(currentPost).length > 0 && (
          <Suspense fallback={<div>Loading...</div>}>
            <ContentWrapper 
              open={openArticle} 
              currentPost={currentPost}
              nearbyPostClickHandler={nearbyPostClickHandler}
              closeArticleClickHandler={closeArticleClickHandler}
              category={category}
              socialShareContainer={
                <SocialShareContainer
                  config={socialShare}
                  articleTitle={currentPost.title}
                  open={openArticle} />
              }
            />
          </Suspense>
        )
      }
    </>
  )
}

export default ArticleList