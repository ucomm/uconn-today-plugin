import React from 'react'

const ListLoader = ({ number }) => {
  return (
    <div id="uct-loading-list">
      {
        // creates an array of variable length based on the number of articles to expect
        [...Array(number)].map((_, index) => (
            <div key={index} className="uct-loading-item"></div>
          )
        )
      }
    </div>
  )
}

export default ListLoader