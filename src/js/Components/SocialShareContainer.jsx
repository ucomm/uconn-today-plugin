import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEnvelope, faLink } from '@fortawesome/free-solid-svg-icons'
import { faTwitter, faLinkedinIn, faFacebookSquare, faRedditSquare } from '@fortawesome/free-brands-svg-icons'
import { useAnalytics } from 'use-analytics'

import { trackEvent } from '../utils/analytics'
import SocialShareItem from './SocialShareItem'
import SocialCopyLink from './SocialCopyLink'

/**
 * 
 * To save on final file size, only the specific FA icons needed can be used. 
 * If additional icons are needed, they must be imported and added to this library.
 * 
 */
library.add(
  faEnvelope,
  faLink,
  faTwitter,
  faLinkedinIn,
  faFacebookSquare,
  faRedditSquare
)

/**
 *
 * A wrapper around the social button and links to help manage them.
 *
 * @param object props
 * - `config` object
 * - `open` boolean: whether the reader mode is open or not
 * - `articleTitle` string
 * 
 */
const SocialShareContainer = ({ config, open, articleTitle }) => {
  const { track } = useAnalytics()

  const shareClickHandler = (title) => {
    trackEvent(track, {
      eventName: 'social-share',
      category: `Social Share Click: ${title}`,
      label: articleTitle
    })
  }

  return (
    <div className="uct-actions-container social-share-area">
      <SocialCopyLink config={config.copy} open={open} socialClick={shareClickHandler}>
        <FontAwesomeIcon icon="link" />
      </SocialCopyLink>
      <SocialShareItem config={config.email} popup={false}>
        <FontAwesomeIcon icon="envelope" />
      </SocialShareItem>
      <SocialShareItem config={config.twitter} socialClick={shareClickHandler}>
        <FontAwesomeIcon icon={[ 'fab', 'twitter' ]} />
      </SocialShareItem>
      <SocialShareItem config={config.linkedIn} socialClick={shareClickHandler}>
        <FontAwesomeIcon icon={[ 'fab', 'linkedin-in' ]} />
      </SocialShareItem>
      <SocialShareItem config={config.facebook} socialClick={shareClickHandler}>
        <FontAwesomeIcon icon={[ 'fab', 'facebook-square' ]} />
      </SocialShareItem>
      <SocialShareItem config={config.reddit} socialClick={shareClickHandler}>
        <FontAwesomeIcon icon={[ 'fab', 'reddit-square' ]} />
      </SocialShareItem>
    </div>
  )
}

export default SocialShareContainer