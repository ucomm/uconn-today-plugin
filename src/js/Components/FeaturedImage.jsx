import React from 'react'

import Fallback from '../../img/oak-leaf-blue.png'

const FeaturedImage = ({ image, useCaption = false }) => {  
  return (
    <>
      <div className="uctoday-img-container">
        {
          image !== null ? (
            <img 
              className="uctoday-cell-image"
              src={image.node.sourceUrl}
              alt={image.node.altText}
              sizes={image.node.sizes}
              srcSet={image.node.srcSet}
            />
          ) : (
            <img
              className="uctoday-cell-image uctoday-fallback-image"
              src={Fallback}
              alt=""
            />
          )
        } 
      </div>
      {
        // display captions in the full article view
        image !== null && useCaption && (
          <figcaption
            className="image-caption"
            dangerouslySetInnerHTML={{ __html: image.node.caption }} />
        )
      }
    </>
  )
}

export default FeaturedImage