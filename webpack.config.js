const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')

const env = process.env.NODE_ENV

const mode = env !== 'production' ? 'development' : 'production'
const devtool = env !== 'production' ? 'eval-source-map' : 'source-map'
const buildPath = env !== 'production' ? path.resolve(__dirname, 'dev-build') : path.resolve(__dirname, 'build')

const optimization = {
  runtimeChunk: 'single',
  splitChunks: {
    chunks: 'all',
    maxInitialRequests: Infinity,
    minSize: 0,
    cacheGroups: {
      // chunk vendor assets
      commons: {
        test: /[\\/]node_modules[\\/]/,
        // avoid naming collisions
        filename: 'vendor.[chunkhash].js',
        chunks: 'all'
      },
      // all assets with a priority > 0 will be in their own chunk
      // higher numbers ensure modules are split correctly
      apolloVendor: {
        test: /[\\/]node_modules[\\/](@apollo\/client)/,
        name: 'apolloVendor',
        chunks: 'all',
        priority: 11,
      },
      reactVendor: {
        test: /[\\/]node_modules[\\/](react|react-dom|react-player)/,
        name: 'reactVendor',
        chunks: 'all',
        priority: 10,
      },
      fontawesomeVendor: {
        test: /[\\/]node_modules[\\/](@fortawesome\/[\w-]+)/,
        name: 'fontawesomeVendor',
        chunks: 'all',
        priority: 10,
      },
      analyticsVendor: {
        test: /[\\/]node_modules[\\/]([@]?[analytics]+[\/\w-]+|use-analytics)/,
        name: 'analyticsVendor',
        chunks: 'all',
        priority: 10,
      },
    }
  }
}

env !== 'production' ?
  optimization :
  {
    ...optimization,
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin({
        sourceMap: true,
        minimizerOptions: {
          preset: [
            'default',
            {
              discardComments: { removeAll: true }
            }
          ]
        }
      }),
      // believe it or not, these dots ensure minifcation
      // https://webpack.js.org/configuration/optimization/#optimizationminimizer
      '...'
    ]
  }

module.exports = {
  entry: [
    path.resolve(__dirname, 'src/js/index.js')
  ],
  mode,
  devtool,
  output: {
    path: buildPath,
    filename: '[name].js',
    chunkFilename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      },
      {
        test: /\.s?css$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader, 
          { loader: 'css-loader', options: { sourceMap: true } }, 
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      },
      {
        test:/\.(png|svg|jpg|jpeg|gif)$/i,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      }
    ]
  },
  optimization,
  resolve: {
    extensions: ['gql', 'graphql', '.js', '.jsx']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    })
  ]
}