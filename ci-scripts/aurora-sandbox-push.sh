#!/bin/bash

user="${AURORA_DEV_USER}"
pass="${AURORA_DEV_PASSWORD}"
host="${AURORA_DEV_HOST}"
remotepath="${AURORA_DEV_PLUGINS_DIR}/${PROJECT_SLUG}"

lftp sftp://$user:$pass@$host:/$remotepath -e "mirror -Rv --exclude .entrypoint --exclude .git --exclude dev-build --exclude node_modules --exclude www --delete ./ ;bye" -p 22 22