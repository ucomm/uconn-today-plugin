#!/bin/bash

# example for looping through the staging0 plugin paths config file

source ./plugin-paths.sh

DRY_RUN=--dry-run

if [ "$RSYNC_DRY_RUN" = "false" ]
  then DRY_RUN=""
fi

for i in "${prod_plugin_paths[@]}"
do
	echo "===== Starting rsync to $i ====="
  projectpath="$i/$PROJECT_SLUG"

  if [ ! -n "$PROJECT_SLUG" ] || [ ! `ssh deploy@${COMM_PRODUCTION_SERVER} test -d $projectpath && echo exists` ]; then
      # raise error without doing damage
      echo "failure - PROJECT_SLUG unviable"
      exit 1
  fi

  rsync $DRY_RUN -avzO -e ssh --chmod=ugo=rwX --no-perms \
    --exclude-from "excludes.txt" \
    --delete ./ deploy@${COMM_PRODUCTION_SERVER}:"$projectpath"
  echo "===== Ending rsync to $i ====="
done
