#!/bin/bash

# example for looping through the staging0 plugin paths config file

source ./plugin-paths.sh

for i in "${staging_plugin_paths[@]}"
do
	echo "===== Starting rsync to $i ====="
  projectpath="$i/$PROJECT_SLUG"

  if [ ! -n "$PROJECT_SLUG" ] || [ ! `ssh deploy@${COMM_STAGING_SERVER} test -d $projectpath && echo exists` ]; then
      # raise error without doing damage
      echo "failure - PROJECT_SLUG unviable"
      exit 1
  fi

  rsync -avzO -e ssh --chmod=ugo=rwX --no-perms \
    --exclude-from "excludes.txt" \
    --delete ./ deploy@${COMM_STAGING_SERVER}:"$projectpath"
  echo "===== Ending rsync to $i ====="
done
