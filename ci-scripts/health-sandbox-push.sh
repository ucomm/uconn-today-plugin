#!/bin/bash

# SFTP settings
user="${HEALTH_DEV_USER}"
pass="${HEALTH_DEV_PASSWORD}"
host="${HEALTH_DEV_HOST}"
remotepath="${HEALTH_DEV_PLUGINS_DIR}/${PROJECT_SLUG}/"

rsync -avzO -e ssh --chmod=ugo=rwX --no-perms \
--exclude-from "excludes.txt" \
./ deploy@${HEALTH_DEV_HOST}:"${HEALTH_DEV_PLUGINS_DIR}/${PLUGINS_SLUG}/"

# ftp files to health.dev
# lftp sftp://$user:$pass@$host -e "cd $remotepath || mkdir -p $remotepath"
# lftp sftp://$user:$pass@$host:/$remotepath -e "mirror -Rv --delete --exclude .git --exclude .entrypoint --exclude dev-build --exclude node_modules --exclude www ./ ;bye" --no-umask -p 22