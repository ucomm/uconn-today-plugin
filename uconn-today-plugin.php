<?php

/**
 * @wordpress-plugin
 * Plugin Name:       UConn Today Plugin
 * Description:       Displays posts from the UConn Today website
 * Version:           3.0.2
 * Author:            University Communications
 * Author URI:        https://uconn.edu/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       uconn-today-plugin
 * Domain Path:       /languages
 */

use UCTP\Admin\Settings\Options;
use UCTP\Admin\Settings\SettingsPage;
use UCTP\Assets\ScriptLoader;
use UCTP\Assets\StyleLoader;
use UCTP\Frontend\Shortcode;
use UCTP\Frontend\UCTWidget;
use UCTP\Services\CacheService;
use UCTP\Admin\Menus\AdminBar;
use UCTP\Frontend\BeaverBuilder\UConnTodayModule;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define('UCT_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('UCT_PLUGIN_URL', plugins_url('/', __FILE__));

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

require('lib/Interfaces/HttpRequest.php');
require('lib/Interfaces/Errors.php');
require('lib/Assets/Loader.php');
require('lib/Assets/StyleLoader.php');
require('lib/Frontend/Shortcode.php');
require('lib/Frontend/UCTWidget.php');
require('lib/Services/CurlService.php');
require('lib/Services/Frontend/ShortcodeService.php');
require('lib/Services/Frontend/ShortcodeDataService.php');
require('lib/Services/CacheService.php');
require('lib/Services/DataNormalizer.php');
require('lib/Services/PostNormalizer.php');
require('lib/Services/SearchNormalizer.php');
require('lib/Services/NotificationService.php');
require('lib/Assets/ScriptLoader.php');
require('lib/Queries/Requester.php');
require('lib/Queries/PostREST.php');
require('lib/Queries/CategoryREST.php');
require('lib/Queries/SearchREST.php');
require('lib/Admin/Settings/Options.php');
require('lib/Admin/Settings/SettingsPage.php');
require('lib/Admin/Menus/AdminBar.php');

/**
 * 
 * When working locally it can help to run this project alongside the UConn Today theme project.
 * To do that, you need to be in "tandem" mode. 
 * This will allow you to send curl requests exclusively through the docker network linking the containers.
 * See the README for more details.
 * 
 */
if (wp_get_environment_type() === 'local') {
	$options = get_option('uctoday_plugin_settings');
	$tandem_mode = isset($options['uctoday_plugin_setting_tandem_mode']) ?
		$options['uctoday_plugin_setting_tandem_mode'] : "0";
	
	define('TANDEM_MODE', filter_var($tandem_mode, FILTER_VALIDATE_BOOLEAN));
}

$pluginSettings = get_option('uctoday_plugin_settings');
$withStyles = isset($pluginSettings['uctoday_plugin_setting_disable_styles']) ?
	$pluginSettings['uctoday_plugin_setting_disable_styles'] : "0";
$withStyles = filter_var($withStyles, FILTER_VALIDATE_BOOLEAN);

$cacheService = new CacheService();
$settingsPage = new SettingsPage($cacheService);
$settingsPageSlug = $settingsPage->getMenuSlug();
$options = new Options($settingsPageSlug, $cacheService);
$adminBar = new AdminBar();

$shortcode = new Shortcode();
$shortcodeService = $shortcode->getShortcodeService();
$shortcodeSlug = $shortcodeService->getShortcodeSlug();

$styleLoader = new StyleLoader($shortcodeSlug);
$scriptLoader = new ScriptLoader($shortcodeSlug);

$uctWidget = new UCTWidget($shortcode);

add_action('init', [ $shortcode, 'addShortcode' ]);
add_action('init', [$cacheService, 'initForceClearCache']);

add_action('wp_enqueue_scripts', [ $styleLoader, 'enqueue' ]);
add_action('wp_enqueue_scripts', [ $scriptLoader, 'enqueue' ]);

if ($withStyles) {
	add_action('wp_print_styles', [ $styleLoader, 'removeStyles' ]);
}

add_action('admin_init', [ $options, 'init' ]);
add_action('admin_menu', [ $settingsPage, 'init' ]);

add_action('admin_enqueue_scripts', [ $styleLoader, 'adminEnqueue' ]);
add_action('admin_enqueue_scripts', [ $scriptLoader, 'adminEnqueue' ]);

add_action('admin_bar_menu', [ $adminBar, 'addMenuBar' ], 100);

add_action('widgets_init', [ $uctWidget, 'registerWidget' ]);

// enable the BB module on sites that need it
if (is_plugin_active('bb-plugin/fl-builder.php')) {
	if (!class_exists('FLBuilder')) {
		return;
	}
	// require BB module as needed
	require_once(UCT_PLUGIN_DIR . 'lib/Frontend/BeaverBuilder/uct-module/uct-module.php');
	require_once(UCT_PLUGIN_DIR . 'lib/Frontend/BeaverBuilder/uct-module/uct-module-settings.php');

	// use dependency injection rather than create new instances in the class
	$uctBBModule = new UConnTodayModule();
	add_action('init', [$uctBBModule, 'loadModule']);
}

if (!is_admin()) {

	$styleLoader->hasActiveWidget = $uctWidget->isActiveWidget();
	$scriptLoader->hasActiveWidget = $uctWidget->isActiveWidget();

}


