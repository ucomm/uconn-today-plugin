<?php

namespace UCTP\Interfaces;

interface PluginErrorCodes {
  public const INVALID_ERROR_CODE = -1;
  public const NO_ERROR = 0;
  public const NETWORK_ERROR = 1;
  public const CACHE_ERROR = 2;
  public const INVALID_RESPONSE = 3;
  public const INVALID_JSON = 4;
  public const CURL_TIMEOUT_ERROR = 5;
  public const CURL_INIT_ERROR = 6;
}

interface ErrorValidation extends PluginErrorCodes {

  /**
   * Validate that the error code exists
   *
   * @param integer $code
   * @return integer
   */
  public function validateErrorCode(int $code): int;
}

interface ErrorMessage extends PluginErrorCodes {

  /**
   * Get an error message for a specific error code
   *
   * @param integer $code
   * @return string
   */
  public function getErrorMessage(int $code): string;
}