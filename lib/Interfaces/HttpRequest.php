<?php

namespace UCTP\Interfaces;

/**
 * Interface for absracting HTTP request methods.
 * This implementation makes testing easier since we don't have to mock individual curl functions. In addition, we can swap out the curl implementation for another HTTP request method.
 */
interface HttpRequest {
  public function close();
  public function errno();
  public function exec();
  public function getInfo($name);
  public function init();
  public function setOption(string $name, $value);
}