<?php

namespace UCTP\Queries;

use UCTP\Queries\Requester;
use UCTP\Services\ResponseData;

/**
 * This class extends the Requester in order to handle getting and caching UCToday possts
 */
class PostREST extends Requester {

  public $safeFetch;
  public $queryString;
  public $route;
  public $usePictures;

  protected $queryAtts;

  public function __construct(bool $safeFetch, array $queryAtts = [], bool $usePictures = true, string $endpoint = '/posts')
  {
    parent::__construct($queryAtts, $endpoint);

    $this->queryAtts = $queryAtts;
    $this->usePictures = $usePictures;
    $this->safeFetch = $safeFetch;
  }

  public function getQueryAtts(): array {
    return $this->queryAtts;
  }

  public function setQueryAtts(array $atts): array {
    return $this->queryAtts = $atts;
  }

  public function setRoute(string $route = ''): string {
    if (!empty($route)) {
      return $this->route = $route;
    }

    $queryString = !empty($this->queryAtts) ? $this->buildQuery() : '';
    if ($this->usePictures) {
      $queryString .= '&_embed';
    }
    return $this->route = $this->url . $queryString;
  }

  public function handleRequest(): ResponseData {
    $route = $this->setRoute();
    $transientID = $this->doTransientID($route);
    $currentTransient = get_transient($transientID);
    $hasTransientError = !$currentTransient instanceof ResponseData || $currentTransient->errorCode > 0;

    if ($hasTransientError) {
      // in this case, something went wrong with the inital transient or request
      // so, we delete the current transient and make a new request
      // then we re-cache the response and return the data even if it's bad
      // an admin notice will be displayed via the shortcode
      $response = $this->doCurlRequest($route);
      return $this->cacheResponse($response, $transientID);
    } else if (
      $currentTransient instanceof ResponseData &&
      !$currentTransient->error &&
      is_array($currentTransient->response) &&
      count($currentTransient->response) === 0
    ) {
      // in this case, there's a valid transient, but the response is empty
      // therefore, we make a new request to the default endpoint and _do not_ cache the response since these articles update frequently
      $route = $this->url . 'category-name=today-homepage&posts_per_page=' . $this->queryAtts['number-of-posts'] . '&offset=' . $this->queryAtts['offset'];

      if ($this->usePictures) {
        $route .= '&_embed';
      }

      $response = $this->doCurlRequest($route);
      // $this->setAdminMessage("No articles were returned from the cache. It's possible that there is a misconfiguration of the attributes used to request articles. Therefore, the default UConn Today feed was used. Try checking the configuration and clearing the cache to re-fetch data.");
      // $this->setAdminNotice(true);
      return $response;
    } else {
      // in this case, there's a valid transient and the response is not empty
      // so, we return the response
      return $currentTransient;
    }
  }

  /**
   * Normalize the relationship between the shortcode attributes and the WP REST API query parameters
   *
   * @return array
   */
  protected function setQuery(): array {
    return [
      'author-name' => $this->queryAtts['author'],
      'category-name' => $this->setCategory($this->queryAtts),
      'offset' => $this->queryAtts['offset'],
      'posts_per_page' => $this->queryAtts['number-of-posts'],
      'slug' => $this->setSlugs($this->queryAtts),
      'include' => $this->queryAtts['include'], 
      'tag' => $this->queryAtts['tag'],
      'search' => $this->queryAtts['search']
    ];
  }
}