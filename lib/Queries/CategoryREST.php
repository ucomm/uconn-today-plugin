<?php

namespace UCTP\Queries;

use UCTP\Queries\Requester;
use UCTP\Services\ResponseData;

/**
 * This class extends the Requester in order to handle getting UCToday Categories
 */
class CategoryREST extends Requester {

  public $queryString;
  public $route;

  protected array $headers = [];

  public function __construct(array $queryAtts = [], string $endpoint = '/categories')
  {
    parent::__construct($queryAtts, $endpoint);
    $this->queryString = '';

    $this->route = $this->url . $this->queryString;
  }

  public function setHeaders(array $headers): void {
    $this->headers = $headers;
  }

  /**
   * This method will get category data from a curl request to the UCToday REST API. 
   * We won't cache the responses as UCToday authors need the most recent categories at all times.
   *
   * @return ResponseData
   */
  public function handleRequest(): ResponseData {
    $categoryRoute = 'https://' . $this->env . '/wp-rest/uconn/v1/taxonomies/category?group=parent';
    $route = $this->setRoute($categoryRoute);
    $response = $this->doCurlRequest($route);
    return $response;
  }

  public function setRoute(string $route = ''): string {
    if (!empty($route)) {
      return $this->route = $route;
    }

    $queryString = !empty($this->queryAtts) ? $this->buildQuery() : '';
    return $this->route = $this->url . $queryString;
  }

  /**
   * This method will return the value of the X-WP-TotalPages header field. This is the total number of pages of returned records (categories).
   * 
   * @return int the integer value of the X-WP-TotalPages header field
   */
  public function getNumOfCatPages(): int
  {
    // wp_remote_get is unstable on Aurora
    $pagesFromHeader = $this->getHeaders($this->route);
    $total_num_of_catpages = isset($pagesFromHeader['X-WP-TotalPages']) ? $pagesFromHeader['X-WP-TotalPages'] : 0;
    $total_num_of_catpages = intval($total_num_of_catpages);

    return $total_num_of_catpages;
  } 

  /**
   * Get an associative array of headers from a route.
   *
   * @param string $route
   * @param boolean $assoc
   * @return array
   */
  protected function getHeaders(string $route): array {
    $headers = [];

    if (!empty($this->headers)) {
      return $this->headers;
    }

    $isValidRoute = filter_var($route, FILTER_VALIDATE_URL);
    $headers = $isValidRoute ? get_headers($route, true) : [];
    return is_array($headers) ? $headers : [];
  }

  protected function buildQuery(): string {
    $perPage = isset($this->queryAtts['per_page']) ? $this->queryAtts['per_page'] : 10;
    $page = isset($this->queryAtts['page']) ? $this->queryAtts['page'] : 1;
    $this->queryAtts['per_page'] = $perPage;
    $this->queryAtts['page'] = $page;
    $filteredQuery = $this->filterQuery($this->queryAtts);
    return http_build_query($filteredQuery);
  }
}