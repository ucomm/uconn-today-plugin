<?php

namespace UCTP\Queries;

use UCTP\Queries\Requester;
use UCTP\Services\ResponseData;

/**
 * This class extends the Requester in order to handle getting API data based on search term
 */
class SearchREST extends Requester {

  public $queryString;
  public $route;

  public function __construct(array $queryAtts = [], string $endpoint = '/search')
  {
    parent::__construct($queryAtts, $endpoint);

    $this->queryString = '';

    $this->route = $this->url . $this->queryString;
  }

/**
 * This method will get search data from a curl request to the UCToday REST API. 
 * Since we're doing searches, we won't cache the response. That way, we get the most recent search results at all times.
 *
 * @return ResponseData
 */
  public function handleRequest(): ResponseData {
    $route = $this->setRoute();
    $response = $this->doCurlRequest($route);
    return $response;
  }

  /**
   * Set the request route. Make sure to include the search term if it exists
   *
   * @param string $route
   * @return string
   */
  public function setRoute(string $route = ''): string {
    if (!empty($route)) {
      return $this->route = $route;
    }

    $queryString = !empty($this->queryAtts) ? $this->buildQuery() : '';
    return $this->route = $this->url . $queryString;
  }

  /**
   * This method will build the query string for the /search endpoint
   * It will ensure that the search term is normalized
   * 
   * @return string
   */
  public function buildQuery(): string {
    $searchTerms = isset($this->queryAtts['search']) ? 
      $this->queryAtts['search'] : 
      '';
    $normalizedSearch = $this->setSearch($searchTerms);
    $this->queryAtts['search'] = $normalizedSearch;
    $filteredQuery = $this->filterQuery($this->queryAtts);
    return http_build_query($filteredQuery);
  }

  /**
   * Create a search query for the /search endpoint
   *
   * @param string|null $keywords
   * @return string|null
   */
  protected function setSearch(?string $keywords): ?string {
    $toReturn = null;
    if ($keywords === null || $keywords === '') {
      return $toReturn;
    }
    $lowered = strtolower($keywords);
    $replaced = str_replace(' ', '+', $lowered);
    $toReturn = $replaced;
    return $toReturn;
  }
}