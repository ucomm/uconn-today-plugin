<?php

namespace UCTP\Queries;

use UCTP\Services\CacheService;
use UCTP\Services\CurlService;
use UCTP\Services\ResponseData;

/**
 * Parent class to create requests to the WP REST API
 */
class Requester {
  protected $env;
  protected $endpoint;
  protected $url;
  protected $queryAtts;

  protected $responseData;

  public $curlService;
  public $cacheService;
  public $showAdminNotice;
  public $adminMessage;

  public function __construct(
    array $queryAtts = [], 
    string $endpoint = '/posts', 
    ?CurlService $curlService = null,
    ?CacheService $cacheService = null
  ) {
    $this->env = wp_get_environment_type() === 'production' ? 'today.uconn.edu' : 'dev.today.uconn.edu';
    $this->endpoint = $endpoint;
    $this->url = 'https://' . $this->env . '/wp-rest/wp/v2' . $this->endpoint . '?';

    $this->queryAtts = $queryAtts;
    // $this->showAdminNotice = false;
    // $this->adminMessage = 'There was a problem retrieving the UConn Today articles.';

    // we might want a new set of response data on each request so this won't be injected in the constructor args
    $this->responseData = new ResponseData(false, null, 0);
    $this->curlService = $curlService ?? new CurlService($this->responseData);
    $this->cacheService = $cacheService ?? new CacheService();

  }

  public function getEnv(): string {
    return $this->env;
  }

  public function getEndpoint(): string {
    return $this->endpoint;
  }

  public function setEndpoint(string $endpoint): string {
    return $this->endpoint = $endpoint;
  }

  public function getUrl(): string {
    return $this->url;
  }

  public function getQueryAtts(): array {
    return $this->queryAtts;
  }

  public function setQueryAtts(array $atts): array {
    return $this->queryAtts = $atts;
  }

  public function getIsError(): bool {
    return $this->responseData->error;
  }

  public function getResponseData(): ResponseData {
    return $this->responseData;
  }

  public function setResponseData(ResponseData $result): ResponseData {
    return $this->responseData = $result;
  }

  /**
   * Make the API request. The implementation is up to each child class
   * 
   *
   * @return null|ResponseData
   */
  public function handleRequest(): ?ResponseData {
    return null;
  }

  /**
   * Make a curl request to the REST API using the CurlService. Set the result to the responseData property.
   *
   * @param string $route
   * @return ResponseData
   */
  protected function doCurlRequest(string $route): ResponseData {
    return $this->curlService->request($route);
  }

  protected function doTransientID(string $route): string {
    return $this->cacheService->transientID($route);
  }

  /**
   * Cache the response from a REST request
   *
   * @param ResponseData $data - data from the REST API
   * @param string $transientID - the transient cache key 
   * @return ResponseData
   */
  protected function cacheResponse(ResponseData $data, string $transientID = ''): ResponseData {
    if ($transientID === '') {
      $transientID = $this->doTransientID($this->url);
    }
    return $this->cacheService->cacheResponse($data, $transientID);
  }
  
  /**
   * Build HTTP query parameters for an endpoint
   *
   * @return string
   */
  protected function buildQuery(): string {
    $query = $this->setQuery();
    $filteredQuery = $this->filterQuery($query);
    return http_build_query($filteredQuery);
  }

  protected function setQuery(): array {
    return $this->queryAtts;
  }

  /**
   * Remove keys with `null` values from the query array
   *
   * @param array $query
   * @return array
   */
  protected function filterQuery(array $query): array {
    return array_filter($query, function($att) {
      if ($att !== null && $att !== '') {
        return $att;
      }
    });
  }

  /**
   * Create a readable transient cache name based on query params.
   *
   * @param string $route
   * @return string
   */
  protected function transientID(string $route): string {
    return $this->cacheService->transientID($route);
  }

  /**
   * For compatibility only.
   * Normalizes non-category attributes into categories. Some of these are quite old.
   *
   * @deprecated version 2.5.0
   * @param array $atts
   * @return string|null
   */
  protected function setCategory(array $atts)
  {

    if ((isset($atts['school-college']) && $atts['school-college'] !== '') && (isset($atts['zone']) && $atts['zone'] !== '')) {
      return $atts['school-college'];
    } elseif (isset($atts['school-college']) && $atts['school-college'] !== '') {
      return $atts['school-college'];
    } elseif (isset($atts['campus']) && $atts['campus'] !== '') {
      return $atts['campus'] . '-campus';
    } elseif (isset($atts['county']) && $atts['county'] !== '') {
      return $atts['county'];
    } elseif (isset($atts['category']) && $atts['category'] !== '') {
      return $atts['category'];
    } else {
      return null;
    }
  }

  /**
   * Create a search query for the /search endpoint
   *
   * @param string|null $keywords
   * @return string|null
   */
  // protected function setSearch(?string $keywords): ?string
  // {
  //   if ($keywords === null || $keywords === '') {
  //     return null;
  //   }
  //   return strtolower(str_replace(' ', '+', $keywords));
  // }

  /**
   * Sets slugs when using the $atts['slug'] attribute
   *
   * @param array $atts
   * @return string|null
   */
  protected function setSlugs(array $atts): ?string
  {

    if (isset($atts['sc-post-slugs']) && $atts['sc-post-slugs'] !== '') {
      // deprecated as of 2.5.0
      $atts['post-slugs'] = $atts['sc-post-slugs'];
      return $atts['post-slugs'];
    } elseif (isset($atts['post-slugs']) && $atts['post-slugs'] !== '') {
      return $atts['post-slugs'];
    } else {
      return null;
    }
  }
}