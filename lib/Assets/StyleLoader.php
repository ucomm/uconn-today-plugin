<?php

namespace UCTP\Assets;

use UCTP\Assets\Loader;

/**
 * A class to handle loading CSS assets
 */
class StyleLoader extends Loader
{

  public $mainChunk;
  public $chunkNames;

  public function __construct(string $shortcodeSlug)
  {
    parent::__construct($shortcodeSlug);
    $this->chunkNames = [];
    $this->mainChunk = '';

    $files = glob(UCT_PLUGIN_DIR . $this->buildDir . '/*.css');
    foreach ($files as $file) {
      $info = pathinfo($file);
      if (strpos($file, 'main') === false) {
        array_push($this->chunkNames, $info['filename']);
      } else {
        $this->mainChunk = $info['filename'];
      }
    }

  }
  /**
   * Enqueue the prepared scripts.
   * In this example, the script will only be enqueued if the shortcode is present.
   *
   * @return void
   */
  public function enqueue(): void
  {
    $setting = get_option('uctoday_plugin_settings', []);
    $uct_settings_is_array = is_array($setting);
    $js_key_exists = false;
    $js_mode = false;
    $isEditorOrAdmin = current_user_can('edit_posts');

    if ($uct_settings_is_array) {
      $js_key_exists = array_key_exists('uctoday_plugin_setting_graphql_mode', $setting);
      $js_mode = ($js_key_exists) ? $setting['uctoday_plugin_setting_graphql_mode'] == 1 : false;

      if ($js_key_exists && $js_mode) {
        $this->prepareAppChunkStyles();
        $this->prepareJSAppStyles();
        wp_enqueue_style($this->handle . '-app');
      }
    }

    $this->preparePluginStyles();
    wp_enqueue_style($this->handle);

    if ($isEditorOrAdmin) {
      $this->prepareAdminBarStyles();
      wp_enqueue_style($this->adminHandle . '-admin-bar');
    }
  }

  /**
   * Check if the user intends to override the styles and remove them on `wp_print_styles`
   *
   * @return void
   */
  public function removeStyles(): void {
    wp_dequeue_style($this->handle);
  }

  private function prepareAppChunkStyles() {
    if (count($this->chunkNames) < 1) return;

    foreach ($this->chunkNames as $name) {
      wp_register_style(
        $this->handle . '-' . $name,
        UCT_PLUGIN_URL . $this->buildDir . '/' . $name . '.css',
        [],
        null
      );
    }
  }

  private function preparePluginStyles() {
    return wp_register_style(
      $this->handle,
      UCT_PLUGIN_URL . 'public-assets/css/uconn-today-plugin-public.css',
      [],
      null
    );
  }

  private function prepareJSAppStyles() {

    $styleDeps = [
      $this->handle
    ];

    if (count($this->chunkNames) > 1) {
      foreach ($this->chunkNames as $name) {
        array_push($styleDeps, $this->handle . '-' . $name);
      }
    }

    wp_register_style(
      $this->handle . '-app',
      UCT_PLUGIN_URL . $this->buildDir . '/' . $this->mainChunk . '.css',
      $styleDeps,
      null
    );
  }

  /**
   * This method can be used to enqueue an asset on an admin page.
   * Use the slug to filter which pages it should be used on.
   *
   * @param string $hook - the admin page's slug to enqueue on
   * @return void
   */
  public function adminEnqueue(string $hook): void
  {
    $this->prepareAdminBarStyles();

    $settingsPage = stripos($hook, 'uctoday_settings');
    if ($settingsPage !== false) {
      $this->prepareAdminStyles();
      wp_enqueue_style($this->adminHandle);
    }

    $isSiteOriginActive = is_plugin_active('siteorigin-panels/siteorigin-panels.php') ? true : false;

    if ($isSiteOriginActive || $hook === 'widgets.php') {
      $this->prepareAdminWidgetStyles();
      wp_enqueue_style($this->adminHandle . '-widget');
    }

    wp_enqueue_style($this->adminHandle . '-admin-bar');
  }

  private function prepareAdminStyles()
  {
    $styleDeps = [];
    return wp_register_style(
      $this->adminHandle,
      UCT_PLUGIN_URL . 'admin-assets/css/style.css',
      $styleDeps
    );
  }

  private function prepareAdminWidgetStyles() {
    $styleDeps = [];
    return wp_register_style(
      $this->adminHandle . '-widget',
      UCT_PLUGIN_URL . 'admin-assets/css/widget-styles.css',
      $styleDeps
    );
  }

  private function prepareAdminBarStyles() {
    $styleDeps = [];
    return wp_register_style(
      $this->adminHandle . '-admin-bar',
      UCT_PLUGIN_URL . 'admin-assets/css/adminBar.css',
      $styleDeps
    );
  }
}
