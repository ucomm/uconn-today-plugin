<?php

namespace UCTP\Assets;

use WP_Post;

class Loader {
  public $hasActiveWidget;

  protected string $handle;
  protected string $adminHandle;
  protected string $buildDir;
  protected string $shortcodeSlug;
  protected bool $hasShortcode;
  protected bool $hasSiteOriginWidget;

  public function __construct(string $shortcodeSlug)
  {

    $isLocalhost = wp_get_environment_type() === 'local';

    $this->handle = 'uctoday-plugin';
    $this->adminHandle = 'uctoday-plugin-admin';
    // set the build directory based on environment
    $this->buildDir = $isLocalhost ? 'dev-build' : 'build';
    $this->shortcodeSlug = $shortcodeSlug;
    $this->hasActiveWidget = false;
    $this->hasShortcode = false;
    $this->hasSiteOriginWidget = false;
  }

  /**
   * Enqueue assets when the wp_enqueue_scripts action is called
   *
   * @return void
   */
  public function enqueue(): void {
    return;
  }

  /**
   * Enqueue assets when the admin_enqueue_scripts action is called
   *
   * @return void
   */
  public function adminEnqueue(string $hook): void {
    return;
  }

  /**
   * Set the base handle for scripts and styles
   *
   * @return string
   */
  public function setHandle(string $handle): string
  {
    return $this->handle = $handle;
  }

  /**
   * Set the base handle for admin scripts and styles
   *
   * @return string
   */
  public function setAdminHandle(string $handle): string
  {
    return $this->adminHandle = $handle;
  }

  public function getHandle(): string {
    return $this->handle;
  }

  public function getAdminHandle(): string {
    return $this->adminHandle;
  }

  public function getBuildDir(): string {
    return $this->buildDir;
  }

  /**
   * SiteOrigin is terrible.
   *
   * @param WP_Post|null $post
   * @return boolean
   */
  public function checkSiteOriginStatus(?WP_Post $post): bool
  {
    $active = false;

    if ($post === null) {
      return $active;
    }

    $isSiteOriginActive = is_plugin_active('siteorigin-panels/siteorigin-panels.php') ? true : false;
    $postMeta = get_post_meta($post->ID, 'panels_data');
    $hasSOWidgets = isset($postMeta[0]) && isset($postMeta[0]['widgets']) ? true : false;

    if ($isSiteOriginActive && $hasSOWidgets) {

      $soWidgets = $postMeta[0]['widgets'];

      foreach ($soWidgets as $widget) {
        $widgetFound = array_search('UCTP\Frontend\UCTWidget', $widget['panels_info'], true);
        if ($widgetFound === 'class') {
          $active = true;
          return $active;
          break;
        }
      }

      return $active;
    }
    return $active;
  }

  /**
   * Check the current post for the plugin shortcode
   *
   * @param WP_Post|null $post
   * @return bool
   */
  public function contentHasShortcode(?WP_Post $post): bool {
    return $post !== null ?
      has_shortcode($post->post_content, $this->shortcodeSlug) :
      false;
  }
}
