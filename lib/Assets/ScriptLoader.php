<?php

namespace UCTP\Assets;

use UCTP\Assets\Loader;

/**
 * A class to handle loading JS assets
 */
class ScriptLoader extends Loader
{

  public $mainChunk;
  public $chunkNames;

  public function __construct(string $shortcodeSlug)
  {

    parent::__construct($shortcodeSlug);
    $this->chunkNames = [];
    $this->mainChunk = '';
    
    // exclude files to be lazy loaded
    $exclude = [
      'uct-article-list',
      'uct-content-wrapper'
    ];
    // get a list of all files in the build directory
    $files = glob(UCT_PLUGIN_DIR . $this->buildDir . '/*.js');

    foreach ($files as $file) {
      $info = pathinfo($file);
      if (strpos($file, 'main') === false && !in_array($info['filename'], $exclude)) {
        array_push($this->chunkNames, $info['filename']);
      } else if (strpos($file, 'main') >=0 && !in_array($info['filename'], $exclude)) {
        $this->mainChunk = $info['filename'];
      }
    }

  }

  /**
   * Enqueue the prepared scripts.
   * In this example, the script will only be enqueued if the shortcode is present.
   *
   * @return void
   */
  public function enqueue(): void
  {
    $isEditorOrAdmin = current_user_can('edit_posts');
    $settings = get_option('uctoday_plugin_settings');

    $gqlOption = isset($settings['uctoday_plugin_setting_graphql_mode'])  ?
      filter_var($settings['uctoday_plugin_setting_graphql_mode'], FILTER_VALIDATE_BOOLEAN) :
      false;

    if ($gqlOption) {
      $this->prepareAppChunks();
      $this->prepareAppScript();

      foreach ($this->chunkNames as $name) {
        wp_enqueue_script($this->handle . '-' . $name);
      }
      wp_enqueue_script($this->handle);
    }

    if ($isEditorOrAdmin) {
      $this->prepareAdminBarScript();
      wp_enqueue_script($this->adminHandle . '-admin-bar');
    }
  }

  /**
   * This method can be used to enqueue an asset on an admin page.
   * Use the slug to filter which pages it should be used on.
   *
   * @param string $hook - the admin page's slug to enqueue on
   * @return void
   */
  public function adminEnqueue(string $hook): void
  {
    $this->prepareAdminBarScript();
    $settingsPage = stripos($hook, 'uctoday_settings');
    if ($settingsPage !== false) {
      $this->prepareAdminScript();
      wp_enqueue_script($this->adminHandle);
    }

    $isSiteOriginActive = is_plugin_active('siteorigin-panels/siteorigin-panels.php') ? true : false;

    if ($isSiteOriginActive || $hook === 'widgets.php') {
      $this->prepareAdminWidgetScripts();
      wp_enqueue_script($this->adminHandle . '-widget');
    }
    wp_enqueue_script($this->adminHandle . '-admin-bar');
  }

  public function localizeScript(array $data) {
    wp_localize_script($this->handle, 'uctodayPostData', $data);
  }

  public function hasActiveWidget(string $idBase) {
    return $this->hasActiveWidget($idBase);
  }

  private function prepareAppChunks() {
    foreach ($this->chunkNames as $name) {
      wp_register_script(
        $this->handle . '-' . $name,
        UCT_PLUGIN_URL . $this->buildDir . '/' . $name . '.js',
        [],
        false,
        true
      );
    }
  }


  /**
   * Prepare the script by registering it.
   *
   * @return void
   */
  private function prepareAppScript()
  {
    $scriptDeps = array_map(function($name) {
      return $this->handle . '-' . $name;
    }, $this->chunkNames);

    wp_register_script(
      $this->handle,
      UCT_PLUGIN_URL . $this->buildDir . '/' . $this->mainChunk . '.js',
      $scriptDeps,
      false,
      true
    );
  }

  private function prepareAdminScript() {
    $scriptDeps = [];
    wp_register_script(
      $this->adminHandle,
      UCT_PLUGIN_URL . 'admin-assets/js/index.js',
      $scriptDeps,
      false,
      true
    );
  }

  private function prepareAdminWidgetScripts() {
    $scriptDeps = ['jquery'];
    wp_register_script(
      $this->adminHandle . '-widget',
      UCT_PLUGIN_URL . 'admin-assets/js/widget-scripts.js',
      $scriptDeps
    );
  }

  private function prepareAdminBarScript() {
    wp_register_script(
      $this->adminHandle . '-admin-bar',
      UCT_PLUGIN_URL . 'admin-assets/js/adminBar.js',
      [],
      false,
      true
    );
  }
}
