<?php

namespace UCTP\Services;

use stdClass;

/**
 * Create normalized post data from a REST/cache response. This is preferred because of how deeply some of the story attributes (e.g. featured image) are. This way, they're brought closer to the top level of an array making stories easier to manipulate in template files.
 */
class PostDataNormalizer extends DataNormalizer {

  public $publicCategories;

  public function __construct(?array $storyData)
  {
    parent::__construct($storyData);
    $this->publicCategories = [
      'arts',
      'athletics',
      'alumni',
      'blue',
      'community-impact',
      'entrepreneurship',
      'health-wellness',
      'research',
      'uconn-health',
      'uconn-life',
      'university-news',
      'magazine',
      'uconn-voices'
    ];
  }

  /**
   * Creates a nested array of stories.
   *
   * @return array
   */
  public function createNormalizedData(): array {
    if (empty($this->data)) {
      return [];
    }
    
    $sortedData = $this->sortStoriesByDate($this->data);

    return array_map([ $this, 'createNormalizedStory' ], $sortedData);
  }

  /**
   * Normalize a story's data to be used in a template file. The purpose is to make it easier to work with the data in the templates.
   *
   * @param object $story - a single story object from the WP REST API
   * @return array
   */
  protected function createNormalizedStory($story): array {
    $terms = null;

    $isEmbeded = isset($story->_embedded);
    $hasTerms = isset($story->_embedded->{'wp:term'}) && 
      $story->_embedded->{'wp:term'} !== null;

    if ($isEmbeded && $hasTerms) {
      $terms = $story->_embedded->{'wp:term'};
    }

    $date = $this->setStoryDate($story->date);
    $featuedImage = $this->setFeaturedImage($story);
    $featuedImageAlt = $this->setFeaturedImageAlt($story);
    $publicCategories = $this->setPublicCategories($terms);

    return [
      'title' => $story->title->rendered,
      'link' => $story->link,
      'date' => $date,
      'excerpt' => $story->excerpt->rendered,
      'featured_image' => $featuedImage,
      'featured_image_alt' => $featuedImageAlt,
      'public_categories' => $publicCategories,
    ]; 
  }

  protected function setPublicCategories(?array $postTerms): array {
    if (!$postTerms) {
      return [];
    }

    // helps with passing the public categories array to the closure
    // passing $this->publicCategories directly to array_reduce doesn't work as well
    $uctPublicCategories = $this->publicCategories;

    $categoryIndex = -1;
    foreach ($postTerms as $index => $termArray) {
      foreach ($termArray as $term) {
        if ($term->taxonomy === 'category') {
          $categoryIndex = $index;
          break 2;
        }
      }
    }

    $categoryTerms = $postTerms[$categoryIndex];
    $publicCategories = array_reduce($categoryTerms, function($acc, $term) use ($uctPublicCategories) {
      if (in_array($term->slug, $uctPublicCategories)) {
        $publicTerm = new stdClass();
        $publicTerm->id = $term->id;
        $publicTerm->name = $term->name;
        $publicTerm->slug = $term->slug;
        $publicTerm->link = $term->link;
        $acc[] = $publicTerm;
      }
      return $acc;
    }, []);
    return $publicCategories;
  }

  /**
   * Sort stories by their published date. The data input is mutated by usort and then returned.
   *
   * @param array $storyData
   * @return array
   */
  protected function sortStoriesByDate(array $storyData = []): array
  {
    if (empty($storyData)) {
      return $storyData;
    }
    usort($storyData, function ($a, $b) {
      $aDate = strtotime($a->date);
      $bDate = strtotime($b->date);
      return $bDate - $aDate;
    });

    return $storyData;
  }

  /**
   * Selects a valid featured image URL for a story. The medium large image is preferred over the medium image. Returns null if neither is present.
   *
   * @param object $story
   * @return string|null
   */
  public function setFeaturedImage(object $story): ?string
  {
    $featuredImageURL = null;
    if (!isset($story->_embedded)) {
      return $featuredImageURL;
    }

    $featuredImageResponse = json_decode(json_encode($story->_embedded), true);

    if (isset($featuredImageResponse['wp:featuredmedia'][0]['media_details']['sizes']['medium_large']['source_url'])) {
      $featuredImageURL = $featuredImageResponse['wp:featuredmedia'][0]['media_details']['sizes']['medium_large']['source_url'];
    } elseif (isset($featuredImageResponse['wp:featuredmedia'][0]['media_details']['sizes']['medium']['source_url'])) {
      $featuredImageURL = $featuredImageResponse['wp:featuredmedia'][0]['media_details']['sizes']['medium']['source_url'];
    }

    return $featuredImageURL;
  }

  /**
   * Creates the alt text for a featured image
   *
   * @param object $story
   * @return string the alt text for the image
   */
  protected function setFeaturedImageAlt(object $story): string
  {
    $alt = '';
    if (!isset($story->_embedded)) {
      return $alt;
    }

    $featuredImageResponse = json_decode(json_encode($story->_embedded), true);

    if ($featuredImageResponse && isset($featuredImageResponse['wp:featuredmedia'][0]['alt_text'])) {
      $alt = $featuredImageResponse['wp:featuredmedia'][0]['alt_text'];
    }

    return $alt;
  }

  /**
   * Converts the story's timestamp to a formatted m/d/y date string
   *
   * @param string $date
   * @return string|false
   */
  protected function setStoryDate(string $date) {
    $timestamp = strtotime($date);
    return date('m/d/y', $timestamp);
  }
}
