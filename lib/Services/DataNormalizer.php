<?php

namespace UCTP\Services;

/**
 * A class to create normalized data to be used in template files etc
 */
class DataNormalizer {

  public $data;
  public $sortedData;

  public function __construct(?array $data) {
    $this->data = $data ?? [];
  }

  public function getData(): array {
    return $this->data;
  }

  public function setData(array $data): array {
    return $this->data = $data;
  }
  
  /**
   * Create an array of normalized data for use in shortcodes
   * This method is a placeholder and will be implemented in child classes
   *
   * @return array
   */
  protected function createNormalizedData(): array {
    return [];
  }
}