<?php

namespace UCTP\Services;

/**
 * Create normalized search data from a REST/cache response. 
 */
class SearchDataNormalizer extends DataNormalizer {
  public function __construct(array $searchData)
  {
    parent::__construct($searchData);
  }

  /**
   * Creates an array of IDs for search results.
   *
   * @return array
   */
  public function createNormalizedData(): array {
    return array_map([ $this, 'normalizeResult' ], $this->data);
  }

  /**
   * Just return the ID of the search result
   *
   * @param object $searchResult
   * @return void
   */
  public function normalizeResult($searchResult) {
    return $searchResult->id;
  }
}
