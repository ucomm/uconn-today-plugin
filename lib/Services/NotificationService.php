<?php

namespace UCTP\Services;

use UCTP\Interfaces\ErrorMessage;

class NotificationService implements ErrorMessage {
  public $message;
  public $type;

  public function __construct(string $message, string $type) {
    $this->message = $message;
    $this->type = $type;
  }

  public function getMessage(): string {
    return $this->message;
  }

  public function setMessage(string $message): string {
    return $this->message = $message;
  }

  public function getType(): string {
    return $this->type;
  }

  public function setType(string $type): string {
    return $this->type = $type;
  }

  public function displayAdminNotice(): void {
    add_action('admin_notices', function() {
      echo '<div class="notice notice-' . $this->type . ' is-dismissible"><p>' . $this->message . '</p></div>';
    });
  }

  public function displayErrorNotice(int $code): void {
    $message = $this->getErrorMessage($code);
    $this->setMessage($message);
    $this->message;
    wp_admin_notice($this->message, [
      'type' => 'error',
      'dismissible' => true
    ]);
  }

  public function getErrorMessage(int $code): string {
    $errorMessages = [
      self::NETWORK_ERROR => 'Network error',
      self::CACHE_ERROR => 'Cache error',
      self::INVALID_RESPONSE => 'Invalid response',
      self::INVALID_JSON => 'Invalid JSON',
      self::CURL_TIMEOUT_ERROR => 'Request timeout error',
      self::CURL_INIT_ERROR => 'Request initialization error'
    ];

    $message = isset($errorMessages[$code]) ? $errorMessages[$code] : 'Invalid error code';
    return $message;
  }
}