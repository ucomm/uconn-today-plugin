<?php

namespace UCTP\Services;

class ShortcodeService {
  const SHORTCODE_SLUG = 'UC-Today';
  const SHORTCODE_ATTS = [
    'author' => null,
    // deprecated as of 2.5.0
    // 'campus' => null,
    'category' => null,
    'columns' => '4',
    // deprecated as of 2.5.0
    // 'county' => null,
    'offset' => '',
    'pictures' => false,
    'number-of-posts' => '4',
    'post-slugs' => null,
    'read-more-text' => '',
    // deprecated as of 2.5.0
    // 'sc-post-slugs' => null,
    // 'school-college' => null,
    'search' => null,
    'include' => null,
    'show-excerpt' => false,
    'show-date' => false,
    'tag' => null,
    // deprecated as of 2.5.0
    // 'zone' => null,
    'debug' => false,
    'safe-fetch' => true,
    'style' => ''
  ];

  protected array $publicAtts = [];

  public function __construct() {
    $this->publicAtts = [];
  }

  public function getShortcodeSlug(): string {
    return self::SHORTCODE_SLUG;
  }

  public function getDefaultAtts(): array {
    return self::SHORTCODE_ATTS;
  }

  /**
   * Public attributes will be read-only. The shortcode attributes are set in `setAtts`. If needed they can then be fetched with this method
   *
   * @return array
   */
  public function getPublicAtts(): array {
    return $this->publicAtts;
  }

  /**
   * Set the shortcode attributes and validate them before passing them to `Request` child classes
   *
   * @param array $atts the shortcode attributes
   * @return array
   */
  public function setAtts(array $atts): array {
    $atts = array_change_key_case($atts, CASE_LOWER);
    $cleanAtts = $this->sanitizeAtts($atts);
    $this->publicAtts = shortcode_atts(
      self::SHORTCODE_ATTS,
      $cleanAtts,
      self::SHORTCODE_SLUG
    );
    return $this->publicAtts;
  }

  protected function sanitizeAtts(array $atts): array {
    return array_map('sanitize_text_field', $atts);
  }
}