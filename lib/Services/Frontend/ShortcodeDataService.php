<?php

namespace UCTP\Services;

use UCTP\Queries\PostREST;
use UCTP\Queries\SearchREST;
use UCTP\Services\PostDataNormalizer;
use UCTP\Services\SearchDataNormalizer;

/**
 * This class is only meant to implement getters and setters for the data services.
 * All the business logic is handled in those classes
 */
class ShortcodeDataService {
  protected PostREST $postRequester;
  protected SearchREST $searchRequester;
  protected PostDataNormalizer $postNormalizer;
  protected SearchDataNormalizer $searchNormalizer;

  public function __construct() {
    $this->postRequester = new PostREST(true);
    $this->searchRequester = new SearchREST();
    $this->postNormalizer = new PostDataNormalizer([]);
    $this->searchNormalizer = new SearchDataNormalizer([]);
  }

  public function getPostRequester(): PostREST {
    return $this->postRequester;
  }

  public function setPostRequester(
    bool $safeFetch, 
    array $queryAtts = [], 
    bool $usePictures = true, 
    string $endpoint = '/posts'
  ): PostREST {
    $this->postRequester = new PostREST(
      $safeFetch, 
      $queryAtts, 
      $usePictures, 
      $endpoint
    );
    return $this->postRequester;
  }

  public function getSearchRequester(): SearchREST {
    return $this->searchRequester;
  }

  public function setSearchRequester(array $queryAtts = [], string $endpoint = '/search'): SearchREST {
    $this->searchRequester = new SearchREST($queryAtts, $endpoint);
    return $this->searchRequester;
  }

  public function getPostNormalizer(): PostDataNormalizer {
    return $this->postNormalizer;
  }

  public function setPostNormalizer(array $postData): PostDataNormalizer {
    $this->postNormalizer = new PostDataNormalizer($postData);
    return $this->postNormalizer;
  }

  public function getSearchNormalizer(): SearchDataNormalizer {
    return $this->searchNormalizer;
  }

  public function setSearchNormalizer(array $searchData): SearchDataNormalizer {
    $this->searchNormalizer = new SearchDataNormalizer($searchData);
    return $this->searchNormalizer;
  }
}