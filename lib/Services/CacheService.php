<?php

namespace UCTP\Services;

class CacheService {
  protected string $transientPrefix;
  protected int $cacheDuration;

  public function __construct() {
    $this->transientPrefix = 'uctoday-';
    $this->cacheDuration = 6 * HOUR_IN_SECONDS;
  }

  public function getTransientPrefix(): string {
    return $this->transientPrefix;
  }

  public function getCacheDuration(): int {
    return $this->cacheDuration;
  }

  /**
   * Set a transient ID according to the requested route including the query string
   *
   * @param string $route
   * @return string
   */
  public function transientID(string $route): string {
    $transientKey = $this->transientPrefix;
    $parsedRoute = parse_url($route);
    $query = $parsedRoute['query'] ?? 'empty-query';

    $transientKey .= preg_replace('/[=&+]+/', '_', $query);

    return $transientKey;
  }

  /**
   * Cache an API response for a given transient ID
   * The duration will be either 6 hours or the value set in the plugin settings
   *
   * @param ResponseData $data
   * @param string $transientID
   * @return ResponseData
   */
  public function cacheResponse(
    ResponseData $data,
    string $transientID
  ): ResponseData {

    $settings = get_option('uctoday_plugin_setings', []);
    $hasCacheDuration = isset($settings['uctoday_plugin_setting_cache_duration']) && $settings['uctoday_plugin_setting_cache_duration'] > 0;

    if ($hasCacheDuration) {
      $this->cacheDuration = intval($settings['uctoday_plugin_setting_cache_duration']);
    }

    $didSetTransient = set_transient($transientID, $data, $this->cacheDuration);

    if (!$didSetTransient) {
      return new ResponseData(true, null, 0, 2);
    }

    $withTransient = get_transient($transientID);
    return $withTransient instanceof ResponseData ? $withTransient : new ResponseData(true, null, 0, 2);
  }

  /**
   * Get all possible cached transient IDs for the plugin.
   * Use of a SQL query is intentional because we can't know all the possible transient IDs since they're created based on the query string.
   *
   * @return null|array
   */
  public function getTransientCacheNames() {
    if (!function_exists('wp_get_current_user')) {
      include ABSPATH . 'wp-includes/pluggable.php';
    }

    global $wpdb;

    $tableName = $this->getTableName();

    $sql = $wpdb->prepare(
      "SELECT option_name FROM " . $tableName .
      " WHERE option_name LIKE %s;",
      '%' . $wpdb->esc_like('_transient_' . $this->transientPrefix) . '%'
    );

    $queryResult = $wpdb->get_results($sql);
    return $queryResult;
  }

  /**
   * Get all cached response values for the plugin
   *
   * @return array
   */
  public function getAllResponseData(): array {
    $transientNames = $this->getTransientCacheNames();

    $responses = array_map([$this, 'getSingleResponseData'], $transientNames);
    return $responses;
  }


  /**
   * Only for use within `array_map` to get the cached data from a single option
   * 
   * _Do not_ call this method directly
   *
   * @param object $option the option object from the database
   * @return mixed response based on the final result of `get_transient`
   */
  protected function getSingleResponseData(object $option) {
    $prefix = '_transient_';
    $transientID = str_replace($prefix, '', $option->option_name);
    return get_transient($transientID);
  }

  public function initForceClearCache() {
    if (!isset($_POST['clear-uct-cache']) || $_POST['clear-uct-cache'] !== 'true') {
      return;
    }

    if (!function_exists('wp_redirect')) {
      require ABSPATH . 'wp-includes/pluggable.php';
    }

    $this->forceClearCache();

    wp_redirect($_SERVER['HTTP_REFERER']);
    exit;
  }

  /**
   * Clears all cached values for the plugin via SQL query
   *
   * @return bool
   */
  public function forceClearCache(): bool {
    if (!function_exists('wp_get_current_user')) {
      include ABSPATH . 'wp-includes/pluggable.php';
    }

    global $wpdb;

    // authors and up can clear the cache
    if (!current_user_can('manage_options')) {
      return false;
    }

    $tableName = $this->getTableName();

    $sql = $wpdb->prepare(
      "DELETE FROM " . $tableName .
        " WHERE option_name LIKE %s;",
      '%' . $wpdb->esc_like('_' . $this->transientPrefix) . '%'
    );
    $queryResult = $wpdb->query($sql);
    return $queryResult !== false;
  }

  /**
   * Get the name of the options table
   * Ensure compatibility with multisite installations
   *
   * @return string
   */
  public function getTableName(): string {
    $tableName = 'wp_';

    if (is_multisite() && get_blog_details()->blog_id > 1) {
      $details = get_blog_details();
      $tableName .= $details->blog_id . '_';
    }

    $tableName .= 'options';

    return $tableName;
  }
}