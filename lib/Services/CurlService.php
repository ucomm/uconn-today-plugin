<?php

namespace UCTP\Services;

use UCTP\Interfaces\HttpRequest;
use UCTP\Interfaces\ErrorValidation;

/**
 * Wrapper class for the curl response. This class allows us to easily test the response data.
 */
class ResponseData implements ErrorValidation {

  public $error;
  public $errorCode;
  public $response;
  public $httpCode;

  public function __construct(
    bool $error, 
    $response, 
    int $httpCode, 
    int $errorCode = self::NO_ERROR
  ) {
    $this->error = $error;
    $this->errorCode = $this->validateErrorCode($errorCode);
    $this->response = $response;
    $this->httpCode = $httpCode;
  }

  /**
   * Prevent invalid error codes from being set.
   *
   * @param integer $code
   * @return integer
   */
  public function validateErrorCode(int $code): int {
    return in_array($code, [
      self::NO_ERROR,
      self::NETWORK_ERROR,
      self::CACHE_ERROR,
      self::INVALID_RESPONSE,
      self::INVALID_JSON,
      self::CURL_TIMEOUT_ERROR,
      self::CURL_INIT_ERROR
    ]) ? $code : self::INVALID_ERROR_CODE;
  }
}

/**
 * Aurora has disabled a core PHP function for security that is used by wp_remote_get.
 * 
 * Moving the curl request to a separate class makes it easier to test and maintain.
 *
 */
class CurlService implements HttpRequest {
  public $ch;

  public function __construct() {
    $this->ch = $this->init();
  }
  /**
   * Initialize a curl request and return the response data.
   * 
   * @param string $url
   * @return ResponseData
   */
  public function request(string $url): ResponseData {
    // initialize the request
    $this->ch = $this->initCurl($url);

    // there was an initialization error. bail immediately.
    if ($this->ch instanceof ResponseData) {
      return $this->ch;
    }

    // execute the request and get the raw data
    $rawData = $this->exec();
    $httpCode = $this->getInfo(CURLINFO_HTTP_CODE);
    $response = $this->handleCurlResponse($this->ch, $rawData, $httpCode);
    
    // close the request and return the data
    $this->close();
    return $response;
  }

  /**
   * Initialize a curl request. If there is an error, return a ResponseData object.
   *
   * @param string $url
   * @return mixed
   */
  protected function initCurl(string $url) {
    if ($this->ch === false) {
      return new ResponseData(true, null, 0, ResponseData::CURL_INIT_ERROR);
    }

    $this->setOption(CURLOPT_URL, $url);
    $this->setOption(CURLOPT_RETURNTRANSFER, 1);
    $this->setOption(CURLOPT_CONNECTTIMEOUT, 5);
    $this->setOption(CURLOPT_TIMEOUT, 10);

    return $this->ch;
  }
  
  /**
   * Handle the curl response and any errors. Always return a `ResponseData` object.
   *
   * @param mixed $ch
   * @param mixed $rawData
   * @param int $httpCode
   * @return ResponseData
   */
  protected function handleCurlResponse($ch, $rawData, int $httpCode): ResponseData {
    $curlError = $this->errno($ch);
    if ($curlError === CURLE_OPERATION_TIMEOUTED) {
      return new ResponseData(true, null, 0, ResponseData::CURL_TIMEOUT_ERROR);
    }

    if ($httpCode >= 400) {
      // there may be useful information in the response even in the case of an error
      $response = $this->decodeRawData($rawData);
      return new ResponseData(true, $response, $httpCode, ResponseData::NETWORK_ERROR);
    }

    if ($rawData === false) {
      return new ResponseData(true, $rawData, $httpCode, ResponseData::INVALID_RESPONSE);
    }

    $responseData = $this->decodeRawData($rawData);
    return new ResponseData(false, $responseData, $httpCode);
  }

  /**
   * Decode raw data from the curl request. In the event that the data is not both a string and decodable JSON, return an empty array. This will force all request responses to be arrays to prevent errors in the Requester class.
   * 
   * In the unlikely event that an object is returned from the API, it will be forced to an array
   *
   * @param mixed $rawData - ideally this will be a properly formatted JSON string.
   * @return array decoded data such as post objects
   */
  protected function decodeRawData($rawData): array {
    // ensure that there's a valid response and if not, bail by returning an empty array
    $decodedData = is_string($rawData) && json_decode($rawData) ? 
      json_decode($rawData) : 
      [];

    if (is_object($decodedData)) {
      $decodedData = (array) $decodedData;
    } 
    return $decodedData;
  }

  /**
   *
   * Implement the HttpRequest interface methods.
   *
   */
  public function init() {
    $this->ch = curl_init();
    return $this->ch;
  }

  public function setOption(string $name, $value): void {
    curl_setopt($this->ch, $name, $value);
  }

  public function exec() {
    return curl_exec($this->ch);
  }

  public function close(): void {
    curl_close($this->ch);
  }

  public function errno() {
    return curl_errno($this->ch);
  }

  public function getInfo($name) {
    return curl_getinfo($this->ch, $name);
  }
}