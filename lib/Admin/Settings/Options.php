<?php

namespace UCTP\Admin\Settings;

use UCTP\Services\CacheService;

/**
 * Manages the options that will go into the wp_options table
 */
class Options {
  private $settingsName;
  private $settingsGroup;
  private $settingsPage;

  private $cacheService;

  /**
   * @param string|null $settingsPage - the menu slug of the settings page
   */
  public function __construct(?string $settingsPage = null, ?CacheService $cacheService = null)
  {
    $this->settingsName = 'uctoday_plugin_settings';
    $this->settingsGroup = 'uctoday_plugin';
    $this->settingsPage = $settingsPage ?? 'uctoday_plugin';
    $this->cacheService = $cacheService ?? new CacheService();
  }

  public function getSettingsName(): string {
    return $this->settingsName;
  }

  public function getSettingsGroup(): string {
    return $this->settingsGroup;
  } 

  public function getSettingsPage(): string {
    return $this->settingsPage;
  }

  /**
   * Callback function to:
   * - register settings 
   * - add settings sections
   * - add settings fields
   *
   * @return void
   */
  public function init(): void {
    $this->registerSettings();
    $this->addSettingsSection();
    $this->addSettingsFields();
  }

  /**
   * Wrapper for the WP register_settings function
   *
   * @return void
   */
  public function registerSettings() {
    register_setting($this->settingsGroup, $this->settingsName);
  }

  /**
   * Creates a settings section. Uses a callback to fetch a partial to display the section
   *
   * @return void
   */
  public function addSettingsSection() {
    $page = $this->settingsPage;
    add_settings_section(
      'uctoday_plugin_general', // key/id
      'General Settings', // section title
      array($this, 'generalSettingsSectionCallback'),
      $page
    );
  }

  /**
   * Create the actual setting form fields. 
   * Uses callbacks to fetch partials to display the field inputs.
   *
   * @return void
   */
  public function addSettingsFields() {
    $page = $this->settingsPage;
    // Defines and setting to disable the custom styles that UConn Today Plugin provides.
    add_settings_field(
      'uctoday_plugin_setting_disable_styles', //key/id
      'Disable UConn Today Plugin Custom Styles',
      [$this, 'settingDisableStylesCallback'], // callback
      $page, // page
      'uctoday_plugin_general', // section
      '' //args
    );

    add_settings_field(
      'uctoday_plugin_setting_graphql_mode',
      'Activate JS Plugin Mode',
      [$this, 'settingGraphQLCallback'],
      $page,
      'uctoday_plugin_general'
    );

    add_settings_field(
      'uctoday_plugin_setting_cache_duration',
      'UCToday Plugin Cache Duration',
      [ $this, 'settingCacheDurationCallback' ],
      $page,
      'uctoday_plugin_general'
    );

    if (wp_get_environment_type() === 'local') {
      add_settings_field(
        'uctoday_plugin_setting_tandem_mode',
        'Enable Tandem Mode',
        array($this, 'settingTandemModeCallback'),
        $page,
        'uctoday_plugin_general'
      );
    }
  }

  public function generalSettingsSectionCallback() {
    echo '';
  }

  public function settingDisableStylesCallback() {
    $disableStylesSetting =
    $this->getSettingValue('uctoday_plugin_setting_disable_styles');
    include(UCT_PLUGIN_DIR . 'partials/admin/disable-styles-input.php');
  }

  public function settingTandemModeCallback() {
    $tandemModeSetting =
    $this->getSettingValue('uctoday_plugin_setting_tandem_mode');
    include(UCT_PLUGIN_DIR . 'partials/admin/tandem-mode-input.php');
  }

  public function settingGraphQLCallback() {
    $graphqlSetting = $this->getSettingValue('uctoday_plugin_setting_graphql_mode');
    include(UCT_PLUGIN_DIR . 'partials/admin/graphql-mode-input.php');
  }

  public function settingCacheDurationCallback() {
    $cacheDurationSetting = $this->getSettingValue('uctoday_plugin_setting_cache_duration', 6);
    include(UCT_PLUGIN_DIR . 'partials/admin/cache-duration.php');
  }

  /**
   * Clears all cached values for the UCT plugin via SQL query.
   * Run when the "Clear UConn Today Cache" submit button is clicked.
   *
   * @return bool
   */
  public function clearUCTCache(): bool {
    return $this->cacheService->forceClearCache();
  }

  /**
   * Gets the checked value of a setting
   *
   * @param string $arrayKey
   * @return bool
   */
  private function getSettingValue(string $arrayKey, $defaultValue = '') {
    $options = get_option('uctoday_plugin_settings', []);
    if ($options === '') {
      $options = [];
    }
    return array_key_exists($arrayKey, $options) ?
      $options[$arrayKey] : 
      $defaultValue;
  }
}
