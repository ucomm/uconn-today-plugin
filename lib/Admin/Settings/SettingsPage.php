<?php

namespace UCTP\Admin\Settings;

use UCTP\Interfaces\ErrorMessage;
use UCTP\Services\CacheService;
use UCTP\Services\NotificationService;

class SettingsPage implements ErrorMessage {
  protected CacheService $cacheService;
  protected ?NotificationService $notificationService;
  private $menuSlug;

  public function __construct(
    CacheService $cacheService,
    ?NotificationService $notificationService = null
  )
  {
    $this->menuSlug = 'uctoday_settings';
    $this->cacheService = $cacheService;
    $this->notificationService = $notificationService ?? new NotificationService('', '');
  }

  public function getMenuSlug(): string {
    return $this->menuSlug;
  }

  public function init() {
    add_options_page(
      __('UConn Today Plugin', 'uconn-today-plugin'),
      __('UConn Today Plugin', 'uconn-today-plugin'),
      'manage_options',
      $this->menuSlug,
      [$this, 'getOptionsPage']
    );
    
  }

  public function getOptionsPage() {
    // get all response data from all the transients
    $responseData = $this->cacheService->getAllResponseData();
    include UCT_PLUGIN_DIR . 'partials/admin/options-page.php';
  }

  public function getErrorMessage(int $code): string {
    $errorMessages = [
      self::NETWORK_ERROR => 'Network error',
      self::CACHE_ERROR => 'Cache error',
      self::INVALID_RESPONSE => 'Invalid response',
      self::INVALID_JSON => 'Invalid JSON',
      self::CURL_TIMEOUT_ERROR => 'Request timeout error',
      self::CURL_INIT_ERROR => 'Request initialization error'
    ];

    $message = isset($errorMessages[$code]) ? $errorMessages[$code] : 'Invalid error code';
    return $message;
  }
}
