<?php

namespace UCTP\Admin\Menus;

use WP_Admin_Bar;

class AdminBar {
  public function addMenuBar(WP_Admin_Bar $adminBar) {
    $this->addMenuGroup($adminBar);
    $this->addClearCacheMenu($adminBar);
    $this->addStatusLink($adminBar);
    $this->currentShortcodeAttributes($adminBar);
  }

  public function addMenuGroup(WP_Admin_Bar $adminBar): void {
    $adminBar->add_menu([
      'id' => 'uctoday-plugin-admin-bar-group',
      'title' => 'UConn Today Plugin',
      'href' => '#'
    ]);
  }

  public function addClearCacheMenu(WP_Admin_Bar $adminBar): void {
    $adminBar->add_menu([
      'id' => 'uctoday-plugin-admin-bar-clear-cache',
      'parent' => 'uctoday-plugin-admin-bar-group',
      'title' => 'Clear UConn Today Cache',
      'meta' => [
        'html' => '<form method="POST" action="/">
          <input type="hidden" name="clear-uct-cache" value="true">
          <input type="submit" value="Clear UConn Today Cache">
        </form>'
      ]
    ]);
  }

  public function addStatusLink(WP_Admin_Bar $adminBar): void {
    $adminUrl = admin_url('options-general.php?page=uctoday_settings#uctoday-status');
    $adminBar->add_menu([
      'id' => 'uctoday-plugin-admin-bar-status',
      'parent' => 'uctoday-plugin-admin-bar-group',
      'title' => 'View Plugin Status/Cache Groups',
      'href' => $adminUrl
    ]);
  }

  public function currentShortcodeAttributes(WP_Admin_Bar $adminBar): void {
    $adminBar->add_menu([
      'id' => 'uctoday-plugin-admin-bar-shortcode-attributes',
      'parent' => 'uctoday-plugin-admin-bar-group',
      'title' => 'Current Shortcode Attributes',
      'meta' => [
        'html' => '<div id="uct-shortcode-atts"></div>'
      ]
    ]);
  }
}