<?php

/**
 * This file builds up shortcode attributes and then passes them to the Shortcode class display method. It should not independently create any markup.
 */

$atts = [
  'number-of-posts' => $settings->posts_num,
  'offset' => $settings->posts_offset,
  'columns' => $settings->columns_num,
  'pictures' => $settings->pictures,
  'read-more-text' => $settings->read_more_text,
  'show-excerpt' => filter_var($settings->show_excerpt, FILTER_VALIDATE_BOOLEAN),
  'show-date' => filter_var($settings->show_date, FILTER_VALIDATE_BOOLEAN)
];

switch ($settings->post_type_select) {
  case 'category':
    $atts['category'] = $settings->category_field;
    break;
  case 'school-college':
  case 'full-school-posts':
    $atts['category'] = $settings->school_field;
    break;
  case 'campus':
    $atts['category'] = $settings->campus_field;
    break;
  case 'county':
    $atts['category'] = $settings->county_field;
    break;
  case 'author':
    $atts['author'] = $settings->author_field;
    break;
  case 'search':
    $atts['search'] = $settings->search_field;
    break;
  default:
    # code...
    break;
}

if ($module->shortcode->useGraphQL) {
  $module->scriptLoader->localizeScript([
    'isUserLoggedIn' => is_user_logged_in(),
    'siteTitle' => get_bloginfo('name')
  ]);
}

echo $module->shortcode->display($atts);