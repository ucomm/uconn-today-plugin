<?php

namespace UCTP\Frontend\BeaverBuilder;

use FLBuilderModule;
use UCTP\Assets\ScriptLoader;
use UCTP\Assets\StyleLoader;
use UCTP\Frontend\Shortcode;

/**
 * Creates the UCT Beaver Builder module. It also handles enqueuing module js/css on a case by case basis.
 */
class UCToday extends FLBuilderModule {

  public ScriptLoader $scriptLoader;
  public Shortcode $shortcode;
  public StyleLoader $styleLoader;
  private $handle;
  private $buildDir;

  public function __construct() {
    parent::__construct(array(
      'name'            => __('UConn Today', 'uconn-today-plugin'),
      'description'     => __('UConn Today module for Beaver Builder', 'uconn-today-plugin'),
      'category'        => __('UConn Today', 'uconn-today-plugin'),
      'dir'             => UCT_PLUGIN_DIR . 'lib/Frontend/BeaverBuilder/uct-module/',
      'url'             => UCT_PLUGIN_URL . 'lib/Frontend/BeaverBuilder/uct-module/',
      'icon'            => 'button.svg',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));

    $this->scriptLoader = new ScriptLoader('UC-Today');
    $this->styleLoader = new StyleLoader('UC-Today');
    $this->shortcode = new Shortcode();
    $this->handle = $this->scriptLoader->getHandle();
    $this->buildDir = $this->scriptLoader->getBuildDir();

    $this->prepareMainCSS();

    if ($this->shortcode->useGraphQL) {
      $this->prepareAppMainCSS();
      $this->prepareVendorJS();
      $this->prepareAppMainJS();
    }
  }

  /**
   * Get and enqueue the main JS chunk for the reader mode application
   *
   * @return void
   */
  public function prepareAppMainJS() {
    $scriptDeps = array_map(function($chunk) {
      return $this->handle . '-' . $chunk;
    }, $this->scriptLoader->chunkNames);

    $this->add_js(
      $this->handle,
      UCT_PLUGIN_URL . $this->buildDir . '/' . $this->scriptLoader->mainChunk . '.js',
      $scriptDeps,
      false,
      true
    );
  }

  /**
   * Enqueue all the vendor chunks for the reader mode application. These will be fetched based on their names as determined by the ScriptLoader class.
   *
   * @return void
   */
  public function prepareVendorJS() {
    $chunkNames = $this->scriptLoader->chunkNames;
    foreach ($chunkNames as $chunk) {
      $this->add_js(
        $this->handle . '-' . $chunk,
        UCT_PLUGIN_URL . $this->buildDir . '/' . $chunk . '.js',
        [],
        false,
        true
      );
    }
  }

  /**
   * Enqueue the main UCT plugin styles
   *
   * @return void
   */
  public function prepareMainCSS() {
    $this->add_css(
      $this->handle,
      UCT_PLUGIN_URL . 'public-assets/css/uconn-today-plugin-public.css'
    );
  }

  /**
   * Enqueue vendor styles for the reader mode application
   *
   * @return void
   */
  public function prepareAppCSSChunks() {
    if (count($this->styleLoader->chunkNames) < 1) return;

    foreach ($this->styleLoader->chunkNames as $name) {
      $this->add_css(
        $this->handle . '-' . $name,
        UCT_PLUGIN_URL . $this->buildDir . '/' . $name . '.css',
        [],
        null
      );
    }
  }

  /**
   * Enqueue application styles for the reader mode application
   *
   * @return void
   */
  public function prepareAppMainCSS() {

    $styleDeps = [ $this->handle ];

    if (count($this->styleLoader->chunkNames) > 0) {
      foreach ($this->styleLoader->chunkNames as $name) {
        array_push($styleDeps, $name);
      }
    }

    $this->add_css(
      $this->handle . '-app',
      UCT_PLUGIN_URL . $this->buildDir . '/' . $this->styleLoader->mainChunk . '.css',
      $styleDeps
    );
  }
}