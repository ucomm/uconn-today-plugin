<?php

namespace UCTP\Frontend\BeaverBuilder;

use FLBuilder;

/**
 * Create the settings form for the UCT Beaver Builder module. This is done off the loadModule method. The class also registers the UCToday module class.
 */
class UConnTodayModule {
  public function loadModule() {
      FLBuilder::register_module('UCTP\Frontend\BeaverBuilder\UCToday', array(
      //Tab for UC-Today shortcode formatting
      'main_tab'    => array(
        'title'      => __('Main', 'uconn-today-plugin'),
        'sections'    => array(

          //Post-type shortcode attributes section
          'shortcode_atts'  => array(
            'title'        => __('Shortcode Attributes', 'uconn-today-plugin'),
            'fields'      => array(

              //Post-type selector field (main-feed, school-college, category)
              'post_type_select'  => array(
                'type'        => 'select',
                'label'        => __('Type of Posts', 'uconn-today-plugin'),
                'description' => '<strong>The category option is prefered.</strong>',
                'default'      => 'category',
                'options'      => array(
                  'main-feed'      => __('Main Feed', 'uconn-today-plugin'),
                  'author' => __('Author (by netID)', 'uconn-today-plugin'),
                  'campus' => __('Campus', 'uconn-today-plugin'),
                  'category'      => __('Category', 'uconn-today-plugin'),
                  'county' => __('County', 'uconn-today-plugin'),
                  'full-school-posts' => __('Full School Posts (includes zoned posts)', 'uconn-today-plugin'),
                  'school-college'  => __('School/College', 'uconn-today-plugin'),
                  'search' => __('Relevanssi Search', 'uconn-today-plugin'),
                ),
                'toggle'      => array(
                  'main-feed'      => array(),
                  'school-college'  => array(
                    'fields'      => array('school_field')
                  ),
                  'author' => array(
                    'fields' => array('author_field')
                  ),
                  'campus' => array(
                    'fields' => array('campus_field')
                  ),
                  'category'      => array(
                    'fields'      => array('category_field')
                  ),
                  'county' => array(
                    'fields' => array('county_field')
                  ),
                  'full-school-posts' => array(
                    'fields' => array('zone_field', 'school_field')
                  ),
                  'search' => array(
                    'fields' => array('search_field')
                  ),
                )
              ),
              //School-college field
              'school_field'    => array(
                'type'        => 'select',
                'label'        => __('School/College', 'uconn-today-plugin'),
                'default'      => 'college-of-agriculture-health-natural-resources',
                'options'      => array(
                  'college-of-agriculture-health-natural-resources'    => __('College of Agriculture, Health, and Natural Resources', 'uconn-today-plugin'),
                  'college-of-liberal-arts-and-sciences'          => __('College of Liberal Arts and Sciences', 'uconn-today-plugin'),
                  'neag-school-of-education'                => __('Neag School of Education', 'uconn-today-plugin'),
                  'the-graduate-school'                  => __('The Graduate School', 'uconn-today-plugin'),
                  'research' => __('Research', 'uconn-today-plugin'),
                  'school-of-business'                  => __('School of Business', 'uconn-today-plugin'),
                  'school-of-engineering'                  => __('School of Engineering', 'uconn-today-plugin'),
                  'school-of-fine-arts'                  => __('School of Fine Arts', 'uconn-today-plugin'),
                  'school-of-law'                      => __('School of Law', 'uconn-today-plugin'),
                  'school-of-nursing'                    => __('School of Nursing', 'uconn-today-plugin'),
                  'school-of-pharmacy'                  => __('School of Pharmacy', 'uconn-today-plugin'),
                  'school-of-social-work'                  => __('School of Social Work', 'uconn-today-plugin'),
                  'schools-of-medicine-and-dental-medicine'        => __('Schools of Medicine and Dental Medicine', 'uconn-today-plugin'),
                )
              ),

              // Author field
              'author_field' => array(
                'type' => 'text',
                'label' => __('Author', 'uconn-today-plugin')
              ),

              // Campus field
              'campus_field' => array(
                'type' => 'select',
                'label' => __('Campus', 'uconn-today-plugin'),
                'default' => 'storrs',
                'options' => array(
                  'storrs' => __('Storrs', 'uconn-today-plugin'),
                  'avery-point' => __('Avery Point', 'uconn-today-plugin'),
                  'hartford' => __('Hartford', 'uconn-today-plugin'),
                  'stamford' => __('Stamford', 'uconn-today-plugin'),
                  'waterbury' => __('Waterbury', 'uconn-today-plugin')
                )
              ),
              'category_field'  => array(
                'type'        => 'text',
                'label'        => __('Enter a category slug', 'uconn-today-plugin'),
                'description' => __('<p>For a list of category slugs, please visit the <a href="https://today.uconn.edu/wordpress/wp-admin/admin.php?page=uconn-today-theme-category-lookup">UConn Today category lookup page</a>.</p>'),
                'default'      => 'arts',
              ),
              // County Field
              'county_field' => array(
                'type' => 'select',
                'label' => __('County', 'uconn-today-plugin'),
                'default' => 'fairfield-county',
                'options' => array(
                  'all' => __('All Counties', 'uconn-today-plugin'),
                  'fairfield-county' => __('Fairfield County', 'uconn-today-plugin'),
                  'hartford-county' => __('Hartford County', 'uconn-today-plugin'),
                  'litchfield-county' => __('Litchfield County', 'uconn-today-plugin'),
                  'middlesex-county' => __('Middlesex County', 'uconn-today-plugin'),
                  'new-haven-county' => __('New Haven County', 'uconn-today-plugin'),
                  'new-london-county' => __('New London County', 'uconn-today-plugin'),
                  'tolland-county' => __('Tolland County', 'uconn-today-plugin'),
                  'windham-county' => __('Windham County', 'uconn-today-plugin')
                )
              ),
              // Zone Posts
              'zone_field' => array(
                'type' => 'text',
                'label' => __('Zone', 'uconn-today-plugin'),
                'description' => __('The slug of the zone being requested', 'uconn-today-plugin')
              ),
              // Search field
              'search_field' => array(
                'type' => 'text',
                'label' => __('Search', 'uconn-today-plugin')
              ),
            )
          ),

          //Post formatting section
          'post_formatting'  => array(
            'title'        => __('Post Formatting', 'uconn-today-plugin'),
            'fields'      => array(

              //Number of posts field
              'posts_num'      => array(
                'type'        => 'select',
                'label'        => __('Number of Posts', 'uconn-today-plugin'),
                'default'      => '4',
                'options'      => array(
                  '1'          => __('1', 'uconn-today-plugin'),
                  '2'          => __('2', 'uconn-today-plugin'),
                  '3'          => __('3', 'uconn-today-plugin'),
                  '4'          => __('4', 'uconn-today-plugin'),
                  '5'          => __('5', 'uconn-today-plugin'),
                  '6'          => __('6', 'uconn-today-plugin'),
                  '7'          => __('7', 'uconn-today-plugin'),
                  '8'          => __('8', 'uconn-today-plugin'),
                  '9'          => __('9', 'uconn-today-plugin'),
                  '10'        => __('10', 'uconn-today-plugin'),
                  '11'        => __('11', 'uconn-today-plugin'),
                  '12'        => __('12', 'uconn-today-plugin'),
                )
              ),

              // post offset
              'posts_offset'      => array(
                'type'        => 'select',
                'label'        => __('Post offset', 'uconn-today-plugin'),
                'default'      => '0',
                'options'      => array(
                  '0'          => __('0', 'uconn-today-plugin'),
                  '1'          => __('1', 'uconn-today-plugin'),
                  '2'          => __('2', 'uconn-today-plugin'),
                  '3'          => __('3', 'uconn-today-plugin'),
                  '4'          => __('4', 'uconn-today-plugin'),
                  '5'          => __('5', 'uconn-today-plugin'),
                  '6'          => __('6', 'uconn-today-plugin'),
                  '7'          => __('7', 'uconn-today-plugin'),
                  '8'          => __('8', 'uconn-today-plugin'),
                  '9'          => __('9', 'uconn-today-plugin'),
                  '10'        => __('10', 'uconn-today-plugin'),
                )
              ),              

              //Columns field
              'columns_num'    => array(
                'type'        => 'select',
                'label'        => __('Columns', 'uconn-today-plugin'),
                'default'      => '4',
                'options'      => array(
                  '1'          => __('1', 'uconn-today-plugin'),
                  '2'          => __('2', 'uconn-today-plugin'),
                  '3'          => __('3', 'uconn-today-plugin'),
                  '4'          => __('4', 'uconn-today-plugin'),
                )
              ),

              //Pictures field
              'pictures'      => array(
                'type'        => 'select',
                'label'        => __('Display Pictures', 'uconn-today-plugin'),
                'default'      => 'false',
                'options'      => array(
                  'true'        => __('true', 'uconn-today-plugin'),
                  'false'        => __('false', 'uconn-today-plugin'),
                )
              ),
              'read_more_toggle' => array(
                'type' => 'select',
                'label' => __('Enable or disable "Read More" text', 'uconn-today-plugin'),
                'default' => 'false',
                'options' => array(
                  'true' => __('Show "Read More" text', 'uconn-today-plugin'),
                  'false' => __('Hide "Read More" text', 'uconn-today-plugin')
                ),
                'toggle' => array(
                  'true' => array(
                    'fields' => array( 'read_more_text' )
                  )
                )
              ),
              'read_more_text' => array(
                'type' => 'text',
                'label' => __('Add "Read More" text', 'uconn-today-plugin')
              ),
              'show_excerpt' => array(
                'type' => 'select',
                'label' => __('Show or hide the article excerpt', 'uconn-today-plugin'),
                'default' => 'false',
                'options' => array(
                  'false' => __('Hide the excerpt', 'uconn-today-plugin'),
                  'true' => __('Show the excerpt', 'uconn-today-plugin'),
                ),
              ),
              'show_date' => array(
                'type' => 'select',
                'label' => __('Show or hide the article date', 'uconn-today-plugin'),
                'default' => 'false',
                'options' => array(
                  'false' => __('Hide the date', 'uconn-today-plugin'),
                  'true' => __('Show the date', 'uconn-today-plugin'),
                ),
              ),
            )
          )
        )
      ),
      'tab_2' => array(
        'title' => __('Style', 'uconn-today-plugin'),
        'sections' => array(
          'section_1' => array(
            'title' => __('Style', 'uconn-today-plugin'),
            'fields' => array(
              'uctoday_link_color' => array(
                'type' => 'color',
                'label' => __('Link Color', 'uconn-today-plugin'),
                'show_reset' => true,
                'default' => '#1540a2'
              ),
              'uctoday_link_hover_color' => array(
                'type' => 'color',
                'label' => __('Link Color', 'uconn-today-plugin'),
                'show_reset' => true,
                'default' => '#b50106'
              )
            )
          )
        )
      )
    ));
  }
}