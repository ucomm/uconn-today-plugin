<?php

namespace UCTP\Frontend;

use UCTP\Assets\ScriptLoader;
use UCTP\Services\ShortcodeService;
use UCTP\Services\ShortcodeDataService;

/**
 * This class manages
 * - shortcode attributes
 * - which partial to display based on attributes
 */
class Shortcode
{

  public $requester;
  public bool $showDebugInfo;
  public bool $useGraphQL;
  protected ShortcodeService $shortcodeService;
  protected ShortcodeDataService $shortcodeDataService;
  protected string $shortcodeSlug;
  protected array $atts;

  public function __construct() {

    $this->showDebugInfo = false;
    $this->shortcodeService = new ShortcodeService();
    $this->shortcodeDataService = new ShortcodeDataService();

    $this->shortcodeSlug = $this->shortcodeService->getShortcodeSlug();

    $settings = get_option('uctoday_plugin_settings');
    $gqlMode = isset($settings['uctoday_plugin_setting_graphql_mode']);
    $this->useGraphQL = $gqlMode ? 
      filter_var($settings['uctoday_plugin_setting_graphql_mode'], FILTER_VALIDATE_BOOLEAN) :
      false;
  }

  public function getShowDebugInfo(): bool {
    return $this->showDebugInfo;
  }

  public function setShowDebugInfo(bool $showDebugInfo): bool {
    $this->showDebugInfo = $showDebugInfo;
    return $this->showDebugInfo;
  }

  /**
   * Get the shortcode slug/attribute service
   *
   * @return ShortcodeService|null
   */
  public function getShortcodeService(): ?ShortcodeService {
    return $this->shortcodeService;
  }

  public function setShortcodeService(
    ShortcodeService $shortcodeService
  ): ShortcodeService {
    $this->shortcodeService = $shortcodeService;
    return $this->shortcodeService;
  }

  public function getShortcodeDataService(): ?ShortcodeDataService {
    return $this->shortcodeDataService;
  }

  public function setShortcodeDataService(
    ShortcodeDataService $shortcodeDataService
  ): ShortcodeDataService {
    $this->shortcodeDataService = $shortcodeDataService;
    return $this->shortcodeDataService;
  }

  /**
   * Hooked to `init`. 
   * Wraps the `add_shortcode` hook and prepares the shortcode with its services
   *
   * @return void
   */
  public function addShortcode(): void {
    $shortcodeSlug = $this->shortcodeService->getShortcodeSlug();
    add_shortcode($shortcodeSlug, [$this, 'display']);
  }

  /**
   * Get the attributes from the shortcode
   * - validate them
   * - pass them to the correct partial for display
   *
   * @param array|null $atts - passed from the add_shortcode hook
   * @return string - the HTML for the shortcode output
   */
  public function display(?array $atts = []): string
  {

    $path = '';

    $this->atts = $this->shortcodeService->setAtts($atts);
    
    $this->setShowDebugInfo($this->atts['debug']);

    // TODO handle errors/notices
    $showAdminNotice = false;

    if (!$this->useGraphQL) {
      $style = isset($this->atts['style']) ? 
        $this->atts['style'] : '';
      $withPictures = isset($this->atts['pictures']) ? 
        $this->atts['pictures'] : false;

      $path = $this->getPublicDisplayPath($style, $withPictures);
      $style = $this->getPicturesClass($style, $withPictures);

      $postRequester = $this->shortcodeDataService->setPostRequester(
        $this->atts['safe-fetch'],
        $this->atts,
        $this->atts['pictures'],
        '/posts'
      );

      $didPostRequest = $postRequester->handleRequest();
      $postData = isset($didPostRequest->response) ? 
        $didPostRequest->response : [];
      $postNormalizer = $this->shortcodeDataService->setPostNormalizer($postData);
      $stories = $postNormalizer->createNormalizedData();

      // I don't know how it wouldn't be an array at this point, but just in case...
      if (!is_array($stories)) {
        $stories = [];
      }

    } else {

      $path = UCT_PLUGIN_DIR . 'partials/public/display-js-graphql.php';
      $loader = new ScriptLoader($this->shortcodeSlug);
      $loader->localizeScript([
        'isUserLoggedIn' => is_user_logged_in(),
        'siteTitle' => get_bloginfo('name'),
      ]);
    }

    ob_start();
    $withAtts = array_filter($this->atts, function($att) {
      return $att;
    });
    $attMarkup = "<div class='shortcode-atts' style='display: none;'>";
    $attMarkup .= "<ul>";
    foreach ($withAtts as $key => $value) {
      $attMarkup .= "<li class='shortcode-att'>$key: $value</li>";
    }
    $attMarkup .= "</ul>";
    $attMarkup .= "</div>";
    include($path);
    $content = ob_get_clean();
    $content .= $attMarkup;
    return $content;
  }

  /**
   * Get the path to the correct partial file based on shortcode attributes
   *
   * @param string $style - the style attribute from the shortcode
   * @param boolean $withPictures - whether or not to use pictures
   * @return string
   */
  public function getPublicDisplayPath(string $style, bool $withPictures): string {
    $path = UCT_PLUGIN_DIR . 'partials/public/display-no-pictures.php';
    switch ($style) {
      case 'with-pictures':
        $path = UCT_PLUGIN_DIR . 'partials/public/display-with-pictures.php';
        break;
      case 'cards':
        $path = UCT_PLUGIN_DIR . 'partials/public/display-cards.php';
        break;
      case 'side-by-side':
        $path = UCT_PLUGIN_DIR . 'partials/public/display-side-by-side.php';
        break;
      default:
        if ($withPictures) {
          $path = UCT_PLUGIN_DIR . 'partials/public/display-with-pictures.php';
        }
        break;
    }
    return $path;
  }

  /**
   * Get the class for the pictures container based on the style attribute to pass to the partial file
   *
   * @param string $style
   * @param boolean $withPictures
   * @return string
   */
  public function getPicturesClass(string $style, bool $withPictures): string {
    $class = '';
    switch ($style) {
      case 'uconn-edu-dropshadow':
        $class = 'uconn-edu-dropshadow';
        break;

      default:
        if ($withPictures) {
          $class = 'with-pictures';
        }
        break;
    }
    return $class;
  }

  public function show_debug_info(): void {
    $showDebugInfo = $this->getShowDebugInfo();
    if ($showDebugInfo) : 
      global $post;
      $shortcodeSlug = $this->shortcodeService->getShortcodeSlug();
      $hasShortcode = has_shortcode($post->post_content, $shortcodeSlug);
      $atts = $this->shortcodeService->getPublicAtts();
      $route = $this->shortcodeDataService->getPostRequester()->route;
      echo "<pre>
      has UC-Today shortcode with: ";
        var_dump('request route: ', $route);
        var_dump('post shortcode status: ', $hasShortcode); 
        var_dump('request attributes: ', $atts);
      echo "</pre>";
    endif;
  }
}
