<?php

namespace UCTP\Frontend;

use UCTP\Queries\CategoryREST;
use WP_Widget;

class UCTWidget extends WP_Widget {
  public $args;
  public $id;
  public $name;
  public Shortcode $shortcode;
  public CategoryREST $requester;
  public $numOfCatsPerPage = 100; 

  public function __construct(
    ?Shortcode $shortcode = null,
    ?CategoryREST $requester = null
  )
  {
    $this->args = [];
    $this->id = 'uct-widget';
    $this->name = 'UConn Today Widget';
    $this->shortcode = $shortcode ?? new Shortcode();
    $this->requester = $requester ?? new CategoryREST();

    parent::__construct(
      $this->id,
      $this->name,
      [
        'description' => __('Display stories from UConn Today', 'uconn-today-plugin')
      ],
      [
        'id_base' => 'uct-widget'
      ]
    );

  }

  /**
   * Display UConn Today stories in a widget area.
   * We use the instance value of use-js-version to determine the display output. Otherwise all widgets get the same display treatment.
   *
   * @param array $args - arguments to affect how the widget is displayed
   * @param array $instance - the saved data from the widget
   * @return void
   */
  public function widget($args, $instance) {
    $useJSVersion = (!empty($instance['use-js-version'])) && $instance['use-js-version'] === 'on' ? true : false;

    if (array_key_exists('category', $instance)) { 
      while (is_array($instance['category'])) {
        if ('AND' === $instance['category-logical-type'] && count($instance['category']) > 1) {
          $instance['category'] = implode('+', $instance['category']);
        } else if ('OR' === $instance['category-logical-type'] && count($instance['category']) > 1) {
          $instance['category'] = implode(',', $instance['category']);
        } else {
          $instance['category'] = $instance['category'][0];
        }
      }
    }

    $this->shortcode->useGraphQL = $useJSVersion;
    echo $this->shortcode->display($instance);
    return;
  }

  /**
   * Create the admin area form for the widget
   * This includes querying the categories endpoint to retrieve a list of categories from UC Today,
   * which is necessary for creating the category dropdown on the widget form.  
   *
   * @param array $instance - the currently stored data
   * @return void
   */
  public function form($instance) {
    $didRequest = $this->requester->handleRequest();
    $hasData = isset($didRequest->response) && is_array($didRequest->response);
    $categories = $didRequest->error || !$hasData ? [] : $didRequest->response;

    ob_start();
    include(UCT_PLUGIN_DIR . 'partials/admin/uct-widget-form.php');
    echo ob_get_clean();
  }

  public function update($newInstance, $oldInstance) {

    $showExcerpt = (!empty($newInstance['show-excerpt'])) && $newInstance['show-excerpt'] === 'on' ? true : false;
    $showDate = (!empty($newInstance['show-date'])) && $newInstance['show-date'] === 'on' ? true : false;
    $showPictures = (!empty($newInstance['pictures'])) && $newInstance['pictures'] === 'on' ? true : false;
    $useJSVersion = (!empty($newInstance['use-js-version'])) && $newInstance['use-js-version'] === 'on' ? true : false;

    $instance['filterType'] = (!empty($newInstance['filterType'])) ? $newInstance['filterType'] : '';
    $instance['category'] = (!empty($newInstance['category'])) ? $newInstance['category'] : '';
    $instance['category-logical-type'] = (!empty($newInstance['category-logical-type'])) ? $newInstance['category-logical-type'] : 'AND';
    $instance['search'] = (!empty($newInstance['search'])) ? $newInstance['search'] : '';
    $instance['post-slugs'] = (!empty($newInstance['post-slugs'])) ? $newInstance['post-slugs'] : '';
    $instance['number-of-posts'] = (!empty($newInstance['number-of-posts'])) ? $newInstance['number-of-posts'] : 4;
    $instance['offset'] = (!empty($newInstance['offset'])) ? $newInstance['offset'] : 0;
    $instance['columns'] = (!empty($newInstance['columns'])) ? $newInstance['columns'] : 4;
    $instance['read-more-text'] = (!empty($newInstance['read-more-text'])) ? $newInstance['read-more-text'] : '';
    $instance['show-excerpt'] = $showExcerpt;
    $instance['show-date'] = $showDate;
    $instance['pictures'] = $showPictures;
    $instance['use-js-version'] = $useJSVersion;

    // enqueuing the js assets is controlled by the options in the db
    $options = get_option('uctoday_plugin_settings', []);

    if (!is_array($options)) {
      $options = [];
    }

    $options['uctoday_plugin_setting_graphql_mode'] = $useJSVersion ? "1" : "0";
    update_option('uctoday_plugin_settings', $options);

    return $newInstance;
  }

  /**
   * Wraps the register_widget method
   *
   * @return void
   */
  public function registerWidget() {
    register_widget($this);
  }

  /**
   * Check to see if this widget is active within the front-end view
   *
   * @return boolean
   */
  public function isActiveWidget(): bool {
    return is_active_widget(false, false, $this->id_base) !== false ? true : false;
  }

  /**
   * Recursively create the markup for the category dropdown
   * Style them to present a nested appearance
   *
   * @param array $categories
   * @param $selectedCategoryValues
   * @param int $depth
   * @return string
   */
  public function createCategoryMarkup(array $categories, $selectedCategoryValues, int $depth = 0): string {
    $markup = '';
    $depth++;
    $selectedCategories = []; 

    if (is_array($selectedCategoryValues) && array_key_exists(0, $selectedCategoryValues) && is_array($selectedCategoryValues[0])) { 
      foreach ($selectedCategoryValues[0] as $item) { 
        array_push($selectedCategories, $item); 
      }
    } else if (is_array($selectedCategoryValues)) {
      foreach ($selectedCategoryValues as $item) { 
        array_push($selectedCategories, $item); 
      }
    } else { 
      $selectedCategories = $selectedCategoryValues; 
    }

    foreach ($categories as $category) {
      $selectedCategory = in_array($category->slug, $selectedCategories) ? ' selected' : '';

      $markup .= '<option value="' . $category->slug . '" style="padding-inline-start:' . ($depth - 1) * 16 . 'px; "';
      $markup .= $selectedCategory . '>';
      $markup .= $category->name;
      $markup .= '</option>';
      if (count($category->children) > 0) {
        $markup .= $this->createCategoryMarkup($category->children, $selectedCategories, $depth);
      }
    }
    return $markup;
  }
}