<?php

/**
 * 
 * This partial creates the containers react will use to render the reader mode display. In order to support multiple instances of the reader, additional data attributes are added.
 * 
 */


$this->show_debug_info();
?>
<div id="uctoday-plugin-app-<?php echo $this->atts['category']; ?>" class="uctoday-plugin-app-container" data-category="<?php echo $this->atts['category']; ?>" data-num-posts="<?php echo $this->atts['number-of-posts']; ?>" data-columns="<?php echo $this->atts['columns']; ?>"></div>