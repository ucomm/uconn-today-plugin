<?php

/**
 * 
 * This partial creates a basic list of stories. Pictures are excluded from this view.
 * 
 */

$this->show_debug_info();

if ($showAdminNotice && is_user_logged_in()) {
  echo $this->doAdminNotice($this->requester->adminMessage);
}
?>

<div class="uctoday-plugin">
  <ul class="uctoday-plugin-list">
    <?php
    foreach ($stories as $story) {
    ?>
      <li class="uctoday-list-item uctoday-no-pictures">
        <div class="uctoday-title-container">
          <?php
          if ($this->atts['show-date']) {
          ?>
            <time class="uctoday-cell-date"><?php echo $story['date']; ?></time><br />
          <?php
          }
          ?>
          <a class="uctoday-cell-title" href='<?php echo $story['link']; ?>?utm_source=uconn-today-plugin' target='_blank' rel="noopener">
            <?php echo $story['title']; ?>
          </a>
        </div>
        <?php
        if ($this->atts['show-excerpt']) {
        ?>
          <div class="uctoday-excerpt-container">
            <?php echo $story['excerpt']; ?>
          </div>
        <?php
        }
        if ($this->atts['read-more-text'] !== '') {
        ?>
          <div class="uctoday-read-more-container">
            <a aria-hidden="true" href="<?php echo $story['link']; ?>"><?php echo $this->atts['read-more-text']; ?></a>
          </div>
        <?php
        }
        ?>
      </li>
    <?php
    }
    ?>
  </ul>
</div>