<?php

$this->show_debug_info();

if ($showAdminNotice && is_user_logged_in()) {
  echo $this->doAdminNotice($this->requester->adminMessage);
}

?>

<div class="uctoday-plugin uctoday-plugin-cards">
  <?php
  $stories;
  foreach ($stories as $story) {
    $date = $story['date'];
    $timestamp = strtotime($date);
    $formatted_date = date('F d, Y', $timestamp);
    $featured_image = isset($story['featured_image']) && $story['featured_image'] !== '' ? $story['featured_image'] : UCT_PLUGIN_URL . 'public-assets/img/uconn-wordmark.jpg';
    $categories = $story['public_categories'];
    $featured_category = isset($categories[0]->name) ? $categories[0]->name : 'Out of the Blue';
    foreach ($categories as $category) {
      if ($category->slug === 'blue') {
        $featured_category = $category->name;
      }
    }
  ?>
    <div class="uctoday-story-card-wrapper">
      <a href="<?php echo $story['link']; ?>" class="uctoday-story-link">
        <div class="uctoday-category-wrapper" aria-hidden="true">
          <p class="uctoday-story-category"><?php echo $featured_category; ?></p>
        </div>
        <div class="uctoday-image-container">
          <img class="uctoday-story-image" src="<?php echo $featured_image; ?>" alt="<?php echo $story['featured_image_alt']; ?>" />
        </div>
        <div class="uctoday-story-info-container">
          <div class="uctoday-title-container">
            <p class="uctoday-story-title"><?php echo $story['title']; ?></p>
          </div>
          <div class="read-more-container" aria-hidden="true">
            <p class="read-more-text">Read More <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Pro 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2024 Fonticons, Inc.-->
                <path d="M256 480a224 224 0 1 0 0-448 224 224 0 1 0 0 448zM256 0a256 256 0 1 1 0 512A256 256 0 1 1 256 0zM200 160l136 0c8.8 0 16 7.2 16 16l0 144c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-105.4L187.3 347.3c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6L297.4 192 200 192c-8.8 0-16-7.2-16-16s7.2-16 16-16z" />
              </svg></p>
          </div>
        </div>
      </a>
    </div>
  <?php
  }
  ?>
</div>