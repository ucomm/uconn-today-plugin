<?php

$this->show_debug_info();

if ($showAdminNotice && is_user_logged_in()) {
  echo $this->doAdminNotice($this->requester->adminMessage);
}
?>

<div class="uctoday-plugin uctoday-side-by-side">
  <?php
  foreach ($stories as $story) {
    $featured_image = isset($story['featured_image']) && $story['featured_image'] !== '' ? $story['featured_image'] : UCT_PLUGIN_URL . 'public-assets/img/uconn-wordmark.jpg';
  ?>
    <div class="uctoday-story-item-container">
      <div class="uctoday-image-container">
        <img src="<?php echo $featured_image; ?>" alt="<?php echo $story['featured_image_alt']; ?>" class="uctoday-story-image">
      </div>
      <div class="uctoday-title-container">
        <a href="<?php echo $story['link']; ?>" class="uctoday-story-link">
          <?php echo $story['title']; ?>
        </a>
      </div>
    </div>
  <?php
  }
  ?>
</div>