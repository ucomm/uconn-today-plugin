<?php

/**
 * Provide a public-facing view with images for the UConn Today plugin
 */


$this->show_debug_info();

if ($showAdminNotice && is_user_logged_in()) {
  echo $this->doAdminNotice($this->requester->adminMessage);
}
?>

<div class="uctoday-plugin uctoday-plugin-cols-<?php echo $this->atts['columns'] ?>">
  <?php
  foreach ($stories as $story) {
  ?>
    <div class="uctoday-list-item uctoday-pictures uctoday-celled">
      <div class="uctoday-img-container">
        <a title="Read about <?php echo $story['title']; ?>" href='<?php echo $story['link']; ?>?utm_source=uconn-today-plugin' target='_blank' rel="noopener">
          <?php
          if ($story['featured_image'] !== null) {
          ?>
            <img alt="<?php echo $story['featured_image_alt']; ?>" class="uctoday-cell-image" src="<?php echo $story['featured_image']; ?>" />
          <?php
          } else {
          ?>
            <img alt="" class="uctoday-cell-image" src="<?php echo UCT_PLUGIN_URL . 'public-assets/img/uconn-wordmark.jpg'; ?>" />
          <?php
          }
          ?>
        </a>
      </div>
      <div class="uctoday-title-container">
        <?php
        if ($this->atts['show-date']) {
        ?>
          <time class="uctoday-cell-date"><?php echo $story['date']; ?></time><br />
        <?php
        }
        ?>
        <a class="uctoday-cell-title" href='<?php echo $story['link']; ?>?utm_source=uconn-today-plugin' target='_blank' rel="noopener"><?php echo $story['title']; ?></a>
      </div>
      <?php
      if ($this->atts['show-excerpt']) {
      ?>
        <div class="uctoday-excerpt-container">
          <?php echo $story['excerpt']; ?>
        </div>
      <?php
      }
      if ($this->atts['read-more-text'] !== '') {
      ?>
        <div class="uctoday-read-more-container">
          <a aria-hidden="true" href="<?php echo $story['link']; ?>"><?php echo $this->atts['read-more-text']; ?></a>
        </div>
      <?php
      }
      ?>
    </div>
  <?php
  }
  ?>
</div>