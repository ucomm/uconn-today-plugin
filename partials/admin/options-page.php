<div class="uconn-today-plugin-admin">
  <h1>UConn Today Plugin Documentation</h1>

  <form action="options.php" method="post">
    <?php
    settings_fields('uctoday_plugin');
    do_settings_sections('uctoday_settings');
    submit_button();
    ?>
  </form>

  <div id="uctoday-status">
    <h2>UConn Today Plugin Responses</h2>
    <?php include UCT_PLUGIN_DIR . 'partials/admin/option-page-responses.php'; ?>
    <form action="options-general.php?page=<?php echo $this->menuSlug; ?>" method="POST">
      <input type="hidden" name="clear-uct-cache" value="true">
      <?php submit_button('Clear UCToday Cache') ?>
    </form>
  </div>

  <h2>Support/Feedback</h2>

  <p>Please contact University Communications for support or feedback on this plugin.</p>
  <ul>
    <li>General contact: <a href="mailto:webmaster@uconn.edu">webmaster@uconn.edu</a></li>
    <li>Developer contact: <a href="mailto:webadmin@uconn.onmicrosoft.com">webadmin@uconn.onmicrosoft.com</a></li>
  </ul>

  <h2>Usage</h2>

  <p>This plugin uses a Shortcode to display stories from UConn Today. The plugin requires a categorization (School/College, Category, Tag, Campus, etc), and then makes several settings available to alter the appearance and display of the stories.</p>

  <p>The bottom of this page provides a builder which can help you create the appropriate shortcode for your site.</p>

  <h3>Categorizations</h3>

  <p>Since the release of the updated UConn Today category model, all content attributes <em>except</em> category are deprecated. They still "work" in the sense that you can leave them in the sortcode. However, they resolve to posts by category. In most cases, ONE categorization should be used per shortcode, and can be mixed with any combination of settings.</p>

  <p><strong>NOTE: All categorizations are in lowercase, using dashes instead of spaces, as to mimic a 'slug'.<br>Ex: College of Liberal Arts and Sciences = college-of-liberal-arts-and-sciences</strong></p>

  <table class="settings-table">
    <thead>
      <tr>
        <th>Categorization</th>
        <th>Explanation</th>
        <th>Example</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Category</td>
        <td>Stories in a top-level categorization.</td>
        <td><code>category="campus-life"</code></td>
      </tr>
      <tr>
        <td>Author</td>
        <td>Stories written by a particular author, by NETID.</td>
        <td><code>author="aaa11111"</code></td>
      </tr>
    </tbody>
  </table>


  <h3>Settings</h3>

  <p>Each of these settings is optional but can help customize the display of stories by the shortcode.</p>

  <table class="settings-table">
    <thead>
      <tr>
        <th>Setting</th>
        <th>Explanation</th>
        <th>Example Value</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>number-of-posts</td>
        <td>Defines a maximum number of posts that will be fetched and displayed.</td>
        <td><code>number-of-posts="4"</code></td>
      </tr>
      <tr>
        <td>pictures</td>
        <td>Enable the display of pictures for each UConn Today story in the display.</td>
        <td><code>pictures="true"</code></td>
      </tr>
      <tr>
        <td>show-excerpt</td>
        <td>Enable the display of a story summary/excerpt for each story in the display.</td>
        <td><code>show-excerpt="true"</code></td>
      </tr>
      <tr>
        <td>read-more-text</td>
        <td>Adds text at the bottom of each story, with the provided text.</td>
        <td><code>read-more-text="Continue Reading"</code></td>
      </tr>
      <tr>
        <td>show-date</td>
        <td>Displays a publish date between the photo and title of each story, in M/D/Y format.</td>
        <td><code>show-date="true"</code></td>
      </tr>
    </tbody>
  </table>


  <h2>Builder</h2>

  <p>This tool can help build a shortcode for your needs.</p>

  <div class="shortcode-builder-container">
    <div>
      <pre><code id="uct-shortcode-builder"></code></pre>
      <button class="btn btn-primary" id="copy-shortcode">Copy Shortcode</button>
    </div>
    <div>
      <form id="uct-shortcode-builder-form">
        <label for="categorization"><strong>Categorization:</strong><br>
          <select id="categorization" name="categorization">
            <option value="category">Category</option>
            <option value="author">Author</option>
          </select>
        </label>

        <label for="category" class="categorization-toggle" data-cat="category">
          <strong>Category:</strong><br>
          <input type="text" name="category" id="category">
        </label>

        <label for="author" class="categorization-toggle" data-cat="author">
          <strong>Author (By NetID):</strong><br>
          <input type="text" maxlength="8" name="author" id="author" placeholder="aaa12345">
        </label>

        <label for="number-of-posts">
          <input type="number" id="number-of-posts" name="number-of-posts" placeholder="4" value="4" min="1" max="10">
          <strong>Number of Posts to Display</strong>
        </label>

        <label for="columns">
          <input type="number" id="columns" name="columns" placeholder="4" value="4" min="1" max="4">
          <strong>Number of Columns</strong>
        </label>

        <label for="pictures">
          <input type="checkbox" id="pictures" name="pictures">
          <strong>Include Pictures?</strong>
        </label>

        <label for="show-excerpt">
          <input type="checkbox" id="show-excerpt" name="show-excerpt">
          <strong>Show Summary/Excerpt?</strong>
        </label>

        <label for="show-date">
          <input type="checkbox" id="show-date" name="show-date">
          <strong>Show Post Date?</strong>
        </label>

        <label for="read-more-text">
          <input type="text" id="read-more-text" name="read-more-text" placeholder="Continue Reading">
          <strong>Read More Text</strong>
        </label>
      </form>
    </div>
  </div>
</div>