<?php

/**
 * 
 * This partial is responsible for the widget form
 * 
 */

$filterTypeFieldID = $this->get_field_id('filter-type');
$filterTypeFieldName = $this->get_field_name('filter-type');
$categoryFieldID = $this->get_field_id('category');
$categoryFieldName = $this->get_field_name('category') . "[]";
$categoryCombinationID = $this->get_field_id('category-logical-type');
$categoryCombinationFieldName = $this->get_field_name('category-logical-type');
$searchFieldID = $this->get_field_id('search');
$searchFieldName = $this->get_field_name('search');
$postSlugsFieldID = $this->get_field_id('post-slugs');
$postSlugsFieldName = $this->get_field_name('post-slugs');
$numPostsFieldID = $this->get_field_id('number-of-posts');
$numPostsFieldName = $this->get_field_name('number-of-posts');
$offsetFieldID = $this->get_field_id('offset');
$offsetFieldName = $this->get_field_name('offset');
$numColsFieldID = $this->get_field_id('columns');
$numColsFieldName = $this->get_field_name('columns');
$pictureFieldID = $this->get_field_id('pictures');
$pictureFieldName = $this->get_field_name('pictures');
$readMoreFieldID = $this->get_field_id('read-more-text');
$readMoreFieldName = $this->get_field_name('read-more-text');
$excerptFieldID = $this->get_field_id('show-excerpt');
$excerptFieldName = $this->get_field_name('show-excerpt');
$dateFieldID = $this->get_field_id('show-date');
$dateFieldName = $this->get_field_name('show-date');
$jsVersionFieldName = $this->get_field_name('use-js-version');
$jsVersionFieldID = $this->get_field_id('use-js-version');

$filterTypeValue = !empty($instance['filter-type']) ? $instance['filter-type'] : '';
$categoryValue = !empty($instance['category']) ? $instance['category'] : [];
$categoryCombinationValue = !empty($instance['category-logical-type']) ? $instance['category-logical-type'] : 'AND';
$searchValue = !empty($instance['search']) ? $instance['search'] : '';
$postSlugsValue = !empty($instance['post-slugs']) ? $instance['post-slugs'] : '';
$numPostsValue = !empty($instance['number-of-posts']) ? $instance['number-of-posts'] : 4;
$offsetValue = !empty($instance['offset']) ? $instance['offset'] : 0;
$numColsValue = !empty($instance['columns']) ? $instance['columns'] : 4;
$readMoreValue = !empty($instance['read-more-text']) ? $instance['read-more-text'] : '';
$pictureFieldChecked = !empty($instance['pictures']) ? $instance['pictures'] : false;
$excerptFieldChecked = !empty($instance['show-excerpt']) ? $instance['show-excerpt'] : false;
$dateFieldChecked = !empty($instance['show-date']) ? $instance['show-date'] : false;
$useJSVersionChecked = !empty($instance['use-js-version']) ? $instance['use-js-version'] : false;

?>

<div class="uct-widget-container" id="uct-widget-container-<?php echo $this->id; ?>">
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $filterTypeFieldID; ?>">
        UConn Today Article Filter
      </label>
    </div>
    <div class="field-wrapper">
      <select class="filter-type-select" id="<?php echo $filterTypeFieldID; ?>" name="<?php echo $filterTypeFieldName; ?>" onchange="checkOptions(this)">
        <option value="">Select a way to filter the articles:</option>
        <option value="defaultFilter" <?php if ($filterTypeValue == "defaultFilter") echo ' selected="selected"'; ?>> Default/Unfiltered </option>
        <option value="categoryFilter" <?php if ($filterTypeValue == "categoryFilter") echo ' selected="selected"'; ?>> Select a Category </option>
        <option value="searchFilter" <?php if ($filterTypeValue == "searchFilter") echo ' selected="selected"'; ?>> Input a Search Term </option>
        <option value="slugFilter" <?php if ($filterTypeValue == "slugFilter") echo ' selected="selected"'; ?>> Input Post Slug(s) </option>
      </select>

      <div class="category-input-wrapper" id="category-input-wrapper-<?php echo $this->id; ?>" <?php if ($filterTypeValue != "categoryFilter") echo ' style="display: none;"'; ?>>
        <div class="label-wrapper">
          <label for="<?php echo $categoryCombinationID; ?>">
            UConn Today Category Combination Type
          </label>
        </div>
        <div class="field-wrapper">
          <select name="<?php echo $categoryCombinationFieldName; ?>" id="<?php echo $categoryCombinationID; ?>">
            <option <?php selected($categoryCombinationValue, 'OR'); ?> value="OR">OR</option>
            <option <?php selected($categoryCombinationValue, 'AND'); ?> value="AND">AND</option>
          </select>
        </div>
        <div class="label-wrapper">
          <label for="<?php echo $categoryFieldID; ?>">
            UConn Today Category
          </label>
        </div>
        <div class="field-wrapper">
          <select multiple size="20" id="<?php echo $categoryFieldID; ?>" name="<?php echo $categoryFieldName; ?>">
            <option value="">Select Category</option>
            <?php
              $markup = $this->createCategoryMarkup($categories, $categoryValue, 0);
              echo $markup;
            ?>
            </select>
            <p>To display the most recent posts (regardless of category), do not select a category from this list (select the 'Select Category' option).</p>
          </div>
        </div>

        <div class="search-input-wrapper" id="search-input-wrapper-<?php echo $this->id; ?>" <?php if ($filterTypeValue != "searchFilter") echo ' style="display: none;"'; ?>>
          <div class="label-wrapper">
            <label for="<?php echo $searchFieldID; ?>">
              Search Term(s)
            </label>
          </div>
          <div class="field-wrapper">
            <input type="text" name="<?php echo $searchFieldName; ?>" id="<?php echo $searchFieldID; ?>" placeholder="Enter your search term(s) here..." value="<?php echo $searchValue; ?>">
            <p>Enter in the term(s) you wish to search by. Most recent articles containing the term will be displayed first.</p>
          </div>
        </div>

        <div class="post-slugs-input-wrapper" id="post-slugs-input-wrapper-<?php echo $this->id; ?>" <?php if ($filterTypeValue != "slugFilter") echo ' style="display: none;"'; ?>>
          <div class="label-wrapper">
            <label for="<?php echo $postSlugsFieldID; ?>">
              Post Slug(s)
            </label>
          </div>
          <div class="field-wrapper">
            <input type="text" name="<?php echo $postSlugsFieldName; ?>" id="<?php echo $postSlugsFieldID; ?>" placeholder="Enter your slug(s) here..." value="<?php echo $postSlugsValue; ?>">
            <p>Enter in your slug(s). If using multiple slugs, separate them by commas. Ex: uconn-nation-together, 2022-uconn-law-commencement-highlights, another-slug-here</p>
          </div>
        </div>

        <p>Which method would you like to use to filter the UConn Today articles?<br />
          Default/Unfiltered: displays the most recent posts, regardless of category<br />
        Select a Category: You will be allowed to select one category from a dropdown menu of official UConn Today categories. The most recent articles from that category will be displayed.<br />
        Input a Search Term: You will be allowed to input a keyword and the most recent categories containing that keyword will be displayed. <br />
        Input Slug(s): You will be allowed to enter a comma separated list of slugs in order to display very specific articles. Ex. uconn-nation-together</p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $numPostsFieldID; ?>">
        Number of posts to display
      </label>
    </div>
    <div class="field-wrapper">
      <input type="number" name="<?php echo $numPostsFieldName; ?>" id="<?php echo $numPostsFieldID; ?>" min="1" max="12" value="<?php echo $numPostsValue; ?>">
      <p>A maximum of 12 posts can be displayed.</p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $offsetFieldID; ?>">
        The number of posts to skip.
      </label>
    </div>
    <div class="field-wrapper">
      <input type="number" name="<?php echo $offsetFieldName; ?>" id="<?php echo $offsetFieldID; ?>" min="0" max="10" value="<?php echo $offsetValue; ?>">
      <p>A maximum of 10 posts can be skipped.</p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $numColsFieldID; ?>">
        Number of columns
      </label>
    </div>
    <div class="field-wrapper">
      <input type="number" name="<?php echo $numColsFieldName; ?>" id="<?php echo $numColsFieldID; ?>" min="1" max="12" value="<?php echo $numColsValue; ?>">
      <p>The number of columns to divide stories into (12 max).</p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $pictureFieldID; ?>">
        Post featured image visibility
      </label>
    </div>
    <div class="field-wrapper">
      <input type="checkbox" name="<?php echo $pictureFieldName; ?>" id="<?php echo $pictureFieldID; ?>" <?php checked($pictureFieldChecked, 'on'); ?>>
      <p>If checked, the post's featured image will be displayed.</p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $readMoreFieldID; ?>">
        "Read More" Text (optional)
      </label>
    </div>
    <div class="field-wrapper">
      <input type="text" name="<?php echo $readMoreFieldName; ?>" id="<?php echo $readMoreFieldID; ?>" placeholder="Read More..." value="<?php echo $readMoreValue; ?>">
      <p>This field is optional. If it is left empty, no "read more" text will be displayed.</p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $excerptFieldID; ?>">
        Excerpt visibility
      </label>
    </div>
    <div class="field-wrapper">
      <input type="checkbox" name="<?php echo $excerptFieldName; ?>" id="<?php echo $excerptFieldID; ?>" <?php checked($excerptFieldChecked, 'on'); ?>>
      <p>If checked, the post's excerpt will be displayed.</p>
    </div>
  </div>
  <div class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $dateFieldID; ?>">
        Post date visibility
      </label>
    </div>
    <div class="field-wrapper">
      <input type="checkbox" name="<?php echo $dateFieldName; ?>" id="<?php echo $dateFieldID; ?>" <?php checked($dateFieldChecked, 'on'); ?>>
      <p>If checked, the post's date will be displayed.</p>
    </div>
  </div>
  <div id="js-version-wrapper" class="widget-form-wrapper">
    <div class="label-wrapper">
      <label for="<?php echo $jsVersionFieldID; ?>">
        Enables the "reader mode" version of the plugin.
      </label>
    </div>
    <div class="field-wrapper">
      <input type="checkbox" name="<?php echo $jsVersionFieldName; ?>" id="<?php echo $jsVersionFieldID; ?>" <?php checked($dateFieldChecked, 'on'); ?>>
      <p>If checked, stories will open with their content on the current site.</p>
    </div>
  </div>
</div>