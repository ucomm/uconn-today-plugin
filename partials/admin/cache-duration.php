<input 
  type="number" 
  name="uctoday_plugin_settings[uctoday_plugin_setting_cache_duration]" 
  id="uctoday_plugin_setting_cache_duration" 
  value="<?php echo $cacheDurationSetting; ?>" 
  min="1" 
  max="24" 
/>
<label for="uctoday_plugin_setting_cache_duration">Set the cache duration in hours.</label>