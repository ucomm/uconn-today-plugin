<?php
foreach ($responseData as $index => $response) {
  $hasErrorStatus = $response->error;
  $errorClass = $hasErrorStatus ? 'error-message' : '';
  $errorMessage = $response->errorCode > 0 ? $notificationService->getErrorMessage($response->errorCode) : 'No errors';
?>
  <div class="response-group">
    <p><strong>Response Group: </strong><?php echo $index + 1; ?></p>
    <p class="<?php echo $errorClass; ?>"><strong>Current Error Status: </strong><?php echo $response->error ? 'Errors' : 'No Errors'; ?></p>
    <p class="<?php echo $errorClass; ?>"><strong>Current Error Message: </strong><?php echo $errorMessage; ?></p>
    <ul class="article-list">
      <?php
      foreach ($response->response as $article) {

        $title = $article->title->rendered;
        $link = $article->link;
        echo "<li><a href='$link'>$title</a></li>";
      }
      ?>
    </ul>
  </div>
<?php
}
